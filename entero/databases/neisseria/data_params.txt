tabname	name	sra_field	mlst_field	display_order	nested_order	label	datatype	min_value	max_value	required	default_value	allow_pattern	description	vals	max_length	no_duplicates	allow_multiple_values	group_name
strains	strain		STRAIN															
strains	contact		SOURCE_LAB															
strains	source_type		ISOLATED_FROM															
strains	collection_year		YEAR															
strains	continent		CONTINENT															
strains	country		COUNTRY															
strains	city		LOCATION															
strains	serotype	Sample,Metadata,Serotype	SEROTYPE	9	0	Serotype	combo			0								
strains	path_nonpath		PATHORNONPATH	11	0	Pathogen/Non pathogen	combo			0				Pathogen|Non Pathogen				
strains	simple_pathogenesis		SIMPLE_PATHOGENICITY	12	0	Simple Patho	combo			0								
strains	simple_disease		SIMPLE_DISEASE	13	0	Simple Disease	combo			0								
strains	species	Sample,Metadata,Species		7	0	Species	combo			0								
strains	disease		DISEASE	14	0	Disease	combo			0																					
strains	serological_group		SEROLOGICAL_GROUP	8	0	Serological Group	combo			0								
strains	source_type		HOST_TYPE															
strains	source_details		HOST_SPECIES