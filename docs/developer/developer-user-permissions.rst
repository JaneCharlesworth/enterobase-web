User Permissions
================
User Permissions are stored in the user_permission_tags table in the system (entero) database, which consists of a field, value species and user id as well as an id. The field values are 

* **api access**- Allows user access to the API. The value should be True or False, although absence is the same as false
* **upload_reads** - Allows users to upload reads to the database.  The value should be True or False, although absence is the same as false
* **view_species** - Allows users to view a private database.  The value should be True or False, although absence is the same as false
* **upload_read_allowed** - The value specifies what kind of read the user can upload. All users are allowed to upload paired end Illumina reads. At the moment, the only value allowed is Complete Genome i.e. a fasta file
* **edit_strain_metadata** Allows the user to edit all the strain metadata in the database.  The value should be True or False, although absence is the same as false
* **allowed_schemes** Allows a user to access a private scheme,  the value should be the name of the scheme
* **delete_strains** If True allows users to delete strains
* **change_strain_owner** If true allows users to change strain ownership
* **change_assembly_release_date** If True allows users to change the release date of assemblies
* **change_assembly_status** If True allows users to change assembly status