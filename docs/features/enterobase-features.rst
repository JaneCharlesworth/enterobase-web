Key features in Enterobase
==========================

.. toctree::
    :maxdepth: 1
    
    species-index
    using-the-browser
    buddies
    main-search-page
    search-agama
    metadata-fields
    experimental-fields
    add-upload-reads
    editing-strain-metadata
    using-workspaces
    user-defined-content
    locus-search
    ms-trees
    snp-projects
    enterobase-snps-jbrowse
    accessory-genome
    user-download-schemes
    user-download-assemblies
    user-download-annotations
    download-schemes
    st-allele-search
    clustering
    searching-strains-by-st
    search-st-types-from-alleles
    searching-within-neighbours
    jobs
