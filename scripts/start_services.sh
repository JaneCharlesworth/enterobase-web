#!/bin/bash  
home_dir=/var/www/entero
date >> ${home_dir}/logs/entero.log
if !  ps ax | grep -v grep | grep " 25:localhost:25 martin@137.205.123.28"  > /dev/null
then  
    echo "tunnel to email server is down - restarting\n" >> ${home_dir}/logs/entero.log
    ssh -i ${home_dir}/keys/mail_key -f -N -q -L 25:localhost:25 martin@137.205.123.28 >> ${home_dir}/logs/entero.log
fi

succeed="$(nc -zv -w 5 127.0.0.1 25 2>&1)"
if  ! [ "$succeed" = "Connection to 127.0.0.1 25 port [tcp/smtp] succeeded!" ]; then
	echo "connection not responding, killing and creating new one" >> ${home_dir}/logs/entero.log
	kill -9 `ps -ef | grep mail_key | grep -v grep | awk '{print $2}'`
	ssh -i ${home_dir}/keys/mail_key -f -N -q -L 25:localhost:25 martin@137.205.123.28 >> ${home_dir}/logs/entero.log

fi


if !  ps ax | grep -v grep | grep " 5432:localhost:5432 enterobase@137.205.52.22"  > /dev/null
then  
    echo "tunnel to email server is down - restarting\n" >> ${home_dir}/logs/entero.log
    ssh -i ${home_dir}/keys/base_nabil.pem -f -N -q -L 5432:localhost:5432 enterobase@137.205.52.22 >> ${home_dir}/logs/entero.log
fi

succeed="$(nc -zv -w 5 127.0.0.1 5432 2>&1)"
if  ! [ "$succeed" = "Connection to 127.0.0.1 5432 port [tcp/postgresql] succeeded!" ]; then
	echo "connection not responding, killing and creating new one" >> ${home_dir}/logs/entero.log
	kill -9 `ps -ef | grep  5432:localhost:5432 | grep -v grep | awk '{print $2}'`
	ssh -i ${home_dir}/keys/base_nabil.pem -f -N -q -L 5432:localhost:5432 enterobase@137.205.52.22 >> ${home_dir}/logs/entero.log

fi

if !  ps ax | grep -v grep | grep " enterotool@137.205.52.26:/share_space"  > /dev/null
then  
    echo "link to share space down - restarting\n" >> ${home_dir}/logs/entero.log
    sshfs enterotool@137.205.52.26:/share_space /share_space -o allow_other >> ${home_dir}/logs/entero.log
fi
