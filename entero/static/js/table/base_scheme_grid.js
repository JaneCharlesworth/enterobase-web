BaseSchemeGrid.prototype = Object.create(BaseExperimentGrid.prototype);
BaseSchemeGrid.prototype.constructor= BaseSchemeGrid;


/**
 * The base grid for all schemes/experiments to be displayed in the 
 * right table of the 
 * @constructor
 * @extends BaseExperimentGrid
 */
function BaseSchemeGrid(name,config,species,scheme,scheme_name){
        
        BaseExperimentGrid.call(this,name,config,species,scheme,scheme_name);
        var self = this;
        this.mstTreeWindows=[];
        this.mstTreeType = "d3";
        this.ignoreMetaData = ['Project','barcode','release_date','created','Sample','uberstrain',"checkselect",
        "edit_individual","seq_library","seq_insert","collection_year","collection_month","collection_date","collection_time",
        "sample_accession","secondary_sample_accession"];
        this.tree_name = null;
        this.tree_id=null;
    
        //just a list of loci
        this.loci = [];
        this.addExtraRenderer("serotype_pred",function(cell,value,rowID){
                if (value){
                        $(cell).append($("<span class='glyphicon glyphicon-eye-open style='padding:0px;margin:0px;display:inline-block;font-size:+"+self.fontSize+"px'></span>"))                            
                        .css("cursor","pointer");
                }
                
        });

        this.addCustomColumnHandler("serotype_pred",function(cell,rowIndex,rowID){	
                var colIndex = self.getColumnIndex('st');
                var st = self.getValueAt(rowIndex,colIndex);
                colIndex = self.getColumnIndex("st");
                self.displaySerotypePrediction(st);
        });
    
};


/**
* Returns the number of unique STs in the table as well as the number of strains
* that contain an ST
* @param {boolean} selected_only - If true then only STs in selected rows will be counted 
* @retuns {object} An object with strain_count and st_count
*/
BaseSchemeGrid.prototype.getSTCount=function(selected_only){
        var barcode_set={};
        var st_count=0;
        var strain_count=0
        for (var n=0;n<this.getRowCount();n++){
                
                var id = this.getRowId(n);
                if  (selected_only && !this.strainGrid.selectedRows[id]){
                        continue;
                }
        
               
                var barcode = this.extraRowInfo[id];
                if (!barcode){
                        continue;
                }
                
                strain_count++;
                if (!barcode_set[barcode]){
                        barcode_set[barcode]=true;
                        st_count++;
                }      
        }
        return {"strain_count":strain_count,"st_count":st_count};
}

/**
* Returns all the strains in the (filtered) grid along with their ST barcodes (any strains without STs will be ignored)
* @param {boolean} selected_only - If true then only STs in selected rows will be returns
* @returns {Object} An dictionary  strain id to an  of arrays which contain three elements
* the ST barcode, ST number  and assembly_id 
*/
BaseSchemeGrid.prototype.getSTList=function(selected_only){
        var barcode_col= this.strainGrid.getColumnIndex("barcode");
        var ST_col = this.getColumnIndex("st")
        var return_dict={};
        for (var rowIndex=0;rowIndex<this.getRowCount();rowIndex++){
               var id =  this.getRowId(rowIndex);
               if (selected_only && !this.strainGrid.selectedRows[id]){
                        continue;
               }
               var st_barcode = this.extraRowInfo[id];
               if (! st_barcode){
                        continue;
               }
               var assembly_id = this.strainGrid.best_assembly[id];
               var st_number = this.getValueAt(rowIndex,ST_col)
               return_dict[id]=[st_barcode,st_number,assembly_id];
               
        }
        return return_dict;

}

BaseSchemeGrid.prototype.makeMSTree = function(tree_name,workspace,callback,only_selected){
        var stColIndex = this.getColumnIndex('st');
        var strainColIndex = this.strainGrid.getColumnIndex('strain');   
        var schemegenes=["ID"];
        var isolates=[];
        var metadata=["StrainID","Collection Date"];
        var grid = this.strainGrid;
        var barcode_list=[];
        var barcode_set = {}
        for (var index2 in this.loci){
                schemegenes.push(this.loci[index2]);
        }
        for (var name in grid.val_params){
                 if (this.ignoreMetaData.indexOf(name) === -1 && !grid.val_params[name]['not_write']){
                        metadata.push(grid.val_params[name]['label']);
                 }
        
        }
        
        for (var rowIndex=0;rowIndex<this.getRowCount();rowIndex++){
                var vals = grid.getRowValues(rowIndex);
                var st = this.getValueAt(rowIndex,stColIndex);
                var id =  this.getRowId(rowIndex);
                if (only_selected && ! this.strainGrid.selectedRows[id]){
                        continue;
                }
                var isolate = {"ID":"ST"+st,"StrainID":id};
                if (!st){
                        continue;                
                }
                var barcode = this.extraRowInfo[id];
                barcode_set[barcode]=true;
                for (name in vals){
                        if (this.ignoreMetaData.indexOf(name) !== -1 ){
                                continue;
                        }
                        if (grid.compound_columns[name]){
                                
                                if (name ==="Collection Date"){
                                     var list= grid.comp_values.getRow("Collection Date",id,1);
                                     var dateText="";
                                     if (list){                                     
                                        if (list[2]){dateText+=list[0]+"-"+list[1]+"-"+list[2]}
                                        else if (list[1]){dateText+=list[0]+"-"+list[1]+"-01"}
                                        else if (list[0]){dateText+=list[0]+"-01-01"}
                                     }
                                     isolate["Collection Date"]=dateText;
                                     continue;
                                }
                                var c_vals =grid.comp_values.getRowAsDict(name,id,1);
                                for (var v in c_vals){
                                   isolate[grid.val_params[v]['label']]=c_vals[v]     
                                }
                        }
                        else if (!grid.val_params[name]['not_write']){
                                isolate[grid.val_params[name]['label']]=vals[name];
                                
                        }
                }
                                               
                isolates.push(isolate);
        }
        var input = { 
                scheme:this.scheme,
                schemegenes: schemegenes,
                metadata: metadata,
                isolates: isolates
                
        };
        for (barcode in barcode_set){
                barcode_list.push(barcode);
        }
        var to_send={
                data:input,
                barcodes:barcode_list,
                database:this.species,
                workspace:workspace,
                name:tree_name
        
        };
        if (isolates.length<2){
                callback("Error","You have only have " + isolates.length + " node(s) in the tree. If you have checked 'selected only' - make sure you have selected some strains in the main grid. ")
                return;
        }
        var self=this;
        Enterobase.call_restful_send_json("/create_ms_tree","POST",to_send,this.showError).done(function(data){
                if (data==='Failiure'){
                        self.showError("There was a problem creating the MS Tree");
                }
                else{
                        if (callback){
                                var arr= data.split(":");
                                callback(arr[0],arr[1]);
                                             
                        }
                
                }
                
        });
        
        
        
           
}

BaseSchemeGrid.prototype.displayPhyloviz = function(){
        var windowName = "test12";//this.scheme+"_"+this.mstTreeWindows.length
        var phylovizWindow = window.open("/my_phyloviz",windowName,'width=1000, height=600');
        this.mstTreeWindows.push(phylovizWindow);
        
}

BaseSchemeGrid.prototype.loadMSTree= function(name,id){
        this.tree_name = name;
        this.tree_id=id;
        var windowName = name;//this.scheme+"_"+this.mstTreeWindows.length
        var phylovizWindow = window.open("/my_phyloviz",windowName,'width=1000, height=600');
      
}
BaseSchemeGrid.prototype.getMSTreeData= function(callback){
        var self = this;
        var callback = callback;
       Enterobase.call_restful_json("/get_ms_tree_data?id="+this.tree_id,"GET","",this.showError).done(function(data){
                if (data['waiting']){
                        setTimeout(function(){
                                 self.getMSTreeData(callback)
                        },10000)
                       
                }
                else{
                        callback(data);            
                }     
       });      
}



BaseSchemeGrid.prototype.displaySerotypePrediction= function(st){
        var to_send = {
                st:st,
                scheme:this.scheme
        
        }
        Enterobase.call_restful_json("/species/"+this.species+"/get_serotype_prediction","POST",to_send,this.showError).done(function(data){
                msg = "<table>"
                for (var index in data){
                        var rec = data[index];
                        msg+="<tr><td>"+rec[0]+"</td><td>"+rec[1]+"</td></tr>"
                
                }
                msg+="</table>"
                Enterobase.modalAlert(msg,"Serotype Prediction For ST:"+st);	
        });

}




BaseSchemeGrid.prototype.setMetaData= function(data){   
        var self =  this;
        for (var index in data){
                if (data[index]['group_name'] === 'Locus'){
                        var locus  = data[index]['name'];
                        this.loci.push(locus);
             }
        }
         data.push({
        name:"differences",
        label:"Differences",
        datatype:'integer',
        display_order:-100
   
   });
        ValidationGrid.prototype.setMetaData.call(self,data);
        //can only set column widths after call to set metadata
        for (var index in data){
                if (data[index]['group_name'] === 'Locus'){
                        var locus  = data[index]['name'];
                        self.columnWidths[locus]=60;
                }
    }	           
} 


BaseSchemeGrid.prototype.loadNewData= function(data){
     //get allele info in order that clicking on the can allow retrieval of information
     var has_diff = false;
      for (index in data){  
            var id = data[index]['id'];
                if (!data[index]['st']){
                  data[index]['st']="ND";
                }
                if (data[index]['differences']){
                        has_diff = true;
                }
            this.nonEditableRows[id]=true;
            this.isDownloadable[id]=false;
           
       }
       this.hiddenColumns['differences']=!has_diff;
       
       
       ValidationGrid.prototype.loadNewData.call(this,data,false,true);
}
BaseSchemeGrid.prototype.getMethodData= function(){
        var self = this;
       return [{
                name:"find_similar_sts",
                label:"Find ST(s)",
                method:function(){
                        self.findSimilarSTs(null,null,false);
                }
    
       }];
}

BaseSchemeGrid.prototype.findSimilarSTs= function(alleles,st){
        var initial_data={
                st:st,
                alleles:alleles,
                scheme:this.scheme
        }
       displaySchemeDialog(initial_data);

};

BaseSchemeGrid.prototype.addToContextMenu =  function (rowIndex,colIndex,target){
        
        var st = this.getValueAt(rowIndex,this.getColumnIndex('st'));
        var alleles = null;
        if (this.loci.length<10){
                alleles=[];
                for (var index in this.loci){
                        var colIndex= this.getColumnIndex(this.loci[index]);
                        if (colIndex === -1){
                                break;
                        }
                        alleles.push(this.getValueAt(rowIndex,colIndex));
                }
        }
        var self = this;
        var extramenu=[	{
                         name: 'Find ST(s)',
                         title: 'Find ST(s)',
                         fun: function () {
                                 self.findSimilarSTs(alleles,st)
                        }
                        }
                
                
        ];         
        return extramenu;
}