Renewing your API Token automatically
=====================================
Your API token will expire every 6 months as a security precaution. You can 
always get a new token from the EnteroBase webpage. Some users may wish to have 
a more automated method.

To refresh your token automatically, you just need to query the login endpoint 
with your username and password. I strongly recommend that send all API requests 
through HTTPS, especially this one. 

.. code-block:: html

    https://enterobase.warwick.ac.uk/api/v2.0/login?username=USERNAME&password=PASSWORD



In Python this could be a simple script built into a larger program:

.. code-block:: python

    from urllib2 import HTTPError
    import urllib2
    import base64
    import json
    
    ENTEROBASE_USERNAME = 'YOUR_USERNAME'
    ENTEROBASE_PASSWORD = 'YOUR_PASSWORD'
    ENTEROBASE_SERVER = 'https://enterobase.warwick.ac.uk'
    
    address = '%s/api/v2.0/login?username=%s&password=%s' %(ENTEROBASE_SERVER, ENTEROBASE_USERNAME, ENTEROBASE_PASSWORD)
    
    try:
        response = urllib2.urlopen(address)
        data = json.load(response)
        print json.dumps(data, sort_keys=True, indent=4, separators=(',', ': '))
    
    except HTTPError as Response_error:
        print '%d %s. <%s>\n Reason: %s' %(Response_error.code,
                                                          Response_error.msg,
                                                          Response_error.geturl(),
                                                          Response_error.read())
