JobsGrid.prototype = Object.create(ValidationGrid.prototype);
JobsGrid.prototype.constructor = JobsGrid

function JobsGrid(name,config,species,admin){
   this.status_to_image= {
		"WAIT RESOURCE":"wait_resource.gif",
		"QUEUE":"queue.gif",
		"RUNNING":"running.gif",
		"FAILED":"fail.gif",
		"KILLED":"fail.gif",
		"COMPLETE":"complete.gif",
		"SUBMIT":"queue.gif"
    }
   this.finished_jobs = ["KILLED","FAILED","COMPLETE"];
   this.failed_jobs = ["KILLED","FAILED"]
   var self = this;
   ValidationGrid.call(this,name,config);
   this.lineColors= ["#CCC","#FFF"]
   this.species=species;
   this.detailsCallback= function(){};
   this.jobDetailsURl = "/species/get_individual_job";
   this.admin=true;
   
   this.addExtraRenderer("status_icon",function(cell,value,rowID){
		
		var r= self.getRowIndex(rowID);
		var c= self.getColumnIndex("status");
		var img_src = self.imgFolder+self.status_to_image[self.getValueAt(r,c)];
		var size = self.fontSize;
		$(cell).append($("<img>").attr("src",img_src).width(size).height(size));
	
   });
 
   this.addExtraRenderer("view_job",function(cell,value,rowID){
		$(cell).html("<span class='glyphicon glyphicon-eye-open style='padding:0px;margin:0px;display:inline-block;font-size:+"+self.fontSize+"px'></span>")               //a hack adding function to the cell		   
		.css("cursor","pointer")
		.unbind('click').bind('click',function(event){
				var p =self.data[1]['columns'];
				self.showJobDetails(rowID);		
		});
   });
 
   
		
  
   
};


JobsGrid.prototype.showJobDetails= function(rowID){
    var self = this;
    Enterobase.call_restful_json(this.jobDetailsURl+"?id="+rowID,"GET","",callFailed).done(function(data){	
		self.detailsCallback(data,rowID);	
	});

}



JobsGrid.prototype.setMetaData= function(){ 
		var self = this;
		self.addCheckSelectColumn();
        var id = {
            name:"id",
            label:"ID",
            datatype:"integer",
            display_order:3
          
        }
        self.addNewColumn(id);
        var user_id = {
             name:"user_id",
             label:"User ID",
             datatype:"integer",
             display_order:4
            
        }
        self.addNewColumn(user_id)
        var status = {
             name:"status",
             label:"Status",
             display_order:5
            
        }
        self.addNewColumn(status)
	    var pipeline = {
             name:"pipeline",
             label:"Pipeline",
             display_order:6
            
        }
        self.addNewColumn(pipeline)
		var date_sent = {
             name:"date_sent",
             label:"Date",
             display_order:7
            
        }
        self.addNewColumn(date_sent)
	
		var accession = {
             name:"input",
             label:"Input",
             display_order:8
            
        }
        self.addNewColumn(accession)
		
		var output = {
			 name:"output",
			 label:"Output",
			 display_order:9
			
		}
		self.addNewColumn(output);
	
		var view={
				name:"view_job",
				label:"<span class='glyphicon glyphicon-eye-open'></span>",
				display_order:2,
				not_write:true
		
		}
		self.addNewColumn(view);
		var status_icon={
				name:"status_icon",
				label:"<span class='glyphicon glyphicon-ok-circle'></span>",
				display_order:1,
				not_write:true
		
		}
		self.addNewColumn(status_icon);
		
		
		
		
		self.columnWidths['status_icon'] = 30;
		self.columnWidths['view_job'] = 30;
		self.columnWidths['id'] = 60;
		self.columnWidths['input'] = 300;
		self.columnWidths['output'] = 300; 
}

JobsGrid.prototype.killJobs =  function (jobIDs){
		var ids = this.getIncompleteJobs(jobIDs);
		if (ids.length ==0){
				Enterobase.modalAlert("There are no selected jobs which are currently running/queued, therefore no jobs have been killed");
				return;
		
		}	
		var url = "/species/kill_jobs?ids="+ids.join(",");
		var self = this;
		var c= this.getColumnIndex("status")
		Enterobase.call_restful_json(url,"GET","",callFailed).done(function(data){	
				for (key in data){
						var r = self.getRowIndex(key)
						self.setValueAt(r,c,data[key])
				}	

		self.refreshGrid();
		});

}

JobsGrid.prototype.downloadResults = function(jobIDS){
		var ids = this.getIncompleteJobs(jobIDS,true);
		if (ids.length ==0){
				Enterobase.modalAlert("There are no selected jobs which are complete, therefore there is no data to downloade");
				return;
		
		}
		for (var index in ids){
				var url = this.extraRowInfo[ids[index]];
				 Enterobase.downloadURL(url);		
		}
}






JobsGrid.prototype.updateDisplay= function(data){
    var self = this;
	for (index in data){
						var row=data[index];
						//set extra row info i.e. download URL
						self.extraRowInfo = row['extra_row_info']
						delete row['extra_row_info'];
						var r = self.getRowIndex(row['id']);
						
						var render = r>=this.firstVisibleRow && r <= this.lastVisibleRow;
						//change all values
						for (var key in row){
								var c = self.getColumnIndex(key);
								if (c === -1){
									continue;								
								}
								
								self.setValueAt(r,c,row[key],render);
						}
				}	
		self.refreshGrid();
}


JobsGrid.prototype.forceReload= function(jobIDS){
		var to_send = {
						ids:jobIDS.join(","),
						}
		var self = this;
		Enterobase.call_restful_json("/species/"+this.species+"/force_job_callback","POST",to_send,callFailed).done(function(data){	
				self.updateDisplay(data);
				Enterobase.modalAlert("Callbacks have been forced on the selected jobs");
		});
		


}

JobsGrid.prototype.showMessage = function(msg){
     window.open(Enterobase.wiki_url+"Jobs#"+msg,'newwindow','width=1000, height=600');
};

JobsGrid.prototype.refreshJobs= function(jobIDS){
		var self = this;
		var ids = this.getIncompleteJobs(jobIDS)
		if (ids.length ==0){
				Enterobase.modalAlert("There are no selected jobs which are running/queued, therefore no jobs have been refreshed");
				return;
		
		}	
				
		var to_send = {
						ids:ids.join(","),
						
						}
		Enterobase.call_restful_json("/species/"+this.species+"/refresh_jobs","POST",to_send,callFailed).done(function(data){	
				self.updateDisplay(data);
		});
}

JobsGrid.prototype.getIncompleteJobs=function(rowIDs,complete){
    var status_col = this.getColumnIndex("status");
	var r_list=[];
	for (var index in rowIDs){
		var rowIndex = this.getRowIndex(rowIDs[index]);
		var status = this.getValueAt(rowIndex,status_col);
		if (this.finished_jobs.indexOf(status)===-1 && ! complete){
				r_list.push(rowIDs[index]);
		}
		else if (this.finished_jobs.indexOf(status)!==-1 && complete){
				r_list.push(rowIDs[index]);
		}
	}
	return r_list;
}


JobsGrid.prototype.resendJobs= function(rowIDs){
	var status_col = this.getColumnIndex("status");
	var id_list=[]
	for (var index in rowIDs){
		var rowIndex = this.getRowIndex(rowIDs[index]);
		var status = this.getValueAt(rowIndex,status_col);
		//if (this.failed_jobs.indexOf(status)!==-1){
				id_list.push(rowIDs[index]);
		//}
		
	}
	//if (id_list.length===0){
		//Enterobase.modalAlert("There are no failed/killed jobs selected to resend. You cannot resend complete or running jobs");
		//return;
	//}
	var to_send={
		ids:id_list.join(","),
		database:this.species
	}
	Enterobase.call_restful_json("/species/"+this.species+"/resend_jobs","POST",to_send,callFailed).done(function(data){	
		Enterobase.modalAlert("Jobs have been resent - please refresh the page to see their status");	
	});

}




JobsGrid.prototype.addToContextMenu =  function (rowIndex,colIndex,target){
	var rowID = this.getRowId(rowIndex);
	var status_col = this.getColumnIndex("status")
    var status = this.getValueAt(rowIndex,status_col);
	var can_kill =  false;
	var can_download = false
	if (status === 'COMPLETE'){
		can_download = true;
	}
	if (this.finished_jobs.indexOf(status)===-1){
		can_kill =true
	}
	var colName = this.getColumnName(colIndex);
	
	var self = this;
	var extramenu =[
		{
			 name: 'Kill Selected Jobs',
			 title: 'Kill Seleceted Jobs',
			 fun: function () {
				 self.killJobs(Object.keys(self.selectedRows));
			 }
		 },
		 
		 {
			 name: 'Download Selected Outputs',
			 title: 'Download Selected Outputs',
			 fun: function () {	
				 self.downloadResults(Object.keys(self.selectedRows));
			 }
		 },
		 
		 {
			 name: 'Refresh Selected Jobs',
			 title: 'Refresh Selected Jobs',
			 fun: function () {	
				 self.refreshJobs(Object.keys(self.selectedRows));
			 }		 
		},
		 {
			 name: 'Resend Selected Jobs',
			 title: 'Resend Selected Jobs',
			 fun: function () {	
				 self.resendJobs(Object.keys(self.selectedRows));
			 }		 
		},
		{
			 name: 'Copy Selected To Clipboard',
			 title: 'Copy Selected To Clipboard',
			 fun: function () {	
				 self.copyRowsToClipboard(Object.keys(self.selectedRows));
			 }		 
		}
		
	];
	if (this.admin){
		extramenu.push(
			{
			 name: 'Force Reload on Selected',
			 title: 'Force Reload on Selected',
			 fun: function () {	
				 self.forceReload(Object.keys(self.selectedRows));
			 }		 
					
		
		});	
	}
	
	return extramenu;
};

