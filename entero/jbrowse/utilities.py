from Bio import Entrez,SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC
import os,json,shutil
from entero import app

jbrowse_bin = app.config['JBROWSE_BIN']
"""Points to the directory containing the executables used in formatting data for JBrowse.
    Usually it is the bin directory where JBrowse was installed
"""
jbrowse_repository='/share_space/jbrowse'
"""Points to the directory where the formatted files will be placed and can be accessed
    by the browser.
"""

#out directory

def concatanate_fasta_file(fasta_file,gff_file,out_directory,stub):
    records = list(SeqIO.parse(open(fasta_file),"fasta"))
    contig_to_offset={}
    total_sequence=""
    previous=0
    offset=0
    for rec in records:
        contig_to_offset[rec.id]=offset
        sequence = str(rec.seq)
        offset+=len(sequence)
        total_sequence+=sequence
    
    sequences = []
    record = SeqRecord(Seq(total_sequence, IUPAC.ambiguous_dna), id=stub)
    sequences.append(record)      
    out_file = os.path.join(out_directory,stub+".fasta")
    SeqIO.write(sequences, out_file, "fasta")
    out_handle=open(os.path.join(out_directory,stub+".gff"),"w")
    with open(os.path.join(out_directory,gff_file)) as f:
        for line in f:
            if line.startswith("#"):
                continue
            arr=line.strip().split("\t")
            offset = contig_to_offset[arr[0]]
            start = int(arr[3])
            end = int(arr[4])
            start=start+offset
            end=end+offset
            arr[3]=str(start)
            arr[4]=str(end)
            arr[0]=stub
            out_handle.write(("\t").join(arr)+"\n")
    out_handle.close()

def build_sequence_index(fasta_path,barcode,dir_name=False,overwrite=False):
    """
         Formats the fasta file so it can be displayed in JBrowse
        Args:
            fasta_path: The absolute path to the fasta file 
            
            barcode: Either the barcode of the assembly (in which case the output directory will be calcualted from the
            barcode ) or the the path of the output directory
            
            dir_name: If True then barcode contains the directtory name
            
            overwrite: If True all JBrowse data in the directory will be removed
    
        Returns:
            The sum of the two numbers
        
    """      
    if not dir_name:
        directory = __get_directory(barcode)
    else:
        directory = barcode
    if not os.path.exists(directory):
        os.makedirs(directory)
    else:
        if overwrite:
            shutil.rmtree(directory)
            os.makedirs(directory)
    command = "%sprepare-refseqs.pl --fasta %s --out %s"  % (jbrowse_bin,fasta_path,directory) 
    os.system(command)
    
def build_annotation_file(barcode,accession,fasta_directory,database='senterica'):
    
    directory = __get_directory(barcode)
    index = os.path.join(jbrowse_repository,"assembly_index.txt")
    file_handle = open(index,'r')
    lines= file_handle.readlines()
    lookup_dict={}
    for line in lines[1:]:
        arr =line.strip().split("\t")
        lookup_dict [arr[0].split('.')[0]]=arr[19]
        lookup_dict[arr[17].split('.')[0]]=arr[19]
    file_handle.close()
    file_address = lookup_dict.get(accession)
    
    if file_address:
        name = os.path.split(file_address)[1]
        download_file = os.path.join(file_address,name+"_genomic.gff.gz")
        print download_file
        gff_output = os.path.join(fasta_directory,barcode+".gff.gz")
        command = 'wget %s -O %s' % (download_file,gff_output)
        os.system(command)
        command = "gunzip %s" % gff_output
        os.system(command)
        fasta_file = os.path.join(fasta_directory,barcode+"_genomic.fna")
        fasta_handle = open(fasta_file)
        lines = fasta_handle.readlines()
        seq_names=[]
        for line in lines:
            if line.startswith(">"):
                seq_names.append(line[1:].split(" ")[0])
        fasta_handle.close()
        
        gff_output = gff_output[0:-3]
        gff_handle= open(gff_output)
        lines = gff_handle.readlines()
        altered=False
        for count,line in enumerate(lines):
            if line.startswith("#"):
                continue
            arr =line.split("\t")
            if  not arr[0] in seq_names:
                altered=True
                lines[count]=line.replace(arr[0],seq_names[0])
        gff_handle.close()
        if altered:
            gff_handle = open(gff_output,"w")
            for line in lines:
                gff_handle.write(line+"\n")
            gff_handle.close()
            
        
        
        
        command = "%sflatfile-to-json.pl -gff %s --tracklabel %s --key 'GenBank Annotation' --out %s" % (jbrowse_bin,gff_output,accession,directory)
        os.system(command)
        return gff_output
    return None



def create_snp_annotations(snp):
    folder = __get_directory(snp.ref_barcode)
    stub_name = str(snp.id)+".vcf.gz"
    file_name = os.path.join(folder,stub_name)
    shutil.copyfile(snp.data['vcf_file'],file_name)
    file_name=os.path.join(folder,stub_name+".tbi")
    shutil.copyfile(snp.data['vcf_file']+".tbi",file_name)
    tree_name = str(snp.id)+".nwk"
    file_name= os.path.join(folder,tree_name)
    shutil.copyfile(snp.data['raxml_tree'],file_name)
    snp_track={
         	"label"         : "snp_"+str(snp.id),
         	"key"           : snp.name+"(SNPs)",
                 "strain_label":"name",
         	"storeClass"    : "SNPViewer/Store/SeqFeature/MultiTabix",
         	"urlTemplate"   : stub_name,
         	"type"          : "SNPViewer/View/Track/SNPViewCanvas",
		"nwk_file"	     : tree_name,
                
                    "info": {
                        "SYN": {
                            "Frameshift":"red",
                            "Intergenic":"orange",
                            "NA":"lightgray", 
                            "Non-Synonymous":"blue",
                            "Synonymous":"green",
                            "STOP":"red"
                        }
                        },
                    "allowed_filters":
                        {
                            "NIF":["True"],
                            "SYN":["Intergenic","Non-Synonymous","Synonymous","STOP","Frameshift"]
                            },
		
      	}
    track ={"formatVersion":1,"tracks":[snp_track]}
    __save_tracks_json(snp.ref_barcode,track,"snps_"+str(snp.id)+".json")
    arr = snp.ref_barcode.split("_")
    loc = arr[0]+"/"+arr[1][0:4]+"/"+snp.ref_barcode
    snp.data['jbrowse_uri'] = app.config['JBROWSE_URI']+"?data=entero_data/"+loc+"&extra_include=snps_"+str(snp.id)               
    snp.update()
    
    
    
    

def make_snp_json(name,ref_barcode,barcodes,snp_project_id):
    tracks_wanted =set()
    for bar in barcodes:
        tracks_wanted.add(bar+"_aligned")
        tracks_wanted.add(bar+"_snp")
    
    data = __get_tracks_json(ref_barcode,"snps.json")
    snp_tracks=[]
    tracks = data['tracks']
    for track in tracks:
        if track['label'] in tracks_wanted:
            snp_tracks.append(track)
    put_data={"formatVersion":1,"tracks":snp_tracks}
    __save_tracks_json(ref_barcode,put_data,name+str(snp_project_id)+".json")
        
    
#bracode of reference file and list of gff_files
        
def build_snp_gff_files(barcode,gff_files,project_id):  
    directory = __get_directory(barcode)
    #create temporary folder to store the resuts
    temp = os.path.join(directory,"temp_"+barcode)
    if not os.path.exists(temp):
        os.mkdir(temp)
    temp_tracks=os.path.join(temp,"tracks")
    if not os.path.exists(temp_tracks):
        os.mkdir(temp_tracks)
    
    #create the necessary annotation 
    for pointer in gff_files:
        name = gff_files[pointer]
        try:
            name=str(name)
        except:
            name ='No parse'
        label = os.path.split(pointer)[1]
        
        command = "%sflatfile-to-json.pl -gff %s --trackType CanvasFeatures --tracklabel %s --key '%s' --out %s" % (jbrowse_bin,pointer,label,name,temp)
        os.system(command)
  
    
    
    #alter the default tracks.json fille and move it to main directory
    json_data = __get_tracks_json(temp,dir_name=True)
    tracks = json_data['tracks']
    snp_tracks=[]
    snp_data={"formatVersion":1,"tracks":snp_tracks}
    
    for track in tracks:
                      
        track['style'] = {"strandArrow":None,                             
                                    "color":"function(f){var base = f.get('/replace');\n if (base && base.length ===3) {base = base.substring(1,base.length-1);\n return JBrowseUtilities.baseColours[base];}\n return '#D8D8D8 ';}",
                                    "borderColor":"function(f){ return JBrowseUtilities.getContigBorder(f)}",
                                    "borderWidth":2                                                              
                          }
        track['displayMode']='collapsed'
        track['category']='snp'
        snp_tracks.append(track)
               
    __save_tracks_json(barcode,snp_data,"snps_"+str(project_id)+".json")
    
    
    #move all the directories from temp
    dst = os.path.join(directory,"tracks")
    folders = os.listdir(temp_tracks)
    for folder in folders:
        src = os.path.join(temp_tracks,folder)
        shutil.move(src, dst)
    
    
    #remove all temporary files
    shutil.rmtree(temp)
    for pointer in gff_files:
        if os.path.exists(pointer):
            os.remove(pointer)

#gff_files name:file_ponter:label   
def build_gff_files(barcode,gff_files,dir_name=False):
    if not dir_name:
        directory = __get_directory(barcode)
    else:
        directory = barcode
   
    for name in gff_files:    
        label = gff_files[name][1]
        pointer = gff_files[name][0]         
        command = "%sflatfile-to-json.pl -gff %s --trackType CanvasFeatures --tracklabel %s --key '%s' --out %s" % (jbrowse_bin,pointer,name,label,directory)
        os.system(command)
        highlight_callback={"className":"feature","color":"function(f){return JBrowseUtilities.setSelectedColor(f)}"}
        new_data = {"label":name,"style":highlight_callback}
        alter_track(barcode, new_data)
     
def build_names(barcode,dir_name=False):
    if not dir_name:
        directory = __get_directory(barcode)
    else:
        directory = barcode
    command = "chmod 777 %s" % directory
    #os.system(command)
    command="%sgenerate-names.pl --out %s"% (jbrowse_bin,directory)
    os.system(command)
    command = "chmod 775 %s" % directory
    #os.system(command)    


def addGCRatioTrack(barcode):
    tracks=__get_tracks_json(barcode)
    gc_ratio={  
                "storeClass" : "JBrowse/Store/SeqFeature/SequenceChunks",
                "type": "GCContent/View/Track/GCContentXY",
                "label" : "GCContentXY",
                "urlTemplate" : "seq/{refseq_dirpath}/{refseq}-",
                "bicolor_pivot": 0.5     
    }
    if tracks.get('tracks'):
        tracks['tracks'].append(gc_ratio)
    __save_tracks_json(barcode, tracks)
    
   
def build_quality_file(fastq_path,barcode):
    directory = os.path.split(fastq_path)[0]
    wigfile=os.path.join(directory,barcode+".wig")
    sizesfile = os.path.join(directory,barcode+".sizes")
    sizesfile_handle=open(sizesfile,"w")
    wigfile_handle  = open(wigfile,"w")
    wigfile_handle.write("track type=wiggle_0\n")
    if not os.path.exists(fastq_path):
        app.logger.error("build_quality_file failed, fastq_path %s is not found"%fastq_path)
        return    
    handle = open(fastq_path,"r")
    lines = handle.readlines()
    seq_name= ""
    for count,line in enumerate(lines,start=1):
        if count%4==1:
            seq_name= line.strip()[1:].split()[0]
            wigfile_handle.write("variableStep chrom=%s\n" % seq_name)
        if count%4==2:
            sizesfile_handle.write(seq_name+"\t"+str(len(line.strip()))+"\n")
        if count%4==0:
            quals = line.strip()
            for i in range(0,len(quals)):
               
                qual =  80 - (6+ord(quals[i]))
                if qual <> 0:
                    pos =i+1
                    wigfile_handle.write(str(pos)+"\t"+str(qual)+"\n")
       
    wigfile_handle.close()
    sizesfile_handle.close()
    handle.close()
    
    directory = __get_directory(barcode)
    bigwigfile = os.path.join(directory,barcode+".bwg")
    command = "%swigToBigWig %s %s %s " % (jbrowse_bin,wigfile,sizesfile,bigwigfile)
    os.system(command)
    tracks_file = os.path.join(directory,"trackList.json")
    tracks = None
    if not os.path.exists(tracks_file):
        app.logger.error("build_quality_file failed, file %s is not found"%tracks_file)
        return
    with open(tracks_file) as json_handle:
        tracks= json.load(json_handle)
        tracks['tracks'].append({
            "label"         : "assembly_quality",
            "key"           : "Assembly Errors",
            "storeClass"    : "JBrowse/Store/SeqFeature/BigWig",
            "urlTemplate"   : barcode+".bwg",
            "type"          : "JBrowse/View/Track/Wiggle/XYPlot",
            "min_score"     : 0,
            "max_score"     : 41,
        });
    
    tracks_out = open(tracks_file,"w")
    tracks_out.write(json.dumps(tracks,indent=4,sort_keys=True))
    tracks_out.close()
    
def alter_track(barcode,new_data,name="trackList.json"):
    json_data = __get_tracks_json(barcode,name)
    tracks = json_data['tracks']
    for track in tracks:
        if track['label'] == new_data['label']:
            for key in new_data:
                track[key]=new_data[key]
            break
    __save_tracks_json(barcode,json_data,name)
    

def __get_tracks_json(barcode,name="trackList.json",dir_name=False):
    if not dir_name:
        directory = __get_directory(barcode)
    else:
        directory=barcode
    tracks_file = os.path.join(directory,name)
    tracks = None
    try:
        with open(tracks_file) as json_handle:
            tracks= json.load(json_handle)
    except:
        return {"formatVersion":1}
    return tracks

def __save_tracks_json(barcode,tracks,name = "trackList.json",dir_name=False):
    if not dir_name:
        directory = __get_directory(barcode)
    else:
        directory=barcode    
    tracks_file = os.path.join(directory,name)
    tracks_out = open(tracks_file,"w")
    tracks_out.write(json.dumps(tracks,indent=4,sort_keys=True))
    tracks_out.close()
    
   
def __get_directory(barcode):
    arr = barcode.split("_")
    directory = os.path.join(jbrowse_repository,arr[0],arr[1][0:4],barcode)
    return directory

def download_genome(accession,directory):
    index = os.path.join(jbrowse_repository,"assembly_index.txt")
    file_handle = open(index,'r')
    lines= file_handle.readlines()
    lookup_dict={}
    for line in lines[1:]:
        arr =line.strip().split("\t")
        lookup_dict [arr[0].split('.')[0]]=arr[19]
        lookup_dict[arr[17].split('.')[0]]=arr[19]
    file_handle.close()
    file_address = lookup_dict.get(accession) 
    if file_address:
        name = os.path.split(file_address)[1]
        download_file = os.path.join(file_address,name+"_genomic.fna.gz")
        print download_file
        gff_output = os.path.join(directory,accession+".fna.gz")
        command = 'wget %s -O %s' % (download_file,gff_output)
        os.system(command)
        command = "gunzip %s" % gff_output
        gff_output = gff_output[0:-3]
        os.system(command)
       
        return gff_output 
    return None