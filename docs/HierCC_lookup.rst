HierCC equivalents
==============================

*Salmonella* database
------------------
* **HC900 & HC2000:** HC900 is equivalent to eBG (eBurstGroup) in legacy 7-gene MLST, which is shown to be natural population that is consistent with serovars. Thus we have given HC900 a trivial name as "ceBG" (cgMLST eBurstGroup). We also exploited a further clustering of cgSTs at HC2000 level. salmonellae of different eBG groups (serovars) grouped together at this level. 

`A mapping table between HC900 and HC2000 versus serovars <https://bitbucket.org/enterobase/enterobase-web/raw/b7444ac566bfd59c60e79ec506f290c19e2f5eaa/docs/files/Salmonella_HC900_lookup.xlsx>`_

* **HC2850:** HC2850 in *Salmonella* corresponds to the separation of subspecies and species in the genus. 

`A mapping table between HC2850 and Salmonella species and Clades <https://bitbucket.org/enterobase/enterobase-web/raw/5fbd7da27e367cf7e4c2fe93a164687b1a77f73a/docs/files/Salmonella_HC2850_vs_subsp.xlsx>`_

*Escherichia* database
------------------
* **HC2350:** HC2350 in *Escherichia* corresponds to the separation of species and clades in *Escherichia* genus. 

`A mapping table between HC2350 and Escherichia species and Clades <https://bitbucket.org/enterobase/enterobase-web/raw/b7444ac566bfd59c60e79ec506f290c19e2f5eaa/docs/files/Escherichia_HC2350_lookup.xlsx>`_

*Yersinia* database
------------------
* **HC1450:** HC1450 in *Yersinia* corresponds to the separation of species of the genus and the Species Complexes defined in `Reuter et al. PNAS <https://www.pnas.org/content/111/18/6768>`_. 

`A mapping table between HC1450 and Yersinia species <https://bitbucket.org/enterobase/enterobase-web/raw/14407f14271b9a68fb4683f4a0bb3ec8c6f8f9ae/docs/files/Yersinia_HC1450_vs_species.xlsx>`_
