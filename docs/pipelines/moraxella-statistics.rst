Details of assembly methods and in silico genotyping for *Moraxella*
====================================================================

* **All MLST-like typing methods in EnteroBase are derived from a genome assembly of sequenced reads. For an explanation of this method**, see :doc:`here </pipelines/backend-pipeline-qassembly>`
* **For a general description of the in silico typing method**, see :doc:`here </pipelines/backend-pipeline-nomenclature>`



======================================= ===================================
MLST – Classic                          Ribosomal MLST (Jolley, 2012)
======================================= ===================================
8 Loci                                  53 Loci
Conserved Housekeeping genes            Ribosomal proteins
Highly conserved; Low resolution        Highly conserved; Medium resolution
Different scheme for each species/genus Single scheme across tree of life
======================================= ===================================

Note that more schemes are currently implemented for `Salmonella`_,
`Escherichia`_ and `Yersinia`_ (and it may be worth reviewing the
relevant pages in the documentation for a broader comparison of
schemes).

8 Gene MLST
-----------

Classic MLST scheme is described in `Wirth et al (2007) Genome Res. 17:
1647-1656`_.

Genes included in 8 gene MLST (together with the length of sequence used
for MLST taken from table 1 in the above cited paper):

======= ======
Gene    Length
======= ======
ppa     393
mutY    426
trpE    372
efp     414
fumC    465
glyBeta 537
adk     471
abcZ    429
======= ======

Ribosomal MLST (rMLST)
----------------------

-  **rMLST** is Copyright © 2010-2016, University of Oxford. rMLST is
   described in `Jolley et al. 2012 Microbiology 158:1005-15`_.

.. _please click here: EnteroBase%20Backend%20Pipeline%3A%20nomenclature
.. _Salmonella: Salmonella%20Statistics
.. _Escherichia: Escherichia%20Statistics
.. _Yersinia: Yersinia%20Statistics
.. _`Wirth et al (2007) Genome Res. 17: 1647-1656`: http://mlst.warwick.ac.uk/mlst/AGroup/team/markPDF/37.pdf
.. _`Jolley et al. 2012 Microbiology 158:1005-15`: http://www.ncbi.nlm.nih.gov/pubmed/22282518