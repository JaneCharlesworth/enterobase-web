#!/bin/sh
myname=admin
home_dir=/home/admin/enterobase-web/
date >> $home_dir/logs/entero.log
cd $home_dir
echo $home_dir
#high priority (0 by default)
#all jobs sent to 'backend' by default, but this can changed
for dbase in senterica ecoli yersinia mcatarrhalis clostridium
do
        echo "sending assembly jobs $dbase\n" >> "$home_dir/logs/entero.log"
        /home/admin/venv/bin/python $home_dir/manage.py update_assemblies  -l 75  -d $dbase  
        /home/admin/venv/bin/python $home_dir/manage.py update_all_schemes  -l 75 -d $dbase  
        echo "Importing from SRA for  $dbase\n" >> $home_dir/logs/entero.log
        /home/admin/venv/bin/python $home_dir/manage.py importSRA -r 10 -d $dbase
        /home/admin/venv/bin/python $home_dir/manage.py check_queued_assemblies -d $dbase
    #    /home/admin/venv/bin/python $home_dir/manage.py  generate_strain_table_dump -d $dbase

done

#low priority (-p 9)
for dbase in klebsiella listeria vibrio helicobacter photorhabdus
do
        echo "sending assembly jobs $dbase\n" >> $home_dir/logs/entero.log
        /home/admin/venv/bin/python $home_dir/manage.py update_assemblies -p 9 -l 75  -d $dbase  
        /home/admin/venv/bin/python manage.py update_all_schemes -p 9 -l 75 -d $dbase  
        echo "Importing from SRA for  $dbase\n" >> $home_dir/logs/entero.log
        /home/admin/venv/bin/python $home_dir/manage.py importSRA -r 10 -d $dbase
        /home/admin/venv/bin/python $home_dir/manage.py check_queued_assemblies -d $dbase
done