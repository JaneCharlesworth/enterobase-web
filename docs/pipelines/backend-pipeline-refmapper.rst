Top level links: 

* **[Main top level page for all documentation](Home)**
* **[EnteroBase Features](Home)**
* **[Registering on EnteroBase and logging in](Enterobase%20website)**
* **[Tutorials](Tutorials)**
* **[Using the API](About%20the%20API)**
* **[About the underlying pipelines and other internals](EnteroBase Backend Pipeline)**
* **[How schemes in EnteroBase work](About%20EnteroBase%20Schemes)**
* **[FAQ](FAQ)**

# refMapper #

[TOC]

# Overview #

refMapper aligns multiple assemblies against a single reference assembly, using lastal and
lastdb from the [LAST] package (version 5.88).

The refMapper pipeline is usually invoked as part of a
[workflow to create a SNP tree].

refMapper is currently in version 1.0.

# Summary of pipeline #
refMapper prepares the reference assembly sequence for subsequent comparison with the other
assemblies by running lastdb.  Then a multithreaded job is started with a number of alignments
consisting of pairwise comparisons of the other assemblies, each with the reference assembly.
The alignments are carried out by the program lastal from the [LAST] package.
(These are multiple sequence alignments since each assembly in these pairwise comparisons may
consist of a number of sequences from its contigs.)  The alignments are subsequently parsed
and interpreted in order to identify mutations and also regions that successfully align with
the reference, repetitive regions and regions with uncertain base calling or ambiguous alignment.
The result is written out in [GFF] format for each assembly aligned to the reference.

![refMapper3quasicropresize2.png](https://bitbucket.org/repo/Xyayxn/images/460757782-refMapper3quasicropresize2.png)

  [LAST]: http://last.cbrc.jp/ "external link"
  [GFF]: http://en.wikipedia.org/wiki/General_feature_format "external link"
  [workflow to create a SNP tree]: EnteroBase%20Backend%20Pipeline#markdown-header-snp-trees