AnnotationGrid.prototype = Object.create(BaseExperimentGrid.prototype);
AnnotationGrid.prototype.constructor= AnnotationGrid;

function AnnotationGrid(name,config,species,scheme,scheme_name){
	BaseExperimentGrid.call(this,name,config,species,scheme,scheme_name);
	this.windows_opened=0;
	var self = this;
	this.currentDate= new Date().getTime();
	this.download_url= '/upload/download';
	this.addExtraRenderer("gff_file",function(cell,value,rowID){
		if (value && !value.startsWith ("&nbsp") ){
			$(cell).html($("<span><img src='/static/img/upload/download.gif' width='"+self.fontSize+"' height='"+self.fontSize+"'></img></span>")).css("cursor","pointer");
		}  
	
	});
	this.addExtraRenderer("gbk_file",function(cell,value,rowID){
		if (value && !value.startsWith ("&nbsp") ){
			$(cell).html($("<span><img src='/static/img/upload/download.gif' width='"+self.fontSize+"' height='"+self.fontSize+"'></img></span>")).css("cursor","pointer");
		}  
	
	});
	this.addCustomColumnHandler("gbk_file",function(cell,rowIndex,rowID){
		
		var value = self.getValueAt(rowIndex,self.getColumnIndex("gbk_file"));
		if (!value || value.startsWith(' &nbsp')){
			return;
		}
		self.downloadFiles([rowID],"gbk_file")
		
	});
	this.addCustomColumnHandler("gff_file",function(cell,rowIndex,rowID){
		var value = self.getValueAt(rowIndex,self.getColumnIndex("gff_file"));
		if (!value || value.startsWith('&nbsp')){
			return;
		}
		self.downloadFiles([rowID],"gff_file")		
	});
		
	
};




AnnotationGrid.prototype.downloadFiles= function(Ids,type){
	
	var downloaded = false;
	var msg = "<ul>";
	var downloaded =0;
	for (index in Ids){
		var rowID =Ids[index];
		var rowIndex = this.getRowIndex(rowID);
		var best_ass = this.strainGrid.best_assembly[rowID];
		var location = this.getValueAt(rowIndex,this.getColumnIndex(type))
		var strainName = this.strainGrid.getValueAt(rowIndex,this.strainGrid.getColumnIndex("strain"));
		var releaseDate = this.strainGrid.getValueAt(rowIndex,this.strainGrid.getColumnIndex("release_date"));
		var is_Downloadable=this.strainGrid.isDownloadable[rowID];
		if (!location || location === '  '){
			msg+="<li>"+strainName+" has no associated file </li>";
			continue;
		}
		else{

			//if (new Date(releaseDate).getTime() > this.currentDate && this.nonEditableRows[rowID]){			
			if (new Date(releaseDate).getTime() > new Date().getTime() && !is_Downloadable){
				msg+="<li>"+strainName+" is private until "+releaseDate+"</li>";
			}
			else if (!is_Downloadable){
				msg+="<li> You are not allowed to download this strain: </li>";
			}
			else{
				Enterobase.downloadURL(this.download_url+"?assembly_id="+best_ass+"&database="+this.species+"&location="+location);
				downloaded++;
			}
		}
		
		
	}
	msg+="<ul>"
	msg = downloaded + " File(s) have been downloaded<br>"+msg;
	Enterobase.modalAlert(msg);
}

AnnotationGrid.prototype.addToContextMenu =  function (rowIndex,colIndex,target){
		var id = this.getRowId(rowIndex);
		var self = this;
		var disable =true;
		
		var extramenu=[	{
						name: 'Download Selected gbk Files',
						title: 'Download Selected gbk Files',
						fun: function () {
							self.downloadFiles(self.strainGrid.getSelectedRows(),"gbk_file");    
						}
						
				},
				{
						name: 'Download Selected gff Files',
						title: 'Download Selected gff Files',
						fun: function () {
							self.downloadFiles(self.strainGrid.getSelectedRows(),"gff_file");    
						}
						
				}
		];
			
		return extramenu;
}

AnnotationGrid.prototype.setMetaData =  function (data){
		data.push({
				datatype:"text",
				display_order:3,
				name:"view_data",
				label:"<span class='glyphicon glyphicon-eye-open style='padding:0px;margin:0px;display:inline-block;''></span>",
				not_write:true
		});
		var self = this;

		this.addExtraRenderer("view_data",function(cell,value,row_id,col_name,row_index){
				if (self.data[row_index].values['gff_file']){
					$(cell).html("<span class='glyphicon glyphicon-eye-open style='padding:0px;margin:0px;display:inline-block;font-size:+"+self.fontSize+"px'></span>")                            
					.css("cursor","pointer");
				}
		});

		this.addCustomColumnHandler("view_data",function(cell,row_index,row_id){
			if (self.strainGrid.isGenomePrivate(row_id)){
				Enterobase.modalAlert("This genome is private and cannot be viewed");
			}
			else if (!self.strainGrid.isDownloadable[row_id]){
				Enterobase.modalAlert("You are not allowed to view this genome");
			}
			else{
				var ass_id = self.strainGrid.best_assembly[row_id];
				if (ass_id){
					var win_name = "jbrowse"+self.windows_opened;
					self.windows_opened++;
					var args ="?assembly_id="+ass_id+"&database="+self.species;
					var win = window.open("/view_jbrowse_annotation"+args,win_name,'width=1000, height=600');
				}
			}
		});

		//call the super method
		BaseExperimentGrid.prototype.setMetaData.call(this,data);
		this.columnWidths["view_data"]=20;
}