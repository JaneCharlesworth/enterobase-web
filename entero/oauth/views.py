from entero.entero_email import sendmail
from flask_login import current_user
from .. import db
from ..databases.system.models import User, LocalClient
from flask import Blueprint, request, session, url_for
from flask import render_template, redirect, jsonify
from werkzeug.security import gen_salt
from authlib.flask.oauth2 import current_token
from authlib.oauth2 import OAuth2Error
from .oauth_utils import authorization, require_oauth, check_client, check_client_registration
from flask_login import login_required, fresh_login_required
from . import oauth
from forms import Create_Client_Form
import json
from .. import app


from oauth_utils import save_user_confirmation, check_user_confirmation, save_upload_test_results_to_database

@oauth.route('/check_client_register', methods=['POST', 'GET'])
def check_client_data():
    '''
    a request from local machine to check the client name and uri
    :return:
    '''
    data = request.args.get('data')
    attributes = json.loads(data)
    return json.dumps(check_client_attributes(attributes))

def check_client_attributes(attributes):
    '''
    this method is used to check client name and url to b sure both are unique
    :param attributes: dict cotains client_name and client_uri values
    :return: OK if the field is not used before otherwise return error message
    '''
    results = {}
    if attributes.get('client_name'):
        res_1 = LocalClient.query.filter_by(client_name=attributes.get('client_name')).first()
        if res_1:
            results['client_name'] = "Client name already in use"
        else:
            results['client_name'] = "OK"
    if attributes.get('client_uri'):
        res = LocalClient.query.filter_by(client_uri=attributes.get('client_uri')).first()
        if res:
            results['client_uri'] = "Client URL already registered"
        else:
            results['client_uri'] = "OK"
    return results

@oauth.route('/client_register_request', methods=['POST', 'GET'])
@login_required
def display_client():
    '''
    recieve a registration request from the local installation
    :return:
    '''
    user = current_user
    client = LocalClient()
    form = Create_Client_Form()
    data = request.args.get('data')
    attributes = json.loads(data)
    results=check_client_attributes(attributes)
    is_ok=True
    for key in results:
        if results[key]=='OK':
           continue
        is_ok=False
    if is_ok:
        client.client_uri = attributes.get("client_uri")
        client.client_name = attributes.get("client_name")
        client.description = attributes.get("description")
        client.redirect_uri = str(client.redirect_uri).strip()
        # toDo check the uri and redirect uri to be sure that they strat with https (secure connection)
        client.redirect_uri = client.client_uri + "/authorized"
        client.token_endpoint_auth_method = 'client_secret_post'
        client.user_id = user.id
        client.response_type = 'code'
        client.grant_type = 'authorization_code'
        client.scope = 'Username, Email, Submitting Assemblies'
        client.client_id = gen_salt(24)
        if client.token_endpoint_auth_method == 'none':
            client.client_secret = ''
        else:
            client.client_secret = gen_salt(48)
        db.session.add(client)
        db.session.commit()
        # send an email to the admin to inform him
        try:
            subject="New local registration request"
            body="A new request has been recieved from: %s@%s, user email is %s, the client name is:%s, the client secret: %s, and id: %s, please check the request"%(user.username,client.client_uri, user.email, str(client.client_name), client.client_secret, client.client_id)
            sendmail(to="khaled.mohamed@warwick.ac.uk", subject=subject, txt=body)
            ###############
        except Exception as e:
            app.logger.exception("Error while sending email to register a local clienr, error message:%s"%e.message)

        ##send an email to the user who register the local client with a token:
        try:
            subject = "%s Registration"%client.client_name
            body = "Dear %s:\n Thank you for submitting a arequest to register your client. " \
                   "As a part of the registration process, you need to test uploading files from you client to Warwick EnteroBase. Please update your system configuration using the following token '%s'\n Kind regards \nEnteroBase Administrator" % (
            user.firstname, client.client_id)
            sendmail(to=user.email, subject=subject, txt=body)
            ###############
        except Exception as e:
            app.logger.exception("Error while sending email to register a local client, error message:%s" % e.message)

        form.client_uri.data = attributes.get("client_uri")
        form.client_name.data = attributes.get("client_name")
        form.description.data = attributes.get("description")
        return render_template('oauth/display_regist_client_form.html', form=form, local_url=client.client_uri)
    else:
        return json.dumps(results)

##@oauth.route('/register_client', methods=['GET', 'POST'])
##@login_required
def create_client():
    form = Create_Client_Form()

    user = current_user
    if not user:
        return redirect('/')
    if request.method == 'GET':
        return render_template('oauth/register_client.html', form=form)

        #return render_template('register_client.html')
    if form.validate_on_submit():
        data=request.form.to_dict(flat=True)
        if 'submit' in data:
            data.pop('submit')
        client = LocalClient(**data)
        client.redirect_uri=str(client.redirect_uri).strip()
        #toDo check the uri and redirect uri to be sure that they strat with https (secure connection)
        #tis should be done from the Create_Client_Form
        client.client_uri=str(client.client_uri).strip()
        client.redirect_uri=client.client_uri+"/authorized"
        client.token_endpoint_auth_method = 'client_secret_post'
        client.user_id = user.id
        client.response_type='code'
        client.grant_type = 'authorization_code'
        client.scope = 'Username, Email, Submitting Assemblies'
        client.client_id = gen_salt(24)
        #if client.token_endpoint_auth_method == 'none':
        #    client.client_secret = ''
        #else:
        #    client.client_secret = gen_salt(48)
        #db.session.add(client)
        #db.session.commit()
        return redirect(url_for('main.index'))

    if not form.is_submitted():
        client = LocalClient()
        client.client_uri = request.args.get("redirect_uri")
        client.client_name = request.args.get("client_name")
        client.description = request.args.get("description")
        client.redirect_uri = str(client.redirect_uri).strip()
        # toDo check the uri and redirect uri to be sure that they strat with https (secure connection)
        # tis should be done from the Create_Client_Form
        client.client_uri = str(client.client_uri).strip()
        client.redirect_uri = client.client_uri + "/authorized"
        client.token_endpoint_auth_method = 'client_secret_post'
        client.user_id = user.id
        client.response_type = 'code'
        client.grant_type = 'authorization_code'
        client.scope = 'Username, Email, Submitting Assemblies'
        client.client_id = gen_salt(24)
        if client.token_endpoint_auth_method == 'none':
            client.client_secret = ''
        else:
            client.client_secret = gen_salt(48)
        #db.session.add(client)
        #db.session.commit()
        #toDO sent an email to the admin to inform him
        form.client_uri = request.args.get("redirect_uri")
        form.client_name = request.args.get("client_name")
        form.description = request.args.get("description")
    return render_template('oauth/register_client.html', form=form)


@oauth.route('/is_client_registered', methods=['GET', 'POST'])
def check_if_client_is_registered():
    client_id=request.args.get('client_id')
    client_secret=request.args.get('client_secret')
    redirect_uri=request.args.get('redirect_uri')
    if not client_id or not clienis_registerd_request_submittedt_secret or not redirect_uri:
        return "False"
    return check_client_registration(client_id, client_secret, redirect_uri)



@oauth.route('/authorize', methods=['GET', 'POST'])
@fresh_login_required
def authorize():
    app.logger.warning(request.url)
    user = current_user
    is_confirm_before=False
    url = request.base_url
    ''''
     this is a temporary solution untill figure out why the url converted from https to http
     which I think it maybe a nginx configuration issue 
    '''
    if url.lower().startswith('http://'):
        request.base_url=url.replace("http://","https://");


    if request.method == 'GET':
        try:
            grant = authorization.validate_consent_request(end_user=user)
            scopes=grant.request.scope.split(',')
            #check if the user has authorize the client before
            is_confirm_before=check_user_confirmation(request.args.get('client_id'), user.id)
        #save_user_confirmation, check_user_confirmation
        except OAuth2Error as error:
            app.logger.exception("Authorize error message: %s" % error.error)
            print error.error
            return redirect(request.args.get("redirect_uri"),302)
            #return error.error
        #usergrantauthorizationcode
        if not is_confirm_before:
            return render_template('oauth/authorize.html', user=user, grant=grant, scopes=scopes)
    if not user and 'username' in request.form:
        username = request.form.get('username')
        user = User.query.filter_by(username=username).first()
    if request.method == 'POST':
        if not is_confirm_before and request.form.get('confirm'):
            save_user_confirmation(request.args.get('client_id'), user.id)
        else:
            return ("User %s has denied your application access to Warwick EnteroBase in his behalf" % user.username)
    grant_user = user
    ttt=authorization.create_authorization_response(grant_user=grant_user)
    return ttt


@oauth.route('/authorized', methods=['GET', 'POST'])
#@login_required
def authorized  ():
    return ("current_token")

@oauth.route('/token', methods=['GET', 'POST'])
def issue_token():
    #print request.__dict__
    #try:
    url = request.base_url
    ''''
     this is a temporary solution untill figure out why the url converted from https to http
     which I think it maybe a nginx configuration issue 
    '''
    if url.lower().startswith('http://'):
        request.base_url = url.replace("http://", "https://");
    return authorization.create_token_response()
    #except:
    #    return "Invalid request ..."

@oauth.route('/revoke', methods=['POST'])
def revoke_token():
    return authorization.create_endpoint_response('revocation')

@oauth.route('/userinfo')
@require_oauth()
def get_user_info_for_oauth():
    #current_token.client_id
    user = current_token.user
    is_local_admin=check_client(current_token.client_id, user.id)
    return jsonify(id=user.id, username=user.username, email=user.email, is_local_admin=is_local_admin)

@oauth.route('/is_registerd_request_submitted', methods=['POST', 'GET'])
def is_client_registered():
    try:
        token = request.form.get('token')
        res=check_client(token)
    except:
        res='False'
    return res

@oauth.route('/save_upload_test_results', methods=['POST', 'GET'])
def save_upload_test_results():
    '''
    Save the client upload files test results which includes
    no of files used in the test
    average upload time
    it will be saved using clien_id  which will be sent with the request as a token
    :return:
    '''
    try:
        token = request.form.get('token')
        no_test_uploaded_files = request.form.get('no_test_uploaded_files')
        ave_uploaded_time = request.form.get('ave_uploaded_time')
        if token and check_client(token):
            print token, no_test_uploaded_files, ave_uploaded_time
            res=save_upload_test_results_to_database(token, no_test_uploaded_files, ave_uploaded_time)
            ##send an email to adminstrator to inform him that the client performed uploading files test
            try:
                subject = "Client test upload files results"
                body = "Local client_id %s has uploaded its upload files test results, no of uploaded files is %s and average upload time is %s"%(token, no_test_uploaded_files, ave_uploaded_time)
                sendmail(to="khaled.mohamed@warwick.ac.uk", subject=subject, txt=body)
                ###############
            except Exception as e:
                app.logger.exception(
                    "Error while sending email to register a local clienr, error message:%s" % e.message)

    except Exception as e:
        app.logger.exception("Error while saving upload test results, error message: %s"%e.message)

        res='False'
    return res