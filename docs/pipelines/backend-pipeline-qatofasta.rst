Top level links: 

* **[Main top level page for all documentation](Home)**
* **[EnteroBase Features](Home)**
* **[Registering on EnteroBase and logging in](Enterobase%20website)**
* **[Tutorials](Tutorials)**
* **[Using the API](About%20the%20API)**
* **[About the underlying pipelines and other internals](EnteroBase Backend Pipeline)**
* **[How schemes in EnteroBase work](About%20EnteroBase%20Schemes)**
* **[FAQ](FAQ)**

# QAtoFasta #

[TOC]

# Overview #

QAtoFasta transforms QAFastq format into standard [FASTA] format files, which is widely used in 3rd party software.

Two different quality values are suggested for masking low quality regions in the [FASTA] files.

1.  Q >= 0. This will maintain all consensus calls from the assembly. This transform is suggested for gene predictions and similarity searches.
2.  Q >= 10. This will mask all the low quality bases. This transform is suggested for SNP calling or highly accurate MLST typing.

The [QAssembly] pipeline runs QAtoFasta in order to process the assemblies generated by [QAssembly] with Q >= 0,
prior to running the [QA evaluation] pipeline.  (However, the [QA evaluation] pipeline processes the sequence
to obtain the masked sequence that is made available to [download](user_download_assemblies) in [FASTA] format itself, without running the code in QAFasta.)

The current version of QAtoFasta is 1.0.

# Inputs/Outputs #

### Parameters ###

    {
        "params": {
            "low_qual": 10
        },
        "inputs": {
            "fastq": "SAL_AA0001AA_AS.scaffold.fastq"
        },
        "outputs": {
            "fasta": "SAL_AA0001AA_AS.scaffold.fasta"
        }
    }

### Outputs ###

    {
        "outputs": {
            "fasta": ["SAL_AA0001AA_AS.scaffold.fasta", "/path/to/folder/SAL_AA0001AA_AS.scaffold.fasta" ]
        }
    }

  [QAssembly]: EnteroBase%20Backend%20Pipeline%3A%20QAssembly
  [QA evaluation]: EnteroBase%20Backend%20Pipeline%3A%20QA%20evaluation
  [available for users to download]: available_for_users_to_download
  [FASTA]: http://en.wikipedia.org/wiki/FASTA_format "external link"