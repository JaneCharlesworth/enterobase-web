EnteroBase API Endpoint Reference
=================================
The API can be explored using the [interactive documentation](http://enterobase.warwick.ac.uk/api/v2.0/swagger-ui "external link").  The query methods currently in the API are:

Refer to interactive documentation : http://enterobase.warwick.ac.uk/api/v2.0/swagger-ui#!/Alleles

The ``alleles`` query method shows sequences for loci, derived from an assembly.

An example of this type of query to try in the interactive documentation which is also
on [the page about downloading schemes](api_download_schemes)
has "database" set to "senterica", "scheme" set to "MLST_Achtman",
"reldate" set to "20", "locus" set to "aroC" and "limit" set to "50".  This will fetch new allele
sequences for aroC in the 7 gene MLST scheme for Salmonella from the last 20 days. The corresponds
to an HTTP request to the URL:

.. code-block:: html

    http://enterobase.warwick.ac.uk/api/v2.0/senterica/MLST_Achtman/alleles?reldate=20&locus=aroC&limit=50

The result is:

.. code-block:: json

    {
      "alleles": [
        {
          "allele_barcode": "SAL_AP0358AA_AL",
          "allele_id": "716",
          "locus": "aroC",
          "seq": "GTGTTCCGTCCGGGGCACGCGGATTACACCTACGAACAAAAATACGGCCTGCGCGACTATCGCGGCGGCGGGCGTTCATCCGCCCGTGAAACCGCCATGCGCGTCGCGGCAGGCGCTATCGCCAAAAAATATCTGGCGCAGAAATTCGGCGTGGTGATTCGCGGCTGCCTGACCCAGATGGGTGATATTCCGCTGGAAATCAAAGACTGGGATCAGGTAGAGCAAAACCCGTTCTTCTGCCCGGACCCGGATAAAATCGAGGCGCTGGATGAGCTGATGCGCGCTCTGAAAAAAGAGGGCGATTCCATCGGCGCGAAAGTCACCGTGGTGGCCGACAGCGTGCCCGCCGGGCTTGGCGAGCCGGTATTTGACCGCCTGGACGCCGATATCGCCCACGCGCTGATGAGCATTAACGCCGTGAAGGGCGTGGAAATCGGCGACGGTTTCGGCGTGGTGCAACTGCGCGGCAGCCAGAACCGCGACGAAATCACCACTGCCGGT"
        }
      ],
      "links": {
        "paging": {},
        "records": 1,
        "total_records": 1
      }
    }

Note that as discussed in [the notes about downloading schemes](api_download_schemes) it is inadvisable to download
the allele sequences in their entirety using the above method, rather to download a tarball with the daily dump of all data and update with new information at regular intervals using queries like the one above as is described on [that page](api_download_schemes).

The assemblies query method
---------------------------
Refer to interactive documentation : http://enterobase.warwick.ac.uk/api/v2.0/swagger-ui#!/Assemblies

The ``assemblies`` query method shows metadata for genome assemblies including N50, coverage, size, and links
to FASTA contigs.

An example of this type of query to try in the interactive documentation has "database"
set to "senterica", "orderby" set to "barcode", "limit" set to "2", reldate set to "10",
"assembly_status" set to "Assembled" and
"sortorder" set to "asc" and corresponds to an HTTP request to the URL

.. code-block:: html
    http://enterobase.warwick.ac.uk/api/v2.0/senterica/assemblies?limit=2&reldate=10&assembly_status=Assembled&sortorder=asc

which (at the time of the query) obtained the result

.. code-block:: json

    {
      "Assemblies": [
        {
          "assembly_barcode": "SAL_IA5204AA_AS",
          "assembly_status": "Assembled",
          "coverage": null,
          "download_fasta_link": "http://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_IA5204AA_AS&database=senterica",
          "download_fastq_link": "http://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_IA5204AA_AS&database=senterica&file_format=fastq",
          "id": 85204,
          "lastmodified": "2017-05-01T22:02:56.892765+00:00",
          "n50": 406329,
          "top_species": "Salmonella enterica;96.66%",
          "version": 1
        },
        {
          "assembly_barcode": "SAL_KA9491AA_AS",
          "assembly_status": "Assembled",
          "coverage": 58,
          "download_fasta_link": "http://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_KA9491AA_AS&database=senterica",
          "download_fastq_link": "http://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_KA9491AA_AS&database=senterica&file_format=fastq",
          "id": 109491,
          "lastmodified": "2017-04-30T17:32:38.119705+00:00",
          "n50": 276765,
          "top_species": "Salmonella enterica;93.44%",
          "version": 1
        }
      ],
      "links": {
        "paging": {
          "next": "http://enterobase.warwick.ac.uk/api/v2.0/senterica/assemblies?orderby=barcode&limit=2&reldate=10&offset=2&assembly_status=Assembled&sortorder=asc"
        },
        "records": 2,
        "total_records": 412
      }
    }

[The page about downloading assemblies](api_download_assemblies) has an example
where metadata is obtained for an assembly via the [``straindata``](api_straindata) endpoint which
may be more convenient for particular applications.

As noted on that page, it works well to fetch assemblies' metadata via an ``assemblies`` query
method if you already have assembly barcodes.  For example, try an ``assemblies`` query in the
interactive documentation for the database senterica with "barcode" set to "SAL_IA5204AA_AS"
which corresponds to a request to the URL

.. code-block:: html

    http://enterobase.warwick.ac.uk/api/v2.0/senterica/assemblies?barcode=SAL_IA5204AA_AS


and obtains the result

.. code-block:: json

    {
      "Assemblies": [
        {
          "assembly_barcode": "SAL_IA5204AA_AS",
          "assembly_status": "Assembled",
          "coverage": null,
          "download_fasta_link": "http://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_IA5204AA_AS&database=senterica",
          "download_fastq_link": "http://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_IA5204AA_AS&database=senterica&file_format=fastq",
          "id": 85204,
          "lastmodified": "2017-05-01T22:02:56.892765+00:00",
          "n50": 406329,
          "top_species": "Salmonella enterica;96.66%",
          "version": 1
        }
      ],
      "links": {
        "paging": {},
        "records": 1,
        "total_records": 1
      }
    }

The info query method
---------------------
Refer to interactive documentation : http://enterobase.warwick.ac.uk/api/v2.0/swagger-ui#!/Info

The ``info`` query method shows a "site map" of API URLs.

For example, an ``info`` query with the parameter "name" set to "senterica" (for that database).
A request is made to the URL

.. code-block:: html
    http://enterobase.warwick.ac.uk/api/v2.0

returning the result

.. code-block:: json

    [
      {
        "description": "Salmonella",
        "links": {
          "assemblies": "http://enterobase.warwick.ac.uk/api/v2.0/senterica/assemblies",
          "current": "http://enterobase.warwick.ac.uk/api/v2.0",
          "schemes": "http://enterobase.warwick.ac.uk/api/v2.0/senterica/schemes",
          "straindata": "http://enterobase.warwick.ac.uk/api/v2.0/senterica/straindata",
          "strains": "http://enterobase.warwick.ac.uk/api/v2.0/senterica/strains",
          "sts": "http://enterobase.warwick.ac.uk/api/v2.0/senterica/rMLST/sts",
          "traces": "http://enterobase.warwick.ac.uk/api/v2.0/senterica/traces"
        },
        "name": "senterica",
        "prefix": "SAL",
        "tables": "{'schemes': 'SC', 'snps': 'SN', 'alleles': 'AL', 'assemblies': 'AS', 'loci': 'LO', 'taxondef': 'TA', 'strains': 'SS', 'datadefs': 'DE', 'sts': 'ST', 'traces': 'TR', 'archive': 'AR', 'refsets': 'RE'}"
      }
    ]

providing direct links to the various endpoints in the given database.

The loci query method
---------------------
Refer to interactive documentation : http://enterobase.warwick.ac.uk/api/v2.0/swagger-ui#!/Loci

The ``loci`` query method shows metadata for loci for genotyping schemes.  This includes links
allowing download of the allele sequences for the loci (in gzipped FASTA files).

An example of this type of query to try in the interactive documentation which is also
on [the page about downloading schemes](api_download_schemes)
has "database" set to "senterica", "scheme" set to "MLST_Achtman" (as a parameter and in the path)
and "limit" set to "50".  This corresponds to an HTTP request to the URL:

.. code-block:: html
    http://enterobase.warwick.ac.uk/api/v2.0/senterica/MLST_Achtman/loci?limit=50&scheme=MLST_Achtman


This gives the result:

.. code-block:: json

    {
      "links": {
        "paging": {},
        "records": 7,
        "total_records": 7
      },
      "loci": [
        {
          "database": "Salmonella",
          "download_alleles_link": "http://enterobase.warwick.ac.uk/download_data?allele=aroC&scheme=UoW&species=Salmonella",
          "locus": "aroC",
          "locus_barcode": "SAL_AA0001AA_LO",
          "scheme": "UoW"
        },
        {
          "database": "Salmonella",
          "download_alleles_link": "http://enterobase.warwick.ac.uk/download_data?allele=dnaN&scheme=UoW&species=Salmonella",
          "locus": "dnaN",
          "locus_barcode": "SAL_AA0002AA_LO",
          "scheme": "UoW"
        },
        {
          "database": "Salmonella",
          "download_alleles_link": "http://enterobase.warwick.ac.uk/download_data?allele=hemD&scheme=UoW&species=Salmonella",
          "locus": "hemD",
          "locus_barcode": "SAL_AA0003AA_LO",
          "scheme": "UoW"
        },
        {
          "database": "Salmonella",
          "download_alleles_link": "http://enterobase.warwick.ac.uk/download_data?allele=hisD&scheme=UoW&species=Salmonella",
          "locus": "hisD",
          "locus_barcode": "SAL_AA0004AA_LO",
          "scheme": "UoW"
        },
        {
          "database": "Salmonella",
          "download_alleles_link": "http://enterobase.warwick.ac.uk/download_data?allele=purE&scheme=UoW&species=Salmonella",
          "locus": "purE",
          "locus_barcode": "SAL_AA0005AA_LO",
          "scheme": "UoW"
        },
        {
          "database": "Salmonella",
          "download_alleles_link": "http://enterobase.warwick.ac.uk/download_data?allele=sucA&scheme=UoW&species=Salmonella",
          "locus": "sucA",
          "locus_barcode": "SAL_AA0006AA_LO",
          "scheme": "UoW"
        },
        {
          "database": "Salmonella",
          "download_alleles_link": "http://enterobase.warwick.ac.uk/download_data?allele=thrA&scheme=UoW&species=Salmonella",
          "locus": "thrA",
          "locus_barcode": "SAL_AA0007AA_LO",
          "scheme": "UoW"
        }
      ]
    }

The login query method
----------------------
Refer to the interactive documentation at http://enterobase.warwick.ac.uk/api/v2.0/swagger-ui#!/Login
(although you may want to avoid trying this interactively if you do not intend to change your API token at
this time).

The ``login`` query method is not really a pure query method since it also causes the API token to be refreshed
(changing the API token as a result and avoiding expiration of the API token).  The result of the query includes
the new API token.

This is discussed on the page about [automatically renewing the API token](api_renew_login).

The lookup query method
-----------------------
Refer to interactive documentation : http://enterobase.warwick.ac.uk/api/v2.0/swagger-ui#!/Lookup

The ``lookup`` query method allows lookup of any EnteroBase barcode.  (Barcodes for any type of data can be looked
up with ``lookup``.  It is like a "Swiss army knife" query method and usually a query method specific to a
particular type of data will be more suitable e.g. the ``assemblies`` query method for queries relating to
assemblies.)

An example of this type of query to try in the interactive documentation is to set "barcode" to
"SAL_AA0001AA_LO"

which is equivalent to an HTTP request to the URL:

.. code-block:: html

    http://enterobase.warwick.ac.uk/api/v2.0/lookup?barcode=SAL_AA0001AA_LO

Another example for looking up a scheme barcode sets the barcode to "SAL_AA0010AA_SC" (for the
cgMLST version 4 scheme in Salmonella):

.. code-block:: html

    http://enterobase.warwick.ac.uk/api/v2.0/lookup?barcode=SAL_AA0010AA_SC


giving the result

.. code-block:: json

{
      "records": 1,
      "results": [
        {
          "accepted": 1,
          "barcode": "SAL_AA0010AA_SC",
          "create_time": "2016-01-27 17:39:55.279130",
          "detail": "",
          "index_id": 10,
          "info": {
            "ext_db": null,
            "ext_site": null,
            "incremental": false,
            "max_flanking": 30,
            "merging_error": 0.08,
            "merging_prop": 0.5,
            "min_flanking": 9,
            "min_frag_len": 50,
            "min_frag_prop": 0.3,
            "min_frag_sum": 0.45,
            "min_iden": 0.75,
            "min_prop": 1,
            "position": false,
            "ref_iden": 0.96,
            "value_type": "text"
          },
          "lastmodified": "2016-02-11 18:20:43.713484",
          "lastmodified_by": "enterotool",
          "mode": "MLST",
          "scheme": "cgMLSTv4",
          "table_alias": {
            "STs": "STs",
            "alleles": "alleles",
            "loci": "loci",
            "nomenclature_records": "nomenclature_records",
            "refsets": "refsets",
            "schemes": "schemes",
            "taxa": "taxa"
          },
          "taxon_index": 1,
          "value_alias": {
            "ST_id": "ST_id",
            "ST_index": "ST_index",
            "STs": "STs",
            "Strain_barcode": "Strain_barcode",
            "accepted": "accepted",
            "allele_id": "allele_id",
            "allele_index": "allele_index",
            "allele_indices": "allele_indices",
            "allele_name": "allele_name",
            "alleles": "alleles",
            "contig_name": "contig_name",
            "detail": "detail",
            "fullname": "fullname",
            "info": "info",
            "loci": "loci",
            "locus": "locus",
            "locus_index": "locus_index",
            "mode": "mode",
            "nomenclature_records": "nomenclature_records",
            "prefix": "prefix",
            "ref_allele": "ref_allele",
            "reference": "reference",
            "refname": "refname",
            "refsets": "refsets",
            "scheme": "scheme",
            "schemes": "schemes",
            "seq": "seq",
            "start": "start",
            "stop": "stop",
            "taxa": "taxa",
            "taxon": "taxon"
          },
          "version": 2
        }
      ]
    }

Another example of this type of query to try in the interactive documentation which is also
on [the page about the API structure](api_structure) (which also has a discussion of barcodes) is
to have "barcode" equal to "SAL_AA0019AA_ST" which is equivalent to an HTTP request to the URL:

.. code-block:: html

    http://enterobase.warwick.ac.uk/api/v2.0/lookup?barcode=SAL_AA0019AA_ST


which gives the result (concerning Sequence Type 19, Salmonella Typhimurium)

.. code-block:: html

    {
      "records": 1,
      "results": [
        {
          "ST_id": 19,
          "accepted": 1,
          "alleles": [
            {
              "accepted": 1,
              "allele_id": 10,
              "allele_name": null,
              "locus": "aroC"
            },
            {
              "accepted": 1,
              "allele_id": 7,
              "allele_name": null,
              "locus": "dnaN"
            },
            {
              "accepted": 1,
              "allele_id": 12,
              "allele_name": null,
              "locus": "hemD"
            },
            {
              "accepted": 1,
              "allele_id": 9,
              "allele_name": null,
              "locus": "hisD"
            },
            {
              "accepted": 1,
              "allele_id": 5,
              "allele_name": null,
              "locus": "purE"
            },
            {
              "accepted": 1,
              "allele_id": 9,
              "allele_name": null,
              "locus": "sucA"
            },
            {
              "accepted": 1,
              "allele_id": 2,
              "allele_name": null,
              "locus": "thrA"
            }
          ],
          "barcode": "SAL_AA0019AA_ST",
          "create_time": "2015-11-24 19:59:36.295460",
          "index_id": 19,
          "info": {
            "lineage": "",
            "predict": {
              "serotype": [
                [
                  "Typhimurium",
                  7372
                ],
                [
                  "Typhimurium Var. 5-",
                  440
                ],
                [
                  "Typhimurium Var. O:5-",
                  208
                ]
            },
            "st_complex": "1",
            "subspecies": ""
          },
          "lastmodified": "2017-02-19 01:44:21.503891",
          "lastmodified_by": "admin",
          "reference": {
            "lab_contact": "DVI",
            "refstrain": "9924828",
            "source": "mlst.warwick.ac.uk"
          },
          "scheme": "UoW",
          "scheme_index": 1,
          "version": 4885
        }
      ]
    }

Looking up an allele barcode with:

.. code-block:: html

    http://hercules.warwick.ac.uk/api/v2.0/lookup?barcode=SAL_AP0358AA_AL

obtains the result

.. code-block:: json

    {
      "records": 1,
      "results": [
        {
          "_combo_tag": "1_716",
          "accepted": 1,
          "allele_id": 716,
          "allele_name": null,
          "barcode": "SAL_AP0358AA_AL",
          "create_time": "2017-04-14 06:58:05.463744",
          "index_id": 3900358,
          "lastmodified": "2017-04-14 06:58:05.463744",
          "lastmodified_by": "admin",
          "locus": "aroC",
          "locus_index": 1,
          "reference": {
            "refstrain": "SAL_FA3594AA_AS",
            "source": "mlst.warwick.ac.uk"
          },
          "scheme": "UoW",
          "seq": "GTGTTCCGTCCGGGGCACGCGGATTACACCTACGAACAAAAATACGGCCTGCGCGACTATCGCGGCGGCGGGCGTTCATCCGCCCGTGAAACCGCCATGCGCGTCGCGGCAGGCGCTATCGCCAAAAAATATCTGGCGCAGAAATTCGGCGTGGTGATTCGCGGCTGCCTGACCCAGATGGGTGATATTCCGCTGGAAATCAAAGACTGGGATCAGGTAGAGCAAAACCCGTTCTTCTGCCCGGACCCGGATAAAATCGAGGCGCTGGATGAGCTGATGCGCGCTCTGAAAAAAGAGGGCGATTCCATCGGCGCGAAAGTCACCGTGGTGGCCGACAGCGTGCCCGCCGGGCTTGGCGAGCCGGTATTTGACCGCCTGGACGCCGATATCGCCCACGCGCTGATGAGCATTAACGCCGTGAAGGGCGTGGAAATCGGCGACGGTTTCGGCGTGGTGCAACTGCGCGGCAGCCAGAACCGCGACGAAATCACCACTGCCGGT",
          "version": 1
        }
      ]
    }


The schemes query method
------------------------
Refer to interactive documentation : http://enterobase.warwick.ac.uk/api/v2.0/swagger-ui#!/Schemes

The ``schemes`` query method shows genotyping schemes and pipelines derived from assemblies.

A simple ``schemes`` query will list each scheme in the given EnteroBase database together with associated
metadata.  This can be tried in the interactive documentation by simply entering the name of the database
and otherwise using default values of the parameters.  This corresponds to an HTTP request to the URL (and is similar to the query in step 1 on [the page about downloading schemes using the API](api_download_schemes "wikilink")):

.. code-block:: html

    http://enterobase.warwick.ac.uk/api/v2.0/senterica/schemes?limit=50&orderby=barcode&sortorder=asc


This gives the result

.. code-block:: json

    {
      "Schemes": [
        {
          "created": "2015-11-25T15:39:04.169278+00:00",
          "download_sts_link": null,
          "label": "Assembly stats",
          "lastmodified": "2015-11-27T12:15:31.053974+00:00",
          "scheme_barcode": "",
          "scheme_name": "assembly_stats",
          "version": 1
        },
        {
          "created": "2015-08-26T15:04:34.033635+00:00",
          "download_sts_link": "http://enterobase.warwick.ac.uk/download_data?allele=profiles&scheme=UoW&species=Salmonella",
          "label": "Achtman 7 Gene",
          "lastmodified": "2015-12-07T17:50:17.186416+00:00",
          "scheme_barcode": "SAL_AA0001AA_SC",
          "scheme_name": "MLST_Achtman",
          "version": 1
        },
        {
          "created": "2015-11-25T15:39:05.748726+00:00",
          "download_sts_link": "http://enterobase.warwick.ac.uk/download_data?allele=profiles&scheme=rMLST&species=Salmonella",
          "label": "rMLST",
          "lastmodified": "2015-12-07T17:50:17.186416+00:00",
          "scheme_barcode": "SAL_AA0002AA_SC",
          "scheme_name": "rMLST",
          "version": 1
        },
        {
          "created": "2015-12-30T15:39:05.748726+00:00",
          "download_sts_link": "http://enterobase.warwick.ac.uk/download_data?allele=profiles&scheme=cgMLSTv4&species=Salmonella",
          "label": "cgMLST(3020) Beta",
          "lastmodified": "2015-12-30T15:39:05.748726+00:00",
          "scheme_barcode": "SAL_AA0010AA_SC",
          "scheme_name": "cgMLST",
          "version": 4
        },
        {
          "created": null,
          "download_sts_link": null,
          "label": "Repeat Finder",
          "lastmodified": null,
          "scheme_barcode": null,
          "scheme_name": "ref_masker",
          "version": null
        },
        {
          "created": null,
          "download_sts_link": null,
          "label": "Serotype Prediction (SISTR)",
          "lastmodified": null,
          "scheme_barcode": null,
          "scheme_name": "SeroPred",
          "version": null
        },
        {
          "created": "2015-12-30T15:39:05.748726+00:00",
          "download_sts_link": "http://enterobase.warwick.ac.uk/download_data?allele=profiles&scheme=CRISPR&species=Salmonella",
          "label": "CRISPR",
          "lastmodified": "2015-12-30T15:39:05.748726+00:00",
          "scheme_barcode": null,
          "scheme_name": "CRISPR",
          "version": 1
        },
        {
          "created": null,
          "download_sts_link": "http://enterobase.warwick.ac.uk/download_data?allele=profiles&scheme=wgMLSTv1&species=SALwgMLST",
          "label": "wgMLST",
          "lastmodified": null,
          "scheme_barcode": null,
          "scheme_name": "wgMLST",
          "version": null
        },
        {
          "created": "2015-12-30T15:39:05.748726+00:00",
          "download_sts_link": "http://enterobase.warwick.ac.uk/download_data?allele=profiles&scheme=CRISPR&species=Salmonella",
          "label": "CRISPOL",
          "lastmodified": "2015-12-30T15:39:05.748726+00:00",
          "scheme_barcode": null,
          "scheme_name": "CRISPOL",
          "version": 1
        },
        {
          "created": null,
          "download_sts_link": "http://enterobase.warwick.ac.uk/download_data?allele=profiles&scheme=cgMLSTv1&species=SALwgMLST",
          "label": "cgMLST V2",
          "lastmodified": null,
          "scheme_barcode": null,
          "scheme_name": "cgMLST_v2",
          "version": null
        },
        {
          "created": null,
          "download_sts_link": null,
          "label": "Annotation",
          "lastmodified": null,
          "scheme_barcode": null,
          "scheme_name": "prokka_annotation",
          "version": null
        },
        {
          "created": null,
          "download_sts_link": null,
          "label": "SNP Calls",
          "lastmodified": null,
          "scheme_barcode": null,
          "scheme_name": "snp_calls",
          "version": null
        }
      ],
      "links": {
        "paging": {},
        "records": 12,
        "total_records": 12
      }
    }

Also, you can use "only_fields" set to "download_sts_link" in order to just fetch the download link,
corresponding to an HTTP request to the URL:

.. code-block:: html

    http://enterobase.warwick.ac.uk/api/v2.0/senterica/schemes?limit=50&orderby=barcode&sortorder=asc&only_fields=download_sts_link


which gives the result

.. code-block:: json

{
      "Schemes": [
        {
          "download_sts_link": null
        },
        {
          "download_sts_link": "http://enterobase.warwick.ac.uk/download_data?allele=profiles&scheme=UoW&species=Salmonella"
        },
        {
          "download_sts_link": "http://enterobase.warwick.ac.uk/download_data?allele=profiles&scheme=rMLST&species=Salmonella"
        },
        {
          "download_sts_link": "http://enterobase.warwick.ac.uk/download_data?allele=profiles&scheme=cgMLSTv4&species=Salmonella"
        },
        {
          "download_sts_link": null
        },
        {
          "download_sts_link": null
        },
        {
          "download_sts_link": "http://enterobase.warwick.ac.uk/download_data?allele=profiles&scheme=CRISPR&species=Salmonella"
        },
        {
          "download_sts_link": "http://enterobase.warwick.ac.uk/download_data?allele=profiles&scheme=wgMLSTv1&species=SALwgMLST"
        },
        {
          "download_sts_link": "http://enterobase.warwick.ac.uk/download_data?allele=profiles&scheme=CRISPR&species=Salmonella"
        },
        {
          "download_sts_link": "http://enterobase.warwick.ac.uk/download_data?allele=profiles&scheme=cgMLSTv1&species=SALwgMLST"
        },
        {
          "download_sts_link": null
        },
        {
          "download_sts_link": null
        }
      ],
      "links": {
        "paging": {},
        "records": 12,
        "total_records": 12
      }
    }


Another example of this type of query to try in the interactive documentation (and which is representative
of a single iteration of the loop in step 2 on [the page about downloading schemes](api_download_schemes "wikilink")) queries
the database "senterica" and has "scheme_name" set to "MLST_Achtman", "limit" set to "50" and "only_fields" set to
"download_sts_link" and is request to the URL

```
http://enterobase.warwick.ac.uk/api/v2.0/senterica/schemes?limit=50&orderby=barcode&sortorder=asc&only_fields=download_sts_link&scheme_name=MLST_Achtman
```

This gets the result below, with the request for the MLST_Achtman scheme only:

```
{
  "Schemes": [
    {
      "created": "2015-08-26T15:04:34.033635+00:00",
      "download_sts_link": "http://enterobase.warwick.ac.uk/download_data?allele=profiles&scheme=UoW&species=Salmonella",
      "label": "Achtman 7 Gene",
      "lastmodified": "2015-12-07T17:50:17.186416+00:00",
      "scheme_barcode": "SAL_AA0001AA_SC",
      "scheme_name": "MLST_Achtman",
      "version": 1
    }
  ],
  "links": {
    "paging": {},
    "records": 1,
    "total_records": 1
  }
}
```


#Searching strain data - the straindata query method
See also the interactive documentation at http://enterobase.warwick.ac.uk/api/v2.0/swagger-ui#!/Straindata

The ``straindata`` query method combines data from strains, assemblies and STs to give a single access for most data.  (If only one of these types of data is desired, then it may be better to use a different query method in the API - for possible alternatives see [the diagram on the page about the API structure](api_structure#markdown-header-understanding-the-enterobase-api-structure).)

An example to try in the Swagger UI for ``straindata`` is to enter "Agona" for serotype, "Assembled" for assembly_status and "senterica" for database (and otherwise leave the defaults in the Swagger UI).  This corresponds to an HTTP GET request (using an API key) with the URL
```
http://enterobase.warwick.ac.uk/api/v2.0/senterica/straindata?serotype=Agona&sortorder=asc&assembly_status=Assembled&limit=50
```

The results are a bit long to show in full in this documentation - a similar query with limit set to the smaller value 3 gives the result:
```
{
  "links": {
    "paging": {
      "next": "http://enterobase.warwick.ac.uk/api/v2.0/senterica/straindata?limit=3&sortorder=asc&offset=3&serotype=Agona&assembly_status=Assembled"
    },
    "records": 1,
    "total_records": 2974
  },
  "straindata": {
    "SAL_EA9564AA": {
      "assembly_barcode": "SAL_KA3160AA_AS",
      "assembly_status": "Assembled",
      "city": null,
      "collection_date": null,
      "collection_month": null,
      "collection_time": null,
      "collection_year": 2015,
      "comment": null,
      "continent": "North America",
      "country": "United States",
      "county": null,
      "created": "2016-02-06T19:55:02.784586+00:00",
      "download_fasta_link": "http://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_KA3160AA_AS&database=senterica",
      "email": null,
      "lab_contact": "United States Department of Agriculture, Food Safety and Inspection Service",
      "lastmodified": "2016-09-07T16:20:58.720874+00:00",
      "latitude": null,
      "longitude": null,
      "n50": 234392,
      "orderby": "barcode",
      "postcode": null,
      "region": "Iowa",
      "secondary_sample_accession": "SRS1278094",
      "serotype": "Agona",
      "source_details": "Animal-Swine-Sow",
      "source_niche": "Livestock",
      "source_type": "Air",
      "strain_barcode": "SAL_EA9564AA",
      "strain_name": "FSIS1605678",
      "sts": [
        {
          "scheme_name": "cgMLST",
          "st_barcode": "SAL_AB4818AA_ST",
          "st_id": 71634,
          "timestamp": "2016-10-13 18:49:32.791374",
          "version": "3.1"
        },
        {
          "predicted_serotype": "Agona",
          "scheme_name": "rMLST",
          "species": "Salmonella enterica",
          "st_barcode": "SAL_RA4278AA_ST",
          "st_complex": "54",
          "st_id": 33136,
          "timestamp": "2017-03-16 22:50:26.193849",
          "version": "3.0"
        },
        {
          "predicted_serotype": "Agona",
          "scheme_name": "MLST_Achtman",
          "st_barcode": "SAL_AA0013AA_ST",
          "st_complex": "54",
          "st_id": 13,
          "timestamp": "2017-04-26 03:08:13.847632",
          "version": "3.0"
        }
      ],
      "top_species": "Salmonella enterica;96.48%",
      "uberstrain": "SAL_EA9564AA",
      "version": 1
    }
  }
}
```

In order to reduce the amount of content downloaded and speed up the query, "only_fields" may be used, for example, setting "only_fields" to "strain_name,download_fasta_link" (and adding this to the above example).  This corresponds to an HTTP request with the URL
```
http://enterobase.warwick.ac.uk/api/v2.0/senterica/straindata?serotype=Agona&only_fields=strain_name%2Cdownload_fasta_link&sortorder=asc&assembly_status=Assembled&limit=50
```

The result with limit set to 3 is:
```
{
  "links": {
    "paging": {
      "next": "http://enterobase.warwick.ac.uk/api/v2.0/senterica/straindata?only_fields=strain_name%2Cdownload_fasta_link&limit=3&sortorder=asc&offset=3&serotype=Agona&assembly_status=Assembled"
    },
    "records": 1,
    "total_records": 2974
  },
  "straindata": {
    "SAL_EA9564AA": {
      "assembly_barcode": "SAL_KA3160AA_AS",
      "download_fasta_link": "http://enterobase.warwick.ac.uk/upload/download?assembly_barcode=SAL_KA3160AA_AS&database=senterica",
      "strain_barcode": "SAL_EA9564AA",
      "strain_name": "FSIS1605678",
      "sts": [
        {
          "scheme_name": "cgMLST",
          "st_barcode": "SAL_AB4818AA_ST",
          "st_id": 71634,
          "timestamp": "2016-10-13 18:49:32.791374",
          "version": "3.1"
        },
        {
          "predicted_serotype": "Agona",
          "scheme_name": "rMLST",
          "species": "Salmonella enterica",
          "st_barcode": "SAL_RA4278AA_ST",
          "st_complex": "54",
          "st_id": 33136,
          "timestamp": "2017-03-16 22:50:26.193849",
          "version": "3.0"
        },
        {
          "predicted_serotype": "Agona",
          "scheme_name": "MLST_Achtman",
          "st_barcode": "SAL_AA0013AA_ST",
          "st_complex": "54",
          "st_id": 13,
          "timestamp": "2017-04-26 03:08:13.847632",
          "version": "3.0"
        }
      ]
    }
  }
}
```


# The strains query method #

Refer to interactive documentation : http://enterobase.warwick.ac.uk/api/v2.0/swagger-ui#!/Strains

The ``strains`` query method shows the strain metadata such as host, geography and lab contacts.

An example of this type of query to try in the interactive documentation has database
set to "senterica" and "strain name" set to "FSIS1605678" which corresponds to
a request to the URL

```
http://enterobase.warwick.ac.uk/api/v2.0/senterica/strains?strain_name=FSIS1605678
```

giving the results

```
{
  "Strains": [
    {
      "antigenic_formulas": null,
      "assembly_barcode": "SAL_KA3160AA",
      "city": null,
      "collection_date": null,
      "collection_month": null,
      "collection_time": null,
      "collection_year": 2015,
      "comment": null,
      "continent": "North America",
      "country": "United States",
      "county": null,
      "created": "2016-02-06T19:00:03.611332+00:00",
      "lab_contact": "United States Department of Agriculture, Food Safety and Inspection Service",
      "lastmodified": "2016-02-06T19:55:02.798303+00:00",
      "latitude": null,
      "longitude": null,
      "postcode": null,
      "region": "Iowa",
      "secondary_sample_accession": "SRS1278094",
      "serotype": "Agona",
      "source_details": "Animal-Swine-Sow",
      "source_niche": "Livestock",
      "source_type": "Air",
      "strain_barcode": "SAL_EA9564AA",
      "strain_name": "FSIS1605678",
      "uberstrain": "SAL_EA9564AA",
      "version": 1
    }
  ],
  "links": {
    "paging": {},
    "records": 1,
    "total_records": 1
  }
}
```

Another example of this type of query to try in the interactive documentation has database
set to "senterica", "serotype" set to "Agona" and "limit" to "5" which corresponds to
a request to the URL

```
http://enterobase.warwick.ac.uk/api/v2.0/senterica/strains?serotype=Agona&limit=5
```

giving the results

```
{
  "Strains": [
    {
      "antigenic_formulas": "4,12:g,s,t:z27",
      "assembly_barcode": "SAL_AA0605AA",
      "city": null,
      "collection_date": null,
      "collection_month": null,
      "collection_time": null,
      "collection_year": 1974,
      "comment": null,
      "continent": "Europe",
      "country": "United Kingdom",
      "county": null,
      "created": "2015-08-26T15:04:33.345763+00:00",
      "lab_contact": "F. Boyd",
      "lastmodified": "2015-08-26T15:04:33.345796+00:00",
      "latitude": null,
      "longitude": null,
      "postcode": null,
      "region": null,
      "secondary_sample_accession": "NA",
      "serotype": "Agona",
      "source_details": "Water",
      "source_niche": null,
      "source_type": "Environment",
      "strain_barcode": "SAL_AA0605AA",
      "strain_name": "SARA61 (Fidelma Boyd)",
      "uberstrain": "SAL_AA0605AA",
      "version": 1
    },
    {
      "antigenic_formulas": "1,4,[5],12:f,g,s:[1,2]",
      "assembly_barcode": "SAL_AA0606AA",
      "city": null,
      "collection_date": null,
      "collection_month": null,
      "collection_time": null,
      "collection_year": null,
      "comment": null,
      "continent": "Europe",
      "country": "Denmark",
      "county": null,
      "created": "2015-08-26T15:04:33.345763+00:00",
      "lab_contact": "SSI",
      "lastmodified": "2015-08-26T15:04:33.345796+00:00",
      "latitude": null,
      "longitude": null,
      "postcode": null,
      "region": null,
      "secondary_sample_accession": "NA",
      "serotype": "Agona",
      "source_details": "",
      "source_niche": null,
      "source_type": "Human",
      "strain_barcode": "SAL_AA0606AA",
      "strain_name": "993112K",
      "uberstrain": "SAL_AA0606AA",
      "version": 1
    },
    {
      "antigenic_formulas": "4,12:f,g,s:-",
      "assembly_barcode": "SAL_AA0607AA",
      "city": null,
      "collection_date": null,
      "collection_month": null,
      "collection_time": null,
      "collection_year": 1971,
      "comment": null,
      "continent": "Europe",
      "country": "France",
      "county": null,
      "created": "2015-08-26T15:04:33.345763+00:00",
      "lab_contact": "F.X.Weill, Institut Pasteur",
      "lastmodified": "2015-08-26T15:04:33.345796+00:00",
      "latitude": null,
      "longitude": null,
      "postcode": null,
      "region": null,
      "secondary_sample_accession": "NA",
      "serotype": "Agona",
      "source_details": "",
      "source_niche": null,
      "source_type": "Human",
      "strain_barcode": "SAL_AA0607AA",
      "strain_name": "2-71",
      "uberstrain": "SAL_AA0607AA",
      "version": 1
    },
    {
      "antigenic_formulas": "1,4,[5],12:f,g,s:[1,2]",
      "assembly_barcode": "SAL_AA0608AA",
      "city": null,
      "collection_date": null,
      "collection_month": null,
      "collection_time": null,
      "collection_year": null,
      "comment": null,
      "continent": "Europe",
      "country": "Denmark",
      "county": null,
      "created": "2015-08-26T15:04:33.345763+00:00",
      "lab_contact": "SSI",
      "lastmodified": "2015-08-26T15:04:33.345796+00:00",
      "latitude": null,
      "longitude": null,
      "postcode": null,
      "region": null,
      "secondary_sample_accession": "NA",
      "serotype": "Agona",
      "source_details": "",
      "source_niche": null,
      "source_type": "Human",
      "strain_barcode": "SAL_AA0608AA",
      "strain_name": "993212K",
      "uberstrain": "SAL_AA0608AA",
      "version": 1
    },
    {
      "antigenic_formulas": "4,12:f,g,s:-",
      "assembly_barcode": "SAL_AA0609AA",
      "city": null,
      "collection_date": null,
      "collection_month": null,
      "collection_time": null,
      "collection_year": 1969,
      "comment": null,
      "continent": "Europe",
      "country": "France",
      "county": null,
      "created": "2015-08-26T15:04:33.345763+00:00",
      "lab_contact": "F.X.Weill, Institut Pasteur",
      "lastmodified": "2015-08-26T15:04:33.345796+00:00",
      "latitude": null,
      "longitude": null,
      "postcode": null,
      "region": null,
      "secondary_sample_accession": "NA",
      "serotype": "Agona",
      "source_details": "Liver",
      "source_niche": null,
      "source_type": "Swine",
      "strain_barcode": "SAL_AA0609AA",
      "strain_name": "5-69",
      "uberstrain": "SAL_AA0609AA",
      "version": 1
    }
  ],
  "links": {
    "paging": {
      "next": "http://enterobase.warwick.ac.uk/api/v2.0/senterica/strains?orderby=barcode&limit=5&sortorder=asc&offset=5&serotype=Agona"
    },
    "records": 5,
    "total_records": 780
  }
}
```

# The strainsversion query method #

Refer to interactive documentation : http://enterobase.warwick.ac.uk/api/v2.0/swagger-ui#!/Strainsversion

The ``strainsversion`` query method shows previous versions of the strain metadata such as host, geography
and lab contacts.

An example of this type of query to try in the interactive documentation has database
set to "senterica" and "strain name" set to "FSIS1605678" which corresponds to
a request to the URL

```
http://enterobase.warwick.ac.uk/api/v2.0/senterica/strainsversion?strain_name=FSIS1605678
```

giving the result with no old versions

```
{
  "StrainsArchive": [],
  "links": {
    "paging": {},
    "records": 0,
    "total_records": 0
  }
}
```

Another example of this type of query to try in the interactive documentation has database
set to "senterica" and "strain name" set to "RM_172" which corresponds to
a request to the URL

```
http://enterobase.warwick.ac.uk/api/v2.0/senterica/strainsversion?strain_name=RM_172
```

giving the results with one old version

```
{
  "StrainsArchive": [
    {
      "antigenic_formulas": "",
      "assembly_barcode": "SAL_AA0630AA",
      "city": null,
      "collection_date": null,
      "collection_month": null,
      "collection_time": null,
      "collection_year": 2006,
      "comment": null,
      "continent": "North America",
      "country": "USA",
      "county": null,
      "created": "2015-08-26T15:04:33.345763+00:00",
      "lab_contact": "R. Meinersmann (USDA)",
      "lastmodified": "2015-08-26T15:04:33.345796+00:00",
      "latitude": null,
      "longitude": null,
      "postcode": null,
      "region": null,
      "secondary_sample_accession": "NA",
      "serotype": "Agona",
      "source_details": "River",
      "source_niche": null,
      "source_type": "Environment",
      "strain_barcode": "SAL_AA0630AA",
      "strain_name": "RM_172",
      "uberstrain": "SAL_AA0630AA",
      "version": 1
    }
  ],
  "links": {
    "paging": {},
    "records": 1,
    "total_records": 1
  }
}
```

And the request for "strain name" set to "Ragna", also using "only_fields":

```
http://hercules.warwick.ac.uk/api/v2.0/senterica/strainsversion?only_fields=city&only_fields=collection_year&only_fields=country&only_fields=lastmodified&only_fields=serotype&only_fields=source_details&only_fields=source_type&only_fields=strain_barcode&only_fields=strain_name&only_fields=version&strain_name=Ragna
```

gives 9 old versions:

```
{
  "StrainsArchive": [
    {
      "city": "Trondheim",
      "collection_year": 1800,
      "country": "Norway",
      "lastmodified": "2016-10-28T13:03:01.679343+00:00",
      "serotype": "Paratyphi C",
      "source_details": "Teeth and long bone",
      "source_type": "Human",
      "strain_barcode": "SAL_HA1701AA",
      "strain_name": "Ragna",
      "version": 1
    },
    {
      "city": "Trondheim",
      "collection_year": null,
      "country": "Norway",
      "lastmodified": "2016-10-28T15:49:55.690802+00:00",
      "serotype": "Paratyphi C",
      "source_details": "Teeth and long bone",
      "source_type": "Human",
      "strain_barcode": "SAL_HA1701AA",
      "strain_name": "Ragna",
      "version": 2
    },
    {
      "city": "Trondheim",
      "collection_year": null,
      "country": "Norway",
      "lastmodified": "2016-10-28T15:50:15.275379+00:00",
      "serotype": "Paratyphi C",
      "source_details": "Teeth and long bone",
      "source_type": "Human",
      "strain_barcode": "SAL_HA1701AA",
      "strain_name": "Ragna",
      "version": 3
    },
    {
      "city": "Trondheim",
      "collection_year": null,
      "country": "Norway",
      "lastmodified": "2016-10-28T16:21:11.895341+00:00",
      "serotype": "Paratyphi C",
      "source_details": "Teeth and long bone",
      "source_type": "Human",
      "strain_barcode": "SAL_HA1701AA",
      "strain_name": "Ragna",
      "version": 4
    },
    {
      "city": "Trondheim",
      "collection_year": null,
      "country": "Norway",
      "lastmodified": "2016-11-02T16:23:51.261721+00:00",
      "serotype": "Paratyphi C",
      "source_details": "Teeth and long bone",
      "source_type": "Human",
      "strain_barcode": "SAL_HA1701AA",
      "strain_name": "Ragna",
      "version": 5
    },
    {
      "city": "Trondheim",
      "collection_year": 1200,
      "country": "Norway",
      "lastmodified": "2016-11-02T18:50:51.950894+00:00",
      "serotype": "Paratyphi C",
      "source_details": "Teeth and long bone",
      "source_type": "Human",
      "strain_barcode": "SAL_HA1701AA",
      "strain_name": "Ragna",
      "version": 6
    },
    {
      "city": "Trondheim",
      "collection_year": 1200,
      "country": "Norway",
      "lastmodified": "2016-12-01T11:56:01.862835+00:00",
      "serotype": "Paratyphi C",
      "source_details": "Teeth and long bone",
      "source_type": "Human",
      "strain_barcode": "SAL_HA1701AA",
      "strain_name": "Ragna",
      "version": 7
    },
    {
      "city": "Trondheim",
      "collection_year": 1200,
      "country": "Denmark",
      "lastmodified": "2017-01-09T12:54:40.712442+00:00",
      "serotype": "Paratyphi C",
      "source_details": "Teeth, long bone",
      "source_type": "Human",
      "strain_barcode": "SAL_HA1701AA",
      "strain_name": "Ragna",
      "version": 8
    },
    {
      "city": "Trondheim",
      "collection_year": 1200,
      "country": "Norway",
      "lastmodified": "2017-01-09T12:55:14.647874+00:00",
      "serotype": "Paratyphi C",
      "source_details": "Teeth, long bone",
      "source_type": "Human",
      "strain_barcode": "SAL_HA1701AA",
      "strain_name": "Ragna",
      "version": 9
    }
  ],
  "links": {
    "paging": {},
    "records": 9,
    "total_records": 9
  }
}
```

#Searching ST data - the sts query method
Refer to interactive documentation : http://enterobase.warwick.ac.uk/api/v2.0/swagger-ui#!/Sts

The ``sts`` query method shows Sequence Types (STs) and IDs for pipeline/ genotype data.

An example of this type of query to try in the interactive documentation which is also
on [the page about downloading schemes](api_download_schemes)
has "database" set to "senterica", "scheme" (in the path and as a parameter) set to "MLST_Achtman", "show_alleles" set to "false" and "reldate" set to "20".  This fetches the new STs
in the 7 gene MLST scheme from the last 20 days.  (Note that you may obtain different results than
in this documentation due to the query being conducted at a different time resulting the query fetching
results for a different time interval.)  This corresponds
to an HTTP request (using the API key) with the URL:
```
http://enterobase.warwick.ac.uk/api/v2.0/senterica/MLST_Achtman/sts?scheme=MLST_Achtman&show_alleles=false&limit=5&reldate=20
```

The results from this query (at the time of writing this documentation) are:
```
{
  "STs": [
    {
      "ST_id": "3856",
      "barcode_link": "http://enterobase.warwick.ac.uk/api/v1.0/lookup?barcode=SAL_HB2013AA_ST",
      "create_time": "2017-04-12 15:36:09.782739",
      "info": {
        "lineage": "",
        "st_complex": "224",
        "subspecies": ""
      },
      "reference": {
        "lab_contact": "public",
        "refstrain": "SAL_RA9042AA_AS",
        "source": "mlst.warwick.ac.uk"
      },
      "scheme": "UoW",
      "st_barcode": "SAL_HB2013AA_ST"
    },
    {
      "ST_id": "3857",
      "barcode_link": "http://enterobase.warwick.ac.uk/api/v1.0/lookup?barcode=SAL_HB2022AA_ST",
      "create_time": "2017-04-13 07:38:58.579619",
      "info": null,
      "reference": {
        "lab_contact": "public",
        "refstrain": "SAL_RA9134AA_AS",
        "source": "mlst.warwick.ac.uk"
      },
      "scheme": "UoW",
      "st_barcode": "SAL_HB2022AA_ST"
    },
    {
      "ST_id": "3858",
      "barcode_link": "http://enterobase.warwick.ac.uk/api/v1.0/lookup?barcode=SAL_HB2024AA_ST",
      "create_time": "2017-04-13 08:22:20.405268",
      "info": null,
      "reference": {
        "lab_contact": "public",
        "refstrain": "SAL_RA9183AA_AS",
        "source": "mlst.warwick.ac.uk"
      },
      "scheme": "UoW",
      "st_barcode": "SAL_HB2024AA_ST"
    },
    {
      "ST_id": "3859",
      "barcode_link": "http://enterobase.warwick.ac.uk/api/v1.0/lookup?barcode=SAL_HB2026AA_ST",
      "create_time": "2017-04-13 09:59:28.391416",
      "info": null,
      "reference": {
        "lab_contact": "public",
        "refstrain": "SAL_RA9211AA_AS",
        "source": "mlst.warwick.ac.uk"
      },
      "scheme": "UoW",
      "st_barcode": "SAL_HB2026AA_ST"
    },
    {
      "ST_id": "3860",
      "barcode_link": "http://enterobase.warwick.ac.uk/api/v1.0/lookup?barcode=SAL_HB2028AA_ST",
      "create_time": "2017-04-13 11:02:00.465841",
      "info": {
        "lineage": "",
        "st_complex": "4",
        "subspecies": ""
      },
      "reference": {
        "lab_contact": "public",
        "refstrain": "SAL_RA9255AA_AS",
        "source": "mlst.warwick.ac.uk"
      },
      "scheme": "UoW",
      "st_barcode": "SAL_HB2028AA_ST"
    }
  ],
  "links": {
    "paging": {
      "next": "http://enterobase.warwick.ac.uk/api/v2.0/senterica/MLST_Achtman/sts?limit=5&offset=5&show_alleles=false&scheme=MLST_Achtman&reldate=20"
    },
    "records": 5,
    "total_records": 28
  }
}
```

Note that as discussed in [the notes about downloading schemes](api_download_schemes) it is inadvisable to download
the ST profiles in their entirety using the above method, rather to download a tarball with the daily dump of all data and update with new information at regular intervals using queries like the one above as is described on [that page](api_download_schemes).
# The traces query method #

Refer to interactive documentation : http://enterobase.warwick.ac.uk/api/v2.0/swagger-ui#!/Traces

The ``traces`` query method shows metadata such as read length, accession number and sequencing platform
for sequenced reads.

An example of this type of query in the Swagger UI has "database" set to "senterica" and "barcode" set to
"SAL_KA1677AA_TR" equivalent to a request to the URL

```
http://enterobase.warwick.ac.uk/api/v2.0/senterica/traces?barcode=SAL_KA1677AA_TR
```

This obtains the result

```
{
  "Traces": [
    {
      "lastmodified": "2017-05-04T09:35:53.574967+00:00",
      "trace_barcode": "SAL_KA1677AA_TR"
    }
  ],
  "links": {
    "paging": {},
    "records": 1,
    "total_records": 1
  }
}
```