tabname	name	sra_field	mlst_field	display_order	nested_order	label	datatype	min_value	max_value	required	default_value	allow_pattern	description	vals	max_length	no_duplicates	allow_multiple_values	group_name
strains	strain		STRAIN															
strains	contact		SOURCE															
strains	collection_year		YEAR															
strains	country		COUNTRY															
strains	city		CITY															
strains	path_nonpath		CLINICAL_SYMPTOMS	17	0	Pathogen/Non pathogen	combo			0				Pathogen|Non Pathogen				
