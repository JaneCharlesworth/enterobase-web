User Space OS and Workspaces
============================

User Space OS
-------------
**Layout for the workspace box**

.. image:: https://bytebucket.org/enterobase/enterobase-web/raw/14a703bfda1e64d86fe04367684bac3ec3715aa1/docs/images/workspace_layout.png

1. The easiest way to access the workspace box is clicking the "Load Workspace" pane on the `Database Homepage`_.  You can also access the workspace box from the menu in the `Search Strain`_ page. 
    
2. User items are assigned to one of the three tabs below. Click the labels to switch between tabs. 

    * a. Mine (default): items that are created and owned by You. 
    * b. Shared: items that are shared from your buddies. 
    * c. Public: items that are publicly released in EnteroBase. 

3. `Folder`_-based explorer of items. 
4. A searching box allowing partial matching of the item names. 
5. A detailed description of the selected item in the left pane. 
6. Click to load an item. You can also open an item by double-clicking its name. 

Remember, that when somebody shares a folder or other items with you, they will appear in Shared under their name rather than in Mine.

**Shared and published items**

.. image:: https://bytebucket.org/enterobase/enterobase-web/raw/a8b14329226a6668a9caea9fa887f3dc3d79db79/docs/images/shared.png

1. Workspaces and analyses in the "shared" and "public" tabs are grouped into folders named after their original owners. 
2. Shared items can be copied using "copy to Mine" option in the right-click menu. 
 

**Items in workspace box**

The workspace dialog box in EnteroBase serves as a manage port for many user items. 

+----------+-----------------------+---------------------------------------------------------+
|Icon      |Name                   |Description                                              |
+==========+=======================+=========================================================+
| |folder| | `Folder`_             | Subgroups of user items                                 |
+----------+-----------------------+---------------------------------------------------------+
| |ws|     | `Workspace`_          | Collection of genomes                                   |
+----------+-----------------------+---------------------------------------------------------+
| |gtree|  | `GrapeTree`_          | Phylogeny based on MLST                                 |
+----------+-----------------------+---------------------------------------------------------+
| |stree|  | `SNP Project`_        | SNP matrix and phylogeny                                |
+----------+-----------------------+---------------------------------------------------------+
| |cview|  | `Custom View`_        | Collection of experimental data and user defined fields |
+----------+-----------------------+---------------------------------------------------------+
| |udf|    | `User Defined Field`_ | Private editable metadata field                         |
+----------+-----------------------+---------------------------------------------------------+
| |locus|  | `Locus Search`_       | MLST gene prediction of queried sequences               |
+----------+-----------------------+---------------------------------------------------------+


Folder system
-------------

Folders can be created to organise user items. `Sharing a folder`_ with your buddies will give them access to all the workspaces and analyses in the folder (both existing and future). 

**To create a folder:**

1. Right-click on any item to create a folder. 
2. Enter a memorable name for the folder. 
3. Drag a workspace and drop it in/out of a folder. 

.. image:: https://bitbucket.org/enterobase/enterobase-web/raw/13277eb9202575f4d7ae72bd4a5c3013d480eaf9/docs/images/create-folder.png



Workspaces
----------

Workspaces allow you to define a set of strains in a defined group, which you can 
revisit at any time.  Such groups should be considered as a list of strains used 
in a given study and can be shared with other users.  (This functionality is 
covered in `Managing your buddies`_).  Workspaces are also the main method 
to defined input data sets for certain analysis pipelines in EnteroBase. 

**Take an existing set of search results and save a workspace via:**

1. From the menu above the strain records, go to the Workspace menu.
2. Select ``Workspace -> Save``
3. Enter a memorable name for this workspace. 
4. Click save button

.. image:: https://bitbucket.org/enterobase/enterobase-web/raw/13277eb9202575f4d7ae72bd4a5c3013d480eaf9/docs/images/save-workspace.png
  
**To retrieve a workspace:**

1. Go to the Workspace menu.
2. Select ``Workspace -> Load``
3. Choose from the names of workspaces. 
4. Check the brief description of the workspace. 
5. Click load button. 

.. image:: https://bitbucket.org/enterobase/enterobase-web/raw/13277eb9202575f4d7ae72bd4a5c3013d480eaf9/docs/images/load-workspace.png

**Publish a workspace:**

Publishing a workspace will make it accessible to all the users in EnteroBase. A public workspace can only be modified and/or retracted by administrators. 

* To publish a workspace, right-click and choose "Make Public" in the context menu. 

.. image:: https://bitbucket.org/enterobase/enterobase-web/raw/13277eb9202575f4d7ae72bd4a5c3013d480eaf9/docs/images/make-public.PNG


.. _`Managing your buddies`: buddies.html
.. _`Sharing a folder`: buddies.html#share-workspaces-analyses-folders

.. _`Folder`: using-workspaces.html#folder-system
.. _`Workspace`: using-workspaces.html
.. _`GrapeTree`: ../grapetree/grapetree-about.html
.. _`SNP Project`: snp-projects.html
.. _`Custom View`: user-defined-content.html
.. _`User Defined Field`: user-defined-content.html#user-defined-fields-udfs
.. _`Locus Search`: locus-search.html
.. _`Database Homepage`: species-index.html
.. _`Search Strain`: main-search-page.html

.. |folder| image:: https://bitbucket.org/enterobase/enterobase-web/raw/f5071010ff91552fb76e2b9c7cb44eed4686cae8/docs/images/icon-folder.PNG
.. |ws| image:: https://bitbucket.org/enterobase/enterobase-web/raw/f5071010ff91552fb76e2b9c7cb44eed4686cae8/docs/images/icon-workspace.PNG
.. |gtree| image:: https://bitbucket.org/enterobase/enterobase-web/raw/f5071010ff91552fb76e2b9c7cb44eed4686cae8/docs/images/icon-grapetree.PNG
.. |stree| image:: https://bitbucket.org/enterobase/enterobase-web/raw/f5071010ff91552fb76e2b9c7cb44eed4686cae8/docs/images/icon-snptree.PNG
.. |cview| image:: https://bitbucket.org/enterobase/enterobase-web/raw/f5071010ff91552fb76e2b9c7cb44eed4686cae8/docs/images/icon-customview.PNG
.. |udf| image:: https://bitbucket.org/enterobase/enterobase-web/raw/f5071010ff91552fb76e2b9c7cb44eed4686cae8/docs/images/icon-udf.PNG
.. |locus| image:: https://bitbucket.org/enterobase/enterobase-web/raw/f5071010ff91552fb76e2b9c7cb44eed4686cae8/docs/images/icon-locussearch.PNG
