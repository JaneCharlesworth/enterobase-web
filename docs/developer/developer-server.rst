Installing The Source Code
==========================

* If not already installed *sudo apt-get install git*
* Navigate to the apprpriate directory (this will be the parent directory of the code)
* *git clone https://my_user_name@bitbucket.org/enterobase/enterobase-web.git*


Installing Python
-----------------
In order to keep a clean local python environment pyenv needs to be installed:-

* In your home directory *git clone https://github.com/yyuu/pyenv.git ~/.pyenv*
* *echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc*
* *echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc*
* *echo 'eval "$(pyenv init -)"' >> ~/.bashrc*
* *exec $SHELL*

Next need to install the correct version of python within pyenv

* *pyenv install 2.7.8*

Depending on your system you may have to install the following dependencies:-

* *sudo apt-get install build-essential*
* *sudo apt-get install zlib1g-dev*
* *sudo apt-get install libssl-dev*
* *sudo apt-get install bzip2  libbz2-dev*
* *sudo apt-get install libreadline6 libreadline6-dev*
* *sudo apt-get install sqlite3 libsqlite3-dev*

Installing virtualenv will help manage your virtual environments

* *git clone https://github.com/yyuu/pyenv-virtualenv.git ~/.pyenv/plugins/pyenv-virtualenv*
* *echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc*
* *exec "$SHELL"*

Now you are ready to create the virtual environment

* *pyenv virtualenv 2.7.8 entero*

To activate the environment

* *pyenv activate entero*

Next install all the required python packages, which are listed in the
requirements.txt file in the base enterobase directory (make sure you are in
your virtual environment)

* *pip install -r requirements.txt*


Installing an IDE
-----------------
First install the x2go server such that you open x windows on your local machine

* *sudo add-apt-repository ppa:x2go/stable*
* *sudo apt-get update*
* *sudo apt-get install x2goserver x2goserver-xsession*

Install wing:-

* *tar -xvf wingide-5.1.12-1-x86_64-linux.tar.gz*
* *cd wingide-5.1.12-1-x86_64-linux/*
* *sudo /home/ubuntu/.pyenv/versions/2.7.8/envs/entero/bin/python wing-install.py*
* *echo 'export PATH="/usr/local/bin:$PATH"' >> ~/.bashrc*
* *exec "$SHELL"*
* *sudo chmod 771 /usr/local/lib/wingide5.1*

Wing General Setup

* In the main menu go to Project>New Project and save it any location (this will just be where the config files are, not the source files). Add the folder where you downloaded the source code once the project is created
* Make sure you tell wing to use a custom python executable and point it to the python file in your virtual environment (e.g. /home/<user_name>/.pyenev/versions/<virtual_env_name>/bin/Pyhton2.7). N.B Right click on the file popup in order to see hidden files and navigate to to .pyenv directory.


Debugging with Wing:-

* Copy the wingdbstub.py and wingdb-suid from /usr/local/lib/wingide5.1 (or wherever you installed wing) to the main app directory i.e.entero
* Make sure that import wingdbstub is in the enterobase code, usually located in create_app method of the main entero __init__ method
* When wing is open, right click on the bug in the bottom left and make sure Accept Debug Connections is checked
* Then run the website in the normal way
* In the gunicorn start up script gunicorn_start.sh, make sure you set the —timeout parameter to 2000 (or greater), else you will only get 30 seconds to debug


Running the app with Gunicorn
-----------------------------
create a file called start_gunicorn.sh with the following content

.. code-block:: bash
        
        NAME="enterobase"
        FLASKDIR=/var/www/enterobase   # Change this to location of app.py
        SOCKFILE=/var/www/enterobase/sock # change this to project_dir/sock (new file will be created)
        USER=ubuntu                        # Change this to your user/group
        GROUP=ubuntu
        NUM_WORKERS=2
        LOG=/var/www/enterobase
        echo "Starting $NAME as `whoami`"
        
        export PYTHONPATH=$FLASKDIR:$PYTHONPATH
        
        # Create the run directory if it doesn't exist
        RUNDIR=$(dirname $SOCKFILE)
        test -d $RUNDIR || mkdir -p $RUNDIR
        
        cd $FLASKDIR
        
        # Start your unicorn
        exec /home/$USER/.pyenv/versions/enterobase/bin/gunicorn "entero:create_app('production')" -b 127.0.0.1:8000 \
          --enable-stdio-inheritance \
          --access-logfile=$LOG/access.log \
          --error-logfile=$LOG/error.log \
          --log-level=debug \
          --name $NAME \
          --workers $NUM_WORKERS \
          --user=$USER --group=$GROUP \
          --log-file=$LOG/gunilog.log \
          --bind=unix:$SOCKFILE \
          --timeout 2000

The app can be run with *nohup start_gunicorn.sh &* , but using supervisor may be more appropriate

Installing the Nginx Server
---------------------------
Install nginx *sudo apt-get install nginx*
in /etc/nginx/sites_enabled

.. code-block:: bash

        server {
                listen 80;
                server_name ip_address;	
                location / {
                        proxy_pass http://localhost:8000;
                }
        }

In order to update nginx with the changes:-  *sudo nginx -s reload*


Allowing Emails
---------------
On Jenner you need to add the IP address of the new server:-

* sudo vim /etc/postfix/main.cf
* Add the  IP address in parameter "mynetworks = "
* sudo /etc/init.d/postfix restart