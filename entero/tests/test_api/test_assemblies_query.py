# import unittest
import pytest
#import flask
#from flask import current_app
#from entero import create_app, config
#import os
# from entero.databases.system.models import User

from entero.test_utilities import ebHTTPNotAuth, get_api_headers, enHashInfoQueryResponseData

from urllib2 import HTTPError
import urllib2
import json
import re

# WVN 20/2/18 Note that according to the Swagger UI documentation does not currently have
# a separate status code for a malformed request; so I'm not testing for that at present.
class TestAssembliesQueryCase1(object):
    assemblies_query1 = "/api/v2.0/senterica/assemblies?limit=2&assembly_status=Assembled"
  
    @pytest.fixture(scope='class') 
    def assemblies_query1_bad_auth_response(self, eb_api_client):
        response = eb_api_client.get(self.assemblies_query1, headers=get_api_headers("aninvalidkey"))
        return response

    @pytest.fixture(scope='class') 
    def assemblies_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        response = eb_api_client.get(self.assemblies_query1, headers=get_api_headers(eb_jack_api_key))
        return response

    @pytest.fixture(scope='class') 
    def assemblies_query1_jack_auth_response_data(self, assemblies_query1_jack_auth_response):
        return json.loads(assemblies_query1_jack_auth_response.get_data(as_text=True))

    def test_assemblies_query1_bad_auth_status(self, assemblies_query1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert assemblies_query1_bad_auth_response.status_code == ebHTTPNotAuth
        assert assemblies_query1_bad_auth_response.status_code == 401

    def test_assemblies_query1_jack_auth_status(self, assemblies_query1_jack_auth_response):
        assert assemblies_query1_jack_auth_response.status_code == 200

    def test_assemblies_query1_non_empty(self, assemblies_query1_jack_auth_response_data):
        # Not sure what checks to do on content apart from ensuring it is non-empty
        # responseData = json.loads(assemblies_query1_jack_auth_response.get_data(as_text=True))
        # These two asserts really are checking pretty much the same thing - no
        # point in breaking out into separate tests
        # assert responseData is not None
        # assert len(responseData) > 0
        assert assemblies_query1_jack_auth_response_data is not None
        assert len(assemblies_query1_jack_auth_response_data) > 0

    def test_assemblies_query1_has_assemblies_key(self, assemblies_query1_jack_auth_response_data):
        # Not sure what checks to do on content apart from ensuring it is non-empty
        assert "Assemblies" in assemblies_query1_jack_auth_response_data

    def test_assemblies_query1_has_links_key(self, assemblies_query1_jack_auth_response_data):
        # Not sure what checks to do on content apart from ensuring it is non-empty
        # responseData = json.loads(assemblies_query1_jack_auth_response.get_data(as_text=True))
        assert "links" in assemblies_query1_jack_auth_response_data

class TestAssembliesBarcodeQueryCase1(object):
    assemblies_barcode_query1 = "api/v2.0/senterica/assemblies/SAL_BA0004AA_AS"
 
    # GET request 
    @pytest.fixture(scope='class') 
    def assemblies_barcode_get_query1_bad_auth_response(self, eb_api_client):
        return eb_api_client.get(self.assemblies_barcode_query1, headers=get_api_headers("aninvalidkey"))

    @pytest.fixture(scope='class') 
    def assemblies_barcode_get_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        return eb_api_client.get(self.assemblies_barcode_query1, headers=get_api_headers(eb_jack_api_key))

    @pytest.fixture(scope='class') 
    def assemblies_barcode_get_query1_jack_auth_response_data(self, assemblies_barcode_get_query1_jack_auth_response):
        return json.loads(assemblies_barcode_get_query1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Known that test fails due to status code 200 when it should get status code 401")
    def test_assemblies_barcode_get_query1_bad_auth_status(self, assemblies_barcode_get_query1_bad_auth_response):
        # 401 correct status to get here rather than 403
        assert assemblies_barcode_get_query1_bad_auth_response.status_code == 401

    def test_assemblies_barcode_get_query1_jack_auth_status(self, assemblies_barcode_get_query1_jack_auth_response):
        assert assemblies_barcode_get_query1_jack_auth_response.status_code == 200

    def test_assemblies_barcode_get_query1_non_empty(self, assemblies_barcode_get_query1_jack_auth_response_data):
        # These two asserts really are checking pretty much the same thing - no
        # point in breaking out into separate tests
        assert assemblies_barcode_get_query1_jack_auth_response_data is not None
        assert len(assemblies_barcode_get_query1_jack_auth_response_data) > 0

    def test_assemblies_barcode_get_query1_has_assemblies_key(self, assemblies_barcode_get_query1_jack_auth_response_data):
        assert "Assemblies" in assemblies_barcode_get_query1_jack_auth_response_data

    @pytest.mark.skip(reason = "Known that test fails due to key being called paging inconsistently with the other type of query where it is called links")
    def test_assemblies_barcode_get_query1_has_links_key(self, assemblies_barcode_get_query1_jack_auth_response_data):
        # At present the relevant key is called "paging" in case of the assembly barcode GET request
        # and "links" (like the standard assembly query) in hte case of a POST request.
        assert "links" in assemblies_barcode_get_query1_jack_auth_response_data

    # POST request
    @pytest.fixture(scope='class') 
    def assemblies_barcode_post_query1_bad_auth_response(self, eb_api_client):
        return eb_api_client.post(self.assemblies_barcode_query1, headers=get_api_headers("aninvalidkey"))

    @pytest.fixture(scope='class') 
    def assemblies_barcode_post_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        return eb_api_client.post(self.assemblies_barcode_query1, headers=get_api_headers(eb_jack_api_key))

    @pytest.fixture(scope='class') 
    def assemblies_barcode_post_query1_jack_auth_response_data(self, assemblies_barcode_post_query1_jack_auth_response):
        return json.loads(assemblies_barcode_post_query1_jack_auth_response.get_data(as_text=True))

    @pytest.mark.skip(reason = "Known that test fails due to status code 200 when it should get status code 401")
    def test_assemblies_barcode_post_query1_bad_auth_status(self, assemblies_barcode_post_query1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert assemblies_barcode_post_query1_bad_auth_response.status_code == ebHTTPNotAuth
        assert assemblies_barcode_post_query1_bad_auth_response.status_code == 401

    def test_assemblies_barcode_post_query1_jack_auth_status(self, assemblies_barcode_post_query1_jack_auth_response):
        assert assemblies_barcode_post_query1_jack_auth_response.status_code == 200

    def test_assemblies_barcode_post_query1_non_empty(self, assemblies_barcode_post_query1_jack_auth_response_data):
        # These two asserts really are checking pretty much the same thing - no
        # point in breaking out into separate tests
        assert assemblies_barcode_post_query1_jack_auth_response_data is not None
        assert len(assemblies_barcode_post_query1_jack_auth_response_data) > 0

    def test_assemblies_barcode_post_query1_has_assemblies_key(self, assemblies_barcode_post_query1_jack_auth_response_data):
        assert "Assemblies" in assemblies_barcode_post_query1_jack_auth_response_data

    # def test_assemblies_barcode_post_query1_has_links_key(self, assemblies_query1_jack_auth_response):
    def test_assemblies_barcode_post_query1_has_links_key(self, assemblies_barcode_post_query1_jack_auth_response_data):
        # At present the relevant key is called "paging" in case of the assembly barcode GET request
        # and "links" (like the standard assembly query) in the case of a POST request.
        assert "links" in assemblies_barcode_post_query1_jack_auth_response_data

    # PUT request
    @pytest.fixture(scope='class') 
    def assemblies_barcode_put_query1_bad_auth_response(self, eb_api_client):
        return eb_api_client.put(self.assemblies_barcode_query1, headers=get_api_headers("aninvalidkey"))

    @pytest.fixture(scope='class') 
    def assemblies_barcode_put_query1_jack_auth_response(self, eb_api_client, eb_jack_api_key):
        return eb_api_client.put(self.assemblies_barcode_query1, headers=get_api_headers(eb_jack_api_key))

    @pytest.fixture(scope='class') 
    def assemblies_barcode_put_query1_jack_auth_response_data(self, assemblies_barcode_put_query1_jack_auth_response):
        return json.loads(assemblies_barcode_put_query1_jack_auth_response.get_data(as_text=True))

    def test_assemblies_barcode_put_query1_bad_auth_status(self, assemblies_barcode_put_query1_bad_auth_response):
        # 401 correct status to get here rather than 403
        # assert assemblies_barcode_put_query1_bad_auth_response.status_code == ebHTTPNotAuth
        assert assemblies_barcode_put_query1_bad_auth_response.status_code == 401

    # jack login - a non-admin login - is not allowed to do this
    def test_assemblies_barcode_put_query1_jack_auth_status(self, assemblies_barcode_put_query1_jack_auth_response):
        assert assemblies_barcode_put_query1_jack_auth_response.status_code == ebHTTPNotAuth

    # Keeping the code for these tests here for now possibly to be re-purposed for
    # testboss login
#    def test_assemblies_barcode_put_query1_non_empty(self, assemblies_barcode_put_query1_jack_auth_response_data):
#        # These two asserts really are checking pretty much the same thing - no
#        # point in breaking out into separate tests
#        assert assemblies_barcode_put_query1_jack_auth_response_data is not None
#        assert len(assemblies_barcode_put_query1_jack_auth_response_data) > 0
#
#    def test_assemblies_barcode_put_query1_has_assemblies_key(self, assemblies_barcode_put_query1_jack_auth_response_data):
#        assert "Assemblies" in assemblies_barcode_put_query1_jack_auth_response_data
#
#    def test_assemblies_barcode_put_query1_has_links_key(self, assemblies_barcode_put_query1_jack_auth_response_data):
#        # At present the relevant key is called "paging" in case of the assembly barcode GET request
#        # and "links" (like the standard assembly query) in the case of a POST request.
#        assert "links" in assemblies_barcode_put_query1_jack_auth_response_data TestAllelesQueryCase1(object):
