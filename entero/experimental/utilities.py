from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC
import os,ujson,requests
from entero import dbhandle,app,db

from entero.jbrowse.utilities import concatanate_fasta_file,build_gff_files,build_sequence_index,__get_tracks_json,__save_tracks_json
sibelia_bin = ''
def run_sibelia(directory,barcode_to_info,ref_barcode):
    '''This method runs sibelia on the supplied assemblies and will build tracks for each assembly showing the pan genome
    The inputs are aligned against the reference with extra sequence not in the reference being tacked on the end
    :param directory: directory where to store temporary files
    :param barcode_to_info: A dictionary with the barcode as the key pointing to an arrau containing
        the assembly file pointer and strain name
    :param ref_barcode: The barcode of the reference
    '''
    out_files=[]
    genome_number = len(barcode_to_info)
    for barcode in barcode_to_info:
        handle = open(barcode_to_info[barcode][0])   
        records = list(SeqIO.parse(handle, "fasta"))
        for rec in records:
            rec.id =barcode+";"+rec.id     
        out_file = os.path.join(directory,barcode+"_id.fasta")
        out_files.append(out_file)
        SeqIO.write(records, out_file, "fasta")
    results = os.path.join(directory,"output")
    ref_fasta_file = barcode_to_info[ref_barcode][0]
    folder = os.path.split(ref_fasta_file)[0]
    ref_gff_file = os.path.join(folder,ref_barcode+".gff")
    concatanate_fasta_file(ref_fasta_file,ref_gff_file,directory,"pangenome")
    
    out_files.append(os.path.join(directory,"pangenome.fasta"))
    command = "Sibelia -s loose -m 5000 -o %s %s" % (os.path.join(directory,"output"), " ".join(out_files))
   #os.system(command)
    pan_genome_file = os.path.join(directory,"pangenome.fasta")
    
    records = list(SeqIO.parse(open(pan_genome_file) ,"fasta"))
    pan_genome_seq = str(records[0].seq)
    pan_genome_offset=len(pan_genome_seq)
    blocks= []
    pangenome={}   
    with open(os.path.join(results,"blocks_coords.txt")) as f:
        mode = 'contig names'
        current_dict=None
        contig_ids = {}
        first_line = True
        new_segment=True
        block_id = None
        percentage_block={}
        for line in f:
            if first_line:
                first_line = False
                continue
            if line.startswith("---") and mode == 'contig names':
                mode = 'Blocks'
                continue
            if mode == 'contig names' and not first_line:
                arr = line.strip().split("\t")
                contig_ids[arr[0]]= arr[2].split(";")
                continue
            if line.startswith("Block"):
                arr = line.strip().split(" #")
                block_id=arr[1]
                current_dict={"id":block_id}
                blocks.append(current_dict)
                mode="Block skip line"
                continue
            if mode == 'Block skip line':
                mode = "Read Block"
                new_segment=True
                max_len=0
                block_max_len =""
                continue
            if mode == 'Read Block' and line.startswith("---") and len(current_dict)>1:
                block_num = len(current_dict) -2
                percentage = int((block_num * 100)/genome_number)
                current_dict['percentage']=percentage
                percentage_block[current_dict['id']]=percentage
                if percentage==100:
                    current_dict['core']="true"
                else:
                    current_dict['core']="false"
                if new_segment:
                    #get the sequence of the largest block:
                    record_dict= SeqIO.index(os.path.join(directory,block_max_len[0]+"_id.fasta"),"fasta")
                    record =record_dict[block_max_len[0]+";"+block_max_len[1]]
                    start = int(block_max_len[2])
                    end = int(block_max_len[3])
                    if start>end:
                        temp=end
                        end=start
                        start=temp
                    start=start-1
                    seq = str(record.seq)[start:end]
                    block_start = len(pan_genome_seq)+1
                    block_end= block_start+len(seq)
                    pangenome[block_id]=[[block_start,block_end]]
                    pan_genome_seq = pan_genome_seq+str(seq)
                
                    
                    
                    
            if mode == 'Read Block' and not line.startswith("---"):
                arr = line.strip().split("\t")
                barcode = contig_ids[arr[0]][0]
                if barcode == "pangenome": 
                    start = int(arr[2])
                    end = int(arr[3])
                    if start>end:
                        temp=end
                        end=start
                        start=temp
                    info_list = pangenome.get(block_id)
                    if not info_list:
                        info_list=[]
                        pangenome[block_id]=info_list
                    info_list.append([start,end])
                    new_segment=False
                else:
                    contig= contig_ids[arr[0]][1]
                    info_list = current_dict.get(barcode)
                    if not info_list:
                        info_list= []
                        current_dict[barcode]=info_list
                    current_dict['size']=arr[4]
                    int_max_size=int(arr[4])
                    if int_max_size>max_len:
                        max_len=int_max_size
                        block_max_len=[barcode,contig,arr[2],arr[3]]
                    info_list.append("%s;%s;%s;%s"%(contig,arr[1],arr[2],arr[3]))
        
        sorted_list = sorted(blocks,key = lambda x: int(x['size']),reverse=True)
        all_blocks_gff = os.path.join(directory,"blocks.gff")
        out_block_handle = open(all_blocks_gff,"w")
        #write out new pangenome
        sequences=[]
        record = SeqRecord(Seq(pan_genome_seq, IUPAC.ambiguous_dna), id="pangenome")
        sequences.append(record)      
        SeqIO.write(sequences, pan_genome_file, "fasta")
        
        
        for block in pangenome:
            for count,info in enumerate(pangenome[block],start=1):
                line = "pangenome\t*\tregion\t%i\t%i\t.\t+\t.\tID=block%s_%i;Name=Block %s;percentage=%i\n" % (info[0],info[1],block,count,block,percentage_block[block])
                out_block_handle.write(line)
        out_block_handle.close()
        block_gff_files={}
        for barcode in barcode_to_info:
            gff_file = os.path.join(directory,barcode+".gff")
            block_gff_files[barcode+"_"+"blocks"]=[gff_file,barcode_to_info[barcode][1]+" Blocks"]
            out_block_handle = open(gff_file,"w")
            for block in blocks:
                #if block['core'] == "true":
                #  continue
                info = block.get(barcode)
                if not info:
                    continue
                block_coords = pangenome.get(block['id'])
                for coords in block_coords:
                    line = "pangenome\t*\tregion\t%i\t%i\t.\t+\t.\tID=%s;Name=Block %s;core=%s;percentage=%i\n" % (coords[0],coords[1],barcode+"_"+block['id'],block['id'],block['core'],block['percentage'])   
                    out_block_handle.write(line)
            out_block_handle.close()
        out_dir= os.path.join("/share_space/jbrowse/pan_genomes","ST131")
        if not os.path.exists(out_dir):
            os.makedirs(out_dir) 
        block_gff_files["all_blocks"]=[all_blocks_gff,"All Blocks"]
        block_gff_files["annotations"]=[os.path.join(directory,"pangenome.gff"),"Annotations"]
        build_sequence_index(pan_genome_file,out_dir,dir_name=True)
        build_gff_files(out_dir,block_gff_files,True)
        data = __get_tracks_json(out_dir,dir_name=True)
        tracks = data['tracks']
        for track in tracks:
            if track['key'].endswith("Blocks"):
                hooks={}
                track['hooks']=hooks
                hooks['modify']= "function(track,f,fdiv){ var per = f.get('percentage');\n fdiv.style.backgroundColor=JBrowseUtilities.colorScale(per)}"
                track['style'] = {"strandArrow":None,
                                  "color":"function(f){ var per = f.get('percentage');return JBrowseUtilities.colorScale(per)}",
                                  "showLabels":None,
                                  "displayMode":"normal"}
            
        
        __save_tracks_json(out_dir,data,dir_name=True)
    
        out_handle = open(os.path.join(directory,"blocks.json"),"w")
        out_handle.write(ujson.dumps(sorted_list))
        out_handle.close()
    

def call_phylotype(assembly_filepointer):
    import subprocess
    from entero.ExtraFuncs.workspace import get_analysis_object
 
    info = {"chuA.1b":["chuA",20],
            "chuA.2":["chuA",20],
            "yjaA.1b":["yjaA",20],
            "yjaA.2b":["yjaA",20],
            "TspE4C2.1b":["Tsp",20],
            "TspE4C2.2b":["Tsp",20],
            "AceK.f":["arpA",20],
            "ArpA1.r":["arpA",20],
            "ArpAgpE.f":["E",24],
            "ArpAgpE.r":["E",24],
            "trpAgpC.1":["C",20],
            "trpAgpC.2":["C",18],
            "aesI.1":["I",20],
            "aesI.2":["I",20],
            "aesII.1":["II",20],
            "aesII.2":["II",20],
             "chuIII.1":["III",20],
            "chuIII.2":["III",19],
            "chuIV.1":["IV",19],
            "chuIV.2":["IV",21],
            "chuV.1":["V",20],
            "chuV.2":["V",21]
            
    }
    gene_to_primer={
        "chuA":["chuA.1b","chuA.2"],
        "yjaA":["yjaA.1b","yjaA.2b"],
        "Tsp":["TspE4C2.1b","TspE4C2.2b"],
        "arpA":["AceK.f","ArpA1.r"],
        "E":["ArpAgpE.f","ArpAgpE.r"],
        "C":["trpAgpC.1","trpAgpC.2"],
        "I":["aesI.1","aesI.2"],
        "II":["aesII.1","aesII.2"],
        "III":["chuIII.1","chuIII.2"],
        "IV":["chuIV.1","chuIV.2"],
        "V":["chuV.1","chuV.2"]
    }
    def get_difference(li):
        left= min(int(i) for i in li)
        right=max(int(i) for i in li)
        return right - left    
  
    for result in results:
        pattern={}
        phylo = "None"
        #if result['ecor']<>'B1':
            #continue
        if not result.get("file_pointer"):
            continue
        out_file="/home/martin/mummer/results.txt"
        primer_file = "/home/martin/mummer/primer.fasta"
        fp = assembly_filepointer
        exe = "/home/zhemin/bitbucket/enterotool/externals/usearch8.0.1623_i86linux32"
        options ="-ublast %s -db %s -rightjust  -evalue 1 -strand both" % (primer_file,fp)
        output = "-blast6out %s " % out_file
        os.system(exe+" "+options+" "+output)
        handle = open(out_file)
        lines = handle.readlines()
        
       
        sites={}
        for line in lines:
            arr= line.split("\t")
            site_info =sites.get(arr[0])
            primer_len = info.get(arr[0])
            if primer_len:
                primer_len=primer_len[1]
                if int(arr[7])<primer_len:
                    continue
            
            if not site_info:
                site_info=[]
                sites[arr[0]]=site_info
            site_info.append(arr)
        
        for gene in gene_to_primer:
           
            primers= gene_to_primer[gene]
          
            primer_1_info = sites.get(primers[0])
            primer_2_info = sites.get(primers[1])
          
            prod_len=0
            if primer_1_info and primer_2_info:
                
                try:
                    for primer_1 in primer_1_info:
                        for primer_2 in primer_2_info:
                            if primer_1[1]==primer_2[1]:
                                prod_len= get_difference([primer_1[8],primer_1[9],primer_2[8],primer_2[9]])
                                if prod_len<500:
                                                                   
                                   
                                    raise Exception
                    for primer_2 in primer_2_info:
                        for primer_1 in primer_1_info:
                            if primer_1[1]==primer_2[1]:
                                prod_len= get_difference([primer_1[8],primer_1[9],primer_2[8],primer_2[9]])
                                if prod_len<500:
                                                                       
                                                        
                                    raise Exception                        
                        
                except Exception as e:
                  
                    pattern[gene]=prod_len

        
    
        if pattern.get('arpA'):
            if pattern.get('chuA'):
                if pattern.get('yjaA'):
                    if pattern.get("E"):
                        phylo="E"
                #arpA+ chuA+ yjaA-
                else:  
                    if pattern.get("E"):
                        phylo="E"
                    else:
                        phylo='D'
            #arpA+ chuA-
            else:
                #
                if pattern.get('yjaA'):
                    if pattern.get("C"):
                        phylo='C'
                    else:
                        phylo="A"
                #arpA+ chuA- yjaA-
                else:
                    if pattern.get('Tsp'):
                        phylo="B1"
                    else:
                        phylo="A"
        #no arpA
        else:
            if pattern.get('chuA') and pattern.get('chuA')<400:
                if not pattern.get("yjaA") and not pattern.get('Tsp'):
                    phylo="F"
                else:
                    phylo='B2'
           
        if phylo=="None":
            if pattern.get("I"):
                phylo='I'
            elif pattern.get("II"):
                phylo='II'
            elif pattern.get("III"):
                phylo='III'            
            elif pattern.get("IV"):
                phylo='IV'             
            elif pattern.get("V"):
                phylo='V'               
        return phylo

def call_IS_positions(database,assemblies,scheme_name):

    dbase = dbhandle[database]
    Schemes=dbase.models.Schemes
    Assemblies = dbase.models.Assemblies
    Lookup= dbase.models.AssemblyLookup
    scheme = dbase.session.query(Schemes).filter_by(description=scheme_name).one()
    ref_is = scheme.param.get("is_reference")
    ref_fasta=scheme.param.get("reference_fasta")
    ref_gbk = scheme.param.get("reference_gbk")
    lookups = dbase.session.query(Lookup).filter(Lookup.assembly_id.in_(assemblies),Lookup.scheme_id == scheme.id).all()
    to_get = set(assemblies)
    for lookup in lookups:
        to_get.discard(lookup.assembly_id)
    
    to_get = map(str,to_get)
    output_dir = os.path.join("/share_space/transposable_elements",scheme_name)
    sql = "SELECT assemblies.id as aid ,traces.read_location as read,assemblies.barcode as bar FROM assemblies INNER JOIN trace_assembly ON assemblies.id= trace_assembly.assembly_id  INNER JOIN traces ON traces.id=trace_assembly.trace_id WHERE assemblies.id IN (%s)" % (','.join(to_get))
    assembly_info = dbase.execute_query(sql)
    for info in assembly_info:
        if not info['read']:
            continue
        direction_arr=[]
        reads_arr = info['read'].split(",")
        output = os.path.join(output_dir,info['bar'])
        for read in reads_arr:
            start_f = read.index("_R")
            end_f =read.index(".",start_f)
            direction_arr.append(read[start_f:end_f])
            
     
        cmd ="ismap --reads  %s   %s  --forward  %s  --reverse %s --queries %s  --typingRef %s --log --runtype typing --directory %s --output %s"\
            % (reads_arr[0],reads_arr[1],direction_arr[0],direction_arr[1], ref_is,ref_gbk,output,info['bar'])
        
        os.system(cmd)
        
        out_files = os.listdir(output)
        full_path_to= os.path.join(output_dir,info['bar']+".txt")
        for fi in out_files:
            if "_table.txt" in fi:
                full_path_from = os.path.join(output,fi)   
                cmd  = "mv %s %s" %(full_path_from,full_path_to)
                os.system(cmd)
                break
        entry = Lookup(assembly_id=info['aid'],scheme_id=scheme.id,status='COMPLETE',other_data={'table_file':full_path_to},st_barcode="CHICKEN_ST")
        dbase.session.add(entry)
        dbase.session.commit()
        cmd = 'rm -rf %s' % output
        os.system(cmd) 
        

def get_is_element_data(database,scheme_name,assembly_ids):
   
    
    dbase = dbhandle[database]
    scheme = dbase.session.query(dbase.models.Schemes).filter_by(description=scheme_name).one()
    Lookups = dbase.models.AssemblyLookup
    lookups = dbase.session.query(Lookups).filter(Lookups.assembly_id.in_(assembly_ids),Lookups.scheme_id==scheme.id).all()
    
    data_dict={}
    for lookup in lookups:
        file_name = lookup.other_data['table_file']
        if not os.path.exists(file_name):
            continue
        barcode =os.path.split(file_name)[1].split(".")[0]
        file_handle = open(file_name)
        lines = file_handle.readlines()
        is_list=[]
        for line in lines:
            item_dict={}
            line =line.strip()
            arr = line.split("\t")
            if arr[5] == 'Novel' or (arr[5].startswith("Novel") and int(arr[4])<50):# or arr[5] == 'Known':
                item_dict['x']=int(arr[2])
                item_dict['type']=arr[5]
                item_dict['rh']=arr[8]
                item_dict['lh']=arr[11]
                is_list.append(item_dict)
        data_dict[barcode]=is_list
        file_handle.close()
                
    return data_dict