import entero
from entero import dbhandle, get_download_scheme_folder
import marshmallow as ma
from flask import url_for

class GenericSchema(ma.Schema):
    barcode = ma.fields.List(ma.fields.String(), load_only=True)
    offset =  ma.fields.Integer(missing=0)
    limit =  ma.fields.Integer(missing=50)
    orderby =  ma.fields.String(missing='barcode')
    sortorder =  ma.fields.String(missing='asc')
    only_fields = ma.fields.List(ma.fields.String(), load_only=True)
    lastmodified = ma.fields.DateTime(dump_only=True)

class NServSchema(ma.Schema):
    lastmodified = ma.fields.DateTime(attribute='lastmodified',dump_only=True)
    barcode = ma.fields.List(ma.fields.String(), load_only=True)
    create_time = ma.fields.DateTime(attribute='create_time')
    limit =  ma.fields.Integer(missing=50)
    only_fields = ma.fields.List(ma.fields.String(), load_only=True)
    offset =  ma.fields.Integer(missing=0)

class TopSchema(ma.Schema):
    name = ma.fields.String()
    description = ma.fields.String()
    prefix = ma.fields.String()
    def get_description(self, field_name):
        description = dict(
            prefix = 'Database prefix, e.g. SAL for Salmonella',
            name = 'Species database name (senterica, ecoli, yersinia, mcatarrhalis) for Salmonella, Escherichia, Yersinia, Moraxella respectively',
            description = 'Database description'
        )
        return description.get(field_name, '')
        
class LoginSchema(ma.Schema):
    username = ma.fields.String()
    password = ma.fields.String()
    token = ma.fields.String(dump_only=True)
    def get_description(self, field_name):
        description = dict(
            username = 'EnteroBase username',
            password = 'EnteroBase Password'
        )
        return description.get(field_name, '')
    
    
class AllelesSchema(NServSchema):
#    [u'allele_id', u'lastmodified_by', u'scheme', u'seq', u'reference', u'index_id', u'barcode', u'locus', u'create_time', u'_combo_tag', u'version', u'lastmodified', u'locus_index', u'accepted', u'allele_name']
    seq = ma.fields.String(attribute ='seq')
    allele_id = ma.fields.String(attribute='allele_id')
    locus = ma.fields.String(required=True,attribute='locus')
    reldate = ma.fields.Integer(load_only= True)    
    allele_barcode = ma.fields.String(dump_only=True, attribute='barcode')
    create_time = ma.fields.DateTime(attribute='create_time', dump_only=True)
    
    def get_description(self, field_name):
        description = dict(
            database = 'Species database name (senterica, ecoli, yersinia, mcatarrhalis) for Salmonella, Escherichia, Yersinia, Moraxella respectively',
            barcode = 'Unique barcode for Strain records, <database prefix>_<ID code> e.g. SAL_AA0001AA'
        )
        return description.get(field_name, '')
    
class LociSchema(NServSchema):
    locus = ma.fields.String(attribute='locus')
    locus_barcode = ma.fields.String(dump_only=True, attribute='barcode')
    database = ma.fields.String(dump_only=True)
    scheme = ma.fields.String(attribute='scheme')
    
    download_alleles_link = ma.fields.Method("get_loci" ,  dump_only=True)
    
    def get_loci(self, obj):

        locus = obj.get('locus')
        folder=get_download_scheme_folder(obj['database'], obj['scheme'])
        if locus  and obj.get('scheme'):
            if locus=='ECs1189':
                print locus

            if not folder:
                return 'http://enterobase.warwick.ac.uk/schemes/%s.%s/%s.fasta.gz' %(obj['database'], obj['scheme'], locus)
            else:
                return 'http://enterobase.warwick.ac.uk/schemes/%s/%s.fasta.gz' % (folder, locus)

                #return url_for('main.download_scheme_data', species=obj['database'],scheme=obj['scheme'],allele=locus,_external = True)
        return None
        
    
    def get_description(self, field_name):
        description = dict(
            database = 'Species database name (senterica, ecoli, yersinia, mcatarrhalis) for Salmonella, Escherichia, Yersinia, Moraxella respectively',
            barcode = 'Unique barcode for Strain records, <database prefix>_<ID code> e.g. SAL_AA0001AA'
        )
        return description.get(field_name, '')
    
class StsSchema(NServSchema):
    #[u'info', u'lastmodified_by', u'ST_id', u'scheme', u'version', u'reference', u'alleles', u'barcode', u'scheme_index', u'create_time', u'lastmodified', u'accepted', u'index_id']

    st_id = ma.fields.String(attribute = 'type_id')
    ST_id = ma.fields.String(attribute = 'ST_id', dump_only=True)
    show_alleles = ma.fields.Boolean(load_only=True)    
    alleles = ma.fields.List(ma.fields.Dict(), dump_only=True)
    st_barcode = ma.fields.String(dump_only=True, attribute='barcode')
    barcode_link = ma.fields.URL(dump_only=True)
    reference = ma.fields.Dict(dump_only=True, attribute = 'reference')
    reldate = ma.fields.Integer(load_only= True)    
    create_time = ma.fields.String(attribute='create_time', dump_only=True)
    
    info = ma.fields.Dict(dump_only=True, attribute='info')
    scheme = ma.fields.String(attribute='scheme')
    
    def get_description(self, field_name):
        description = dict(
            database = 'Species database name (senterica, ecoli, yersinia, mcatarrhalis) for Salmonella, Escherichia, Yersinia, Moraxella respectively',
            barcode = 'Unique barcode for Strain records, <database prefix>_<ID code> e.g. SAL_AA0001AA'
        )
        return description.get(field_name, '')

class StrainSchema(GenericSchema):
    
    created = ma.fields.DateTime(dump_only=True)    
    comment = ma.fields.String()
    strain_name = ma.fields.String(attribute = 'strain')
    lab_contact = ma.fields.String(attribute = 'contact')
    source_niche = ma.fields.String()
    source_type = ma.fields.String()
    source_details = ma.fields.String()
    collection_year =  ma.fields.Integer()
    sample_accession = ma.fields.String()
    collection_month =  ma.fields.Integer()
    collection_date =  ma.fields.Integer()
    collection_time =  ma.fields.Time()
    continent = ma.fields.String()
    country = ma.fields.String()
    region = ma.fields.String(attribute = 'admin1')
    county = ma.fields.String(attribute = 'admin2')
    city = ma.fields.String()
    postcode = ma.fields.String()
    secondary_sample_accession = ma.fields.String()
    assembly_barcode = ma.fields.String(attribute = 'best_assembly')
    antigenic_formulas = ma.fields.String()
    latitude = ma.fields.Float()
    longitude = ma.fields.Float()
    my_strains = ma.fields.Boolean()
    substrains = ma.fields.Boolean()
    return_all = ma.fields.Boolean()
    strain_barcode = ma.fields.String(dump_only=True, attribute='barcode')
    uberstrain = ma.fields.String()
    serotype = ma.fields.String(attribute = 'serotype')    
    reldate = ma.fields.Integer(load_only= True)    
    version = ma.fields.Integer()
    
    
    # E coli only
    ecor = ma.fields.String(dump_only=True)
    serological_group = ma.fields.String(dump_only=True)
    simple_disease = ma.fields.String(dump_only=True)
    path_nonpath = ma.fields.String(dump_only=True)
    species = ma.fields.String(dump_only=True)
        
    
    
    def get_description(self, field_name):
        description = dict(
            database = 'Species database name (senterica, ecoli, yersinia, mcatarrhalis) for Salmonella, Escherichia, Yersinia, Moraxella respectively',
            barcode = 'Unique barcode for Strain records, <database prefix>_<ID code> e.g. SAL_AA0001AA',
            limit = 'Number of results per page',
            offset = 'Cursor position in results',
            sortorder = 'Order of search results: asc or desc',
            orderby = 'Field to order by. Default: barcode'
        )
        return description.get(field_name, '')    
    
    
class TracesSchema(GenericSchema):
    trace_barcode = ma.fields.String(dump_only=True, attribute='barcode')
    offset =  ma.fields.Integer(missing=0)
    limit =  ma.fields.Integer(missing=50)
    # WVN 9/5/17 Below override of GenericSchema appeared to prevent getting the correct default for orderby and sortorder with the "traces" API query method
    # (unlike the "strains" API query method where there is not a similar override in the StrainSchema class)
    #orderby =  ma.fields.String('barcode')
    #sortorder =  ma.fields.String('asc')    
    
    def get_description(self, field_name):
        description = dict(
            database = 'Species database name (senterica, ecoli, yersinia, mcatarrhalis) for Salmonella, Escherichia, Yersinia, Moraxella respectively',            
            barcode = 'Unique barcode for Traces records, <database prefix>_<ID code>_TR e.g. SAL_AA0001AA_TR',
            limit = 'Number of results per page',
            offset = 'Cursor position in results',
            sortorder = 'Order of search results: asc or desc',
            orderby = 'Field to order by. Default: barcode'
        )
        return description.get(field_name, '')

class SchemeSchema(GenericSchema):
    scheme_barcode = ma.fields.String(dump_only=True, attribute = 'barcode')
    scheme_name = ma.fields.Str(attribute = 'description')    
    version = ma.fields.Integer()
    created = ma.fields.DateTime()
    lastmodified = ma.fields.DateTime()
    label = ma.fields.String(attribute = 'name')
    download_sts_link = ma.fields.Method("download_scheme", dump_only=True)

    
    
    def get_description(self, field_name):
        description = dict(
            database = 'Species database name (senterica, ecoli, yersinia, mcatarrhalis) for Salmonella, Escherichia, Yersinia, Moraxella respectively',
            barcode = 'Unique barcode for Strain records, <database prefix>_<ID code> e.g. SAL_AA0001AA',
            limit = 'Number of results per page',
            offset = 'Cursor position in results',
            sortorder = 'Order of search results: asc or desc',
            orderby = 'Field to order by. Default: barcode'
        )
        return description.get(field_name, '')     

    def download_scheme(self, dat):
        #K.M  07/09/2018
        #This is the methos which we may need to modify to use better download links
        pipeline_scheme = dat.get_param('scheme')
        if pipeline_scheme:
            if pipeline_scheme != 'rMLST':
                pipeline_species = pipeline_scheme.split('_')[0]
                pipeline_name = pipeline_scheme.split('_')[1]
                #Call this metod to get more logical folder name if th eone in the database is not good enough
                #if it returns None, it will use the one in the database
                folder=get_download_scheme_folder(pipeline_species, pipeline_name)
                if not folder:
                    return 'http://enterobase.warwick.ac.uk/schemes/%s.%s/profiles.list.gz' %(pipeline_species, pipeline_name)
                else:
                    return 'http://enterobase.warwick.ac.uk/schemes/%s/profiles.list.gz' %folder
                # return url_for('main.download_scheme_data', species=pipeline_species,scheme=pipeline_name,allele='profiles',_external = True)
        return None
    
    
    
class AssembliesSchema(GenericSchema):
    
    
    id = ma.fields.Integer(dump_only = True)
    download_fasta_link = ma.fields.Method("get_fasta_link", dump_only=True)
    download_fastq_link = ma.fields.Method("get_fastq_link", dump_only=True)
    n50 =  ma.fields.Integer()
    assembly_status = ma.fields.String(attribute = 'status', load_from='assembly_status', dump_to='assembly_status')
    top_species = ma.fields.String()
    uberstrain = ma.fields.String()
    assembly_barcode = ma.fields.String(dump_only=True, attribute='barcode')
    reldate = ma.fields.Integer(load_only= True)    
    version = ma.fields.Integer()    
    coverage = ma.fields.Integer(dump_only=True)    
    
    def get_fasta_link(self, obj):
        if str(obj.status) !=  'Assembled': 
            return 'None'
        database_name = None
        for key in entero.config.Config.DB_CODES:
            if entero.config.Config.DB_CODES[key][1] == obj.barcode[0:3]:
                database_name = key 
                break        
        return  url_for('upload.download_assemblies',  _external=True) + '?assembly_barcode=%s' % obj.barcode + '&database=%s' % database_name
        
    def get_fastq_link(self, obj):
        if str(obj.status) !=  'Assembled': 
            return 'None'
        database_name = None
        for key in entero.config.Config.DB_CODES:
            if entero.config.Config.DB_CODES[key][1] == obj.barcode[0:3]:
                database_name = key 
                break        
        return  url_for('upload.download_assemblies',  _external=True) + '?assembly_barcode=%s' % obj.barcode + '&database=%s&file_format=fastq' % database_name                
        

    def get_description(self, field_name):
        description = dict(
            database = 'Species database name (senterica, ecoli, yersinia, mcatarrhalis) for Salmonella, Escherichia, Yersinia, Moraxella respectively',            
            barcode = 'Unique barcode for Traces records, <database prefix>_<ID code>_AS e.g. SAL_AA0001AA_AS',
            limit = 'Number of results per page',
            offset = 'Cursor position in results',
            sortorder = 'Order of search results: asc or desc',
            download_link = 'Direct link to download asembly file',
            orderby = 'Field to order by. Default: barcode'
        )
        return description.get(field_name, '')
    
class StrainDataSchema(GenericSchema):
    # attribute should be database table field name 
    created = ma.fields.DateTime(dump_only=True)
    comment = ma.fields.String()
    strain_name = ma.fields.String(attribute = 'strain')
    lab_contact = ma.fields.String(attribute = 'contact')
    serotype = ma.fields.String(attribute = 'serotype')
    secondary_sample_accession = ma.fields.String()
    sample_accession = ma.fields.String()
    source_niche = ma.fields.String()
    source_type = ma.fields.String()
    source_details = ma.fields.String()
    collection_year =  ma.fields.Integer()
    collection_month =  ma.fields.Integer()
    collection_date =  ma.fields.Integer()
    collection_time =  ma.fields.Time()
    continent = ma.fields.String()
    country = ma.fields.String()
    region = ma.fields.String(attribute = 'admin1')
    county = ma.fields.String(attribute = 'admin2')
    city = ma.fields.String()
    postcode = ma.fields.String()
    latitude = ma.fields.Float()
    longitude = ma.fields.Float()
    email = ma.fields.String()
    my_strains = ma.fields.Boolean()
    substrains = ma.fields.Boolean()
    orderby =  ma.fields.String('barcode')
    n50 =  ma.fields.Integer()
    assembly_status = ma.fields.String(attribute = 'status', load_from='assembly_status', dump_to='assembly_status')
    top_species = ma.fields.String()
    uberstrain = ma.fields.String()
    strain_barcode = ma.fields.String(dump_only=True, attribute='barcode')
    reldate = ma.fields.Integer(load_only= True)
    version = ma.fields.Integer()
    download_fasta_link = ma.fields.Method("get_fasta_link", dump_only=True)
    download_fastq_link = ma.fields.Method("get_fastq_link", dump_only=True)
    custom_fields = ma.fields.String()
    
      
    # E coli only
    ecor = ma.fields.String(dump_only=True)
    serological_group = ma.fields.String(dump_only=True)
    simple_disease = ma.fields.String(dump_only=True)
    path_nonpath = ma.fields.String(dump_only=True)
    species = ma.fields.String(dump_only=True)
    
    
    def get_fasta_link(self, obj):
        if str(obj.assembly.status) !=  'Assembled': 
            return 'None'
        database_name = None
        for key in entero.config.Config.DB_CODES:
            if obj.barcode:
                if entero.config.Config.DB_CODES[key][1] == obj.barcode[0:3]:
                    database_name = key 
                    break        
        return  url_for('upload.download_assemblies',  _external=True) + '?assembly_barcode=%s' % obj.assembly.barcode + '&database=%s' % database_name    
    
    
    def get_fastq_link(self, obj):
        if str(obj.assembly.status) !=  'Assembled': 
            return 'None'
        database_name = None
        for key in entero.config.Config.DB_CODES:
            if obj.barcode:
                if entero.config.Config.DB_CODES[key][1] == obj.barcode[0:3]:
                    database_name = key 
                    break        
        return  url_for('upload.download_assemblies',  _external=True) + '?assembly_barcode=%s' % obj.assembly.barocode + '&database=%s&file_format=fastq' % database_name        
    
    
    def get_description(self, field_name):
        description = dict(
            database = 'Species database name (senterica, ecoli, yersinia, mcatarrhalis) for Salmonella, Escherichia, Yersinia, Moraxella respectively',            
            barcode = 'Unique barcode for Traces records, <database prefix>_<ID code>_AS e.g. SAL_AA0001AA_AS',
            limit = 'Number of results per page',
            offset = 'Cursor position in results',
            sortorder = 'Order of search results: asc or desc',
            orderby = 'Field to order by. Default: strain barcode'
        )
        return description.get(field_name, '')

    
class AssemblyLookupSchema(ma.Schema):
    st_barcode = ma.fields.String()
    version = ma.fields.String()
    st_id = ma.fields.Integer(dump_only=True)
    lineage = ma.fields.String(dump_only=True)
    species = ma.fields.String(dump_only=True)
    subspecies = ma.fields.String(dump_only=True)
    st_complex = ma.fields.String(dump_only=True)
    predicted_serotype = ma.fields.String(dump_only=True)
    timestamp = ma.fields.String(dump_only=True)
    
    
    
    
class LookupSchema(ma.Schema):
    barcode = ma.fields.String()
    
    def get_description(self, field_name):    
        description = dict(
            barcode = 'Unique barcode for Traces records, <database prefix>_<ID code>_<Table code> e.g. SAL_AA0001AA_ST'
        )
        return description.get(field_name, '')    
    