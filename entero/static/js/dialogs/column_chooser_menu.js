 $.widget("custom.menuplus",$.ui.menu,{
       _closeOnDocumentClick: function( event ) {
                if ($(event.target).hasClass("allele-input")){
                        return false;
                }
                $(".allele-input").hide();
                
            
                return true;
        },
        refresh: function() {
                var menus,
                        items,
                        icon = this.options.icons.submenu,
                        submenus = this.element.find( this.options.menus );

                this.element.toggleClass( "ui-menu-icons", !!this.element.find( ".ui-icon" ).length );

                // Initialize nested menus
                submenus.filter( ":not(.ui-menu)" )
                        .addClass( "ui-menu ui-widget ui-widget-content ui-corner-all" )
                        .hide()
                        .attr({
                                role: this.options.role,
                                "aria-hidden": "true",
                                "aria-expanded": "false"
                        })
                        .each(function() {
                                var menu = $( this ),
                                        item = menu.parent(),
                                        submenuCarat = $( "<span>" ).css("float","right")
                                                .addClass( "glyphicon glyphicon-menu-right")
                                                .data( "ui-menu-submenu-carat", true );

                                item
                                        .attr( "aria-haspopup", "true" )
                                        .prepend( submenuCarat );
                                menu.attr( "aria-labelledby", item.attr( "id" ) );
                        });

                menus = submenus.add( this.element );
                items = menus.find( this.options.items );

                // Initialize menu-items containing spaces and/or dashes only as dividers
                items.not( ".ui-menu-item" ).each(function() {
                        var item = $( this );
                        // hyphen, em dash, en dash
                        //if ( !/[^\-\u2014\u2013\s]/.test( item.text() ) ) {
                        //	item.addClass( "ui-widget-content ui-menu-divider" );
                        //}
                });

                // Don't refresh list items that are already adapted
                items.not( ".ui-menu-item, .ui-menu-divider" )
                        .addClass( "ui-menu-item" )
                        .uniqueId()
                        .addClass( "ui-corner-all" )
                        .attr({
                                tabIndex: -1,
                                role: this._itemRole()
                        });

                // Add aria-disabled attribute to any disabled menu item
                items.filter( ".ui-state-disabled" ).attr( "aria-disabled", "true" );

                // If the active item has been removed, blur the menu
                if ( this.active && !$.contains( this.element[ 0 ], this.active[ 0 ] ) ) {
                        this.blur();
                }
        }

        
    });
ColumnChooserMenu.prototype = Object.create(null);
ColumnChooserMenu.prototype.constructor= ColumnChooserMenu;

/**
* Enables users to choose either an experimental column or custom column
* @constructor
* @param {string} database - The name of the database
*/
function ColumnChooserMenu(database,callback){
        
        this.database = database;
        this.callback=callback;
        this.allele_autocompletes={};
        this._init();
        this.open_allele_complete=null;
}

ColumnChooserMenu.prototype.showErrorMessage=function(msg){
        Enterobase.modalAlert(msg);
}
ColumnChooserMenu.prototype._init


ColumnChooserMenu.prototype._init=function(){
        this.menu = $("<ul>");
        var self =this;
        var experiment_call= Enterobase.call_restful_json("/get_experiment_details?database="+this.database,"GET","",this.showErrorMessage);
        var custom_column_call= Enterobase.call_restful_json("/get_custom_view_information?database="+this.database,"GET","",this.showErrorMessage);
        $.when(experiment_call,custom_column_call).done(function(exp_data,col_data){
                var experiments=exp_data[0]['experiments'];
                var custom_columns= col_data[0]['custom_column'];
                var exp_menu=$("<li>");
                var col_menu=$("<li>");
                exp_menu.append("<div>Experiments</div>");
                var exp_list= $("<ul>").css("width","200px");
                
                exp_menu.append(exp_list);
                var col_list=$("<li>");
                col_list.append("<div>Custom Columns</div>");
                col_menu.append(col_list);
                self.menu.append(exp_menu).append(col_menu);
                for (var exp_name in experiments){
                        var exp= experiments[exp_name];
                        var li = $("<li>");
                        exp_list.append(li);
                        li.append("<div>"+exp['label']+"</div>")
                        var sublist = $("<ul>").css("width","200px");
                        li.append(sublist);
                        var is_mlst=false;
                        for (var field_name in exp['fields']){
                                
                                var field=exp['fields'][field_name];
                                if (field['group_name']=='Locus'){
                                        is_mlst=true;
                                        continue
                                }
                                var  field_li=$("<li>").data({"experiment":exp_name,"field":field_name});
                                field_li.append("<div>"+field['label']+"<div>");
                                sublist.append(field_li);
                        
                        }
                        if (is_mlst){
                                var allele_li = $("<li>").data({"alleles":true,"experiment":exp_name});
                                allele_li.append("<div>Alleles<span style='float:right' class ='glyphicon glyphicon-menu-right'></span></div>");
                                sublist.append(allele_li);
                                self.allele_autocompletes[exp]=null;
                               // var sub_sub_list = $("<ul>").css({"width":"200px","height":"50px"});
                               // allele_li.append(sub_sub_list);
                              //  var chooser_li =$("<li>").css({"width":"200px","height":"50px"});
                              //  var input =$("<input type='text' name='firstname' value='John'>").css({"width":"100px","height":"35px"});
                               // input.val("wjsjdjsjd");
                                //chooser_li.append(input);
                        
                               // sub_sub_list.append(input);
                                
                        }
                      
                      
                        
                        
                }
                self.menu.css({"width":"200px","position":"absolute","left":"30px","top":"30px"});
                $("body").append(self.menu);
                self.menu.menuplus({
                        select:function(event,ui){
                                var item =ui.item[0];
                        
                                var exp = item.data("experiment");
                      
                                var name = item.data("field");
                                if (!name){
                                
                                }
                                callback(exp,name);
                        
                        
                
                        },
                        disabled:false,
                        focus:function(event,ui){
                                var item =$(ui.item[0]);
                                var exp = item.data("experiment");
                              
                                if (item.data("alleles")){
                                                self._showAlleleDialog(exp,item);
                        
                            
                                }
                                else{
                                $(".allele-input").hide();
                                
                                }
                              
                           
                
                        }
             
                        
                
                });
        });

}

ColumnChooserMenu.prototype._showAlleleDialog=function(experiment,parent){
        var self = this;
        var auto = this.allele_autocompletes[experiment];
        if (! auto){
                var name  = experiment+"-cs-auto-input";
                var input = $("<input>").css({"position":"absolute","display":"none"}).attr({"id":name,"class":"allele-input"});
                $("body").append(input);
                new Enterobase.LocusSuggest(name,this.database,experiment,function(name,label){
                        self._alleleChosen(experiment,name,label);
                });
                auto  = this.allele_autocompletes[experiment]=input;
                  auto.position({my: "left top",
                                at: "right top",
                                of: parent});
        }
      auto.show();
         
      
        this.open_allele_complete=auto;
        
}

ColumnChooserMenu.prototype._alleleChosen=function(experiment,name,label){
        this.allele_autocompletes[experiment].remove();
       this.allele_autocompletes[experiment]=null;
      
     
        
}