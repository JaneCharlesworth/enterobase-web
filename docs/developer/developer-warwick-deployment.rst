Updating Warwick Production Server
==================================
**These notes only apply to developers at University of Warwick.**

There are three production servers olkep,tomva and karmo. karmo is the main
front end running nginx and passes normal requests to either itself or tomva.
Requests for uploading reads and the api are sent to olkep Push your changes
to the master branch on bitbucket, the this needs to be propagated to the
three servers

** setup for code changes(hercules)

* ssh to host using yoor university user name and private key
* cd /home/admin/enterobase-web
* sudo -u admin git pull
* sudo service enterobase restart

** setup for static files changes, e.g JavaScripts (karmo)

* ssh to karmo using enterobase and the normal admistrative password (or key)
* cd /var/www/entero
* sudo -u enterobase git pull