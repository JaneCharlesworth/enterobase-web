var wholeData;//declare to stort the whole data
var allRoot={};//declare var for storing the root directory of all folder and file 
var shareLoadButton="";
var current_customViewId;
//aded by yulei, 20190804
var rightGridType="";
	
EnteroWSTree.prototype = Object.create(null);
EnteroWSTree.prototype.constructor= EnteroWSTree;

/*
 * Creates a new Tree, for the manipulation of workspaces
 * @constructor
 * @param {string} elementID The id of the element displaying the tree
 * @param {object} folder_dat The folder data for the tree in the following format:-
* <pre>
{
	"RN":{
		id:"RN",
		workspaces:[233,4566,...]
		children:['f1',...],
		text:"RN"
     },
	{"f1",:{
        id:"f1",
        workspaces:[...],
        children:[...],
		text:"Folder 1"
	},
	......
}
* </pre>
* @param {EnteroWorkspaceDialog} parent The parent dialog, which holds information on the workspaces
* @param {string} type The tree type - either public,shared or main
 */

 //this method is used to detmine the node id for a folder now using its text
 //K.M. 22/10/2019
 function get_node_id(folder_data, title){
 for (var name in folder_data){
		title_=title.toLowerCase();
		var item = folder_data[name];
		//alert (Object.getOwnPropertyNames(item));
		//text,id,children,type
		if (title_==item.text.toLowerCase()){
		//alert ("I found itL "+name+", "+item.id);
		return item.id;
		}

		}
 }
function EnteroWSTree(elementID,folder_data,parent,type, folder_type, folder_name_){
	this.elementID="#"+elementID;
	var self=this;
	this.type=type;
	this.parent=parent;
	this.filter_function=null;
	this.structure_altered=false;
	for (var name in folder_data){
		var item = folder_data[name];
		item.type='folder';	
		if (item.children){
			for (var index in item.children){
				item.children[index]=folder_data[item.children[index]];
			}
		}
		if (item.workspaces){
			if (! item.children){
				item.children=[];               
			}
			for (var index in item.workspaces){
				var ws_id = item.workspaces[index];
				var ws = this.parent.workspaces[ws_id];
				if (!ws){
					continue;
				}
				var text = this.getWorkspaceText(ws);
				
				item.children.push({
					type:ws[4],
					text:text,
					id:ws[3],
				});
			}
		}
		delete item.workspaces;
	}
	types = {};
	for (var type in parent.workspace_types){
		types[type]={icon:parent.workspace_types[type]['icon']};
	
	}
	types['folder']={};
	
	$(this.elementID).jstree({
		plugins:['dnd','sort','types',"changed","unique","contextmenu","search"],
		core : {
			data : [folder_data['RN']],
			check_callback : function(operation, node, parent, position, more) {
				if (operation === 'move_node'){
					if (!parent){
						return false;
					}
					var ws=self.parent.workspaces[node.id];
					if (ws && ws[1]==='temp'){
						return false;
					}
					//only move  into folders
					if (parent.type==='folder'){
						if (self.type==='public' && ! self.parent.is_administrator){
							//can only move into own subfolder
							var user_id=self.parent.user_details[0]+"";
							if (parent.id===user_id || parent.parents.indexOf(user_id)!==-1){
								return true
							}
							return false
						}
						return true;
					}
					return  false;
				}
				return true;
			}
		},
		search:{
			show_only_matches:true,
			case_insensetive:true
		},
		dnd : {
			is_draggable : function(node) {
				var type = node[0].type;
				//subtrees
				if (type === 'snp_project' || type ==='ms_tree' ){
					return false;
				}
				if (self.type==='shared'){
					return false;
				}
				if (self.type==='public'){
					if (self.parent.is_administrator){
						return true;
					}
					//is this in the owners directory
					if (node[0].parents.indexOf(self.parent.user_details[0]+"")!==-1){
						return true
					}
					return false;
				}
				return true;
			}
		},
		contextmenu:{"items":function(node){
						return self._getContextMenu(node);
					}
		},
		sort:function(a,b){
			   a1 = this.get_node(a);
				b1 = this.get_node(b);
	if (a1.type==='folder' && b1.type!=='folder'){
	    return -1
	}else if (a1.type!=='folder' && b1.type==='folder'){
		return 1;
	
	}
	else if (a1.type!=="folder"){
		var ta1 = self.parent.workspaces[a1.id][0].toLowerCase();
		var tb1 = self.parent.workspaces[b1.id][0].toLowerCase();
	    return (ta1> tb1) ? 1 : -1;
	}
	else{
		return (a1.text.toLowerCase()> b1.text.toLowerCase()) ? 1 : -1;
	}
		},
		types: types
	});

	$(this.elementID).bind("rename_node.jstree",function(node,ref){
		if (ref.node.type !=='folder'){
			var ws = self.parent.workspaces[ref.node.id];
			var new_name =ref.text;
			var url = "/rename_workspace?workspace_id="+ref.node.id
			url+="&name="+new_name
			Enterobase.call_restful_json(url,"GET","",self.showErrorMessage).done(function(data){
				if (data['status']==='OK'){
					ws[0]=new_name
				}
				else{
					Enterobase.modalAlert(data['msg']);
				}
				//relabel the tree
				$(self.elementID).jstree('set_text', ref.node , self.getWorkspaceText(ws));
				var tree= $(self.elementID).jstree(true);
				var parent= tree.get_node(ref.node.parent)
				tree.sort(parent,true);
				tree.redraw_node(parent, true);
			});
		}
		else{
			self.structure_altered=true;
		}
	})
	.bind("move_node.jstree", function (e, data) {
        self.structure_altered=true;
	}).bind("loaded.jstree", function (event, data) {
        // you get two params - event & data - check the core docs for a detailed description
	if (elementID==folder_type);
       $(self.elementID).jstree("open_all");
//add event lisner to select the folder if it is provided
//K.M. 22/10/2019
    }).bind("ready.jstree", function (event, data) {
    
    if (elementID==folder_type){
    node_id=get_node_id(folder_data, folder_name_);
         if (node_id!=null)
	 {
        //$(self.elementID).jstree("open_all");
	$(self.elementID).jstree('select_node', node_id);//'j3_95');
	$(self.elementID).jstree(true).get_node(node_id, true).children('.jstree-anchor').focus();
	}
	else
	{
	   msg=folder_name_+" is not Found, please check the folder name";
	   Enterobase.modalAlert(msg);
	   //alert (msg);
	}

	}
	});
	var self  = this;
	setTimeout(function(){
		for (var index in self.parent.workspaces){
		if (self.parent.workspaces[index].length===5){
			$("#"+self.parent.workspaces[index][3]).css("font-weight","bold");
		}
	}
	},300);
};


/** Gets the text to use in the tree for a workspace 
* @param {list} the workspace (see {@llink EnteroWorkspaceDialog})
* @reurns {string} The label e.g. big tree (public)
*/
EnteroWSTree.prototype.getWorkspaceText=function(ws){
	var text = ws[3] +" - "+ ws[0];
	if (ws[1] !== 'mine' && this.type==='main'){
		text+="("+ws[2]+")";
	}
	//new workspaces (not relevant anymore?)
	if (ws.length===6){
		text=text+"*";
	}
	return text;
}

EnteroWSTree.prototype._getContextMenu=function(node){
	var ws = this.parent.workspaces[node.id];
	var items={};
	var self = this;
	var admin = this.parent.is_administrator;
	var user_id = this.parent.user_details[0]+"";
	var user_public = (this.type==='public' && !admin && node.parents.indexOf(user_id)!==-1)
	var empty_folder= (node.type=='folder' && node.children.length===0);
	var public_edit = (this.type==='public' && admin);
	var is_mine = (ws && ws[1]==='mine');
	
	//normal users rename own workspaces and folders in own tree plus
	//workspaces that are in their folder in public
	//admin can also rename anything in public
	if (is_mine || (node.type==='folder' && this.type==="mine") || public_edit || user_public){
		items['rename']= {
			label:"Rename",
			action:function(data){
				var inst = $.jstree.reference(data.reference);
				var obj = inst.get_node(data.reference);
				//change text to  name if not a folde 
				if (ws){
					obj.text=ws[0];
				}
				inst.edit(obj);
			}	
		};
	}
	if((this.type==='main' &&  (empty_folder || is_mine))  
			|| (public_edit && empty_folder ) || (public_edit && ws)){
		items['delete']={
			label:"Delete",
			action:function(data){
				var inst = $.jstree.reference(data.reference);
				var obj = inst.get_node(data.reference);
				if (obj.type==='folder'){
					$(self.elementID).jstree("delete_node",[obj.id]);
				}
				else{
					self.parent.deleteWorkspaces([obj.id]);
				}
			}
		};
	}
	if(this.type==='main' && is_mine){
		items['make_public']={
			label:"Make Public",
			action:function(data){
				var inst = $.jstree.reference(data.reference);
				var obj = inst.get_node(data.reference);
				self.parent.makePublic(obj.id)
			}
		}
	}
	if (ws && public_edit){
		items['make_private']={
			label:"Make Private",
			action:function(data){
				var inst = $.jstree.reference(data.reference);
				var obj = inst.get_node(data.reference);
				self.parent.makePrivate(obj.id)
			}
		}
	}
	if (this.type!=='main' && node.type!=="folder"){
		items['copy']={
			label:"Copy to Mine",
			action:function(data){
				var inst = $.jstree.reference(data.reference);
				var obj = inst.get_node(data.reference);
				var ws = self.parent.workspaces[obj.id];
				self.parent.copyToMainTree(ws);
			
			}
		}
	}
	if (this.type==='main' && !is_mine && node.type!=='folder'){
		items['remove']={
			label:"Remove",
			action:function(data){
				var inst = $.jstree.reference(data.reference);
				var obj = inst.get_node(data.reference);
				self.deleteNodes([obj.id]);
			
			}
		}
	}
	//can add folders to their own or adimns to public or users to their own in public
	if (this.type==='main' || public_edit || node.id===user_id || node.parents.indexOf(user_id)!==-1){
		items['new_folder']={
			label:"New Folder",
			action:function(data){
				var inst = $.jstree.reference(data.reference);
				
				var parent = self.getSelectedFolder();
				var id = self.createNewFolder(parent,"NewFolder");
				var obj = inst.get_node(id);
				inst.edit(obj);
			}	
		}
	}
	return items;
};


/**
*Deletes nodes on the tree (does not delete the actual items)
* @param {list} nodes An arrays of node ids (wokspaces node id's are the same as their own id) 
*/
EnteroWSTree.prototype.deleteNodes = function(nodes){	
	$(this.elementID).jstree("delete_node",nodes);
	this.structure_altered=true;
}

/**
* Any nodes containing the supplied text will be highlighted 
* @param {string} text The search text
*/
EnteroWSTree.prototype.searchTree = function(text){
	$(this.elementID).jstree("search",text);
}


/**
* Hides any nodes in the tree according to the supplied filter e.g. to display only workspaces that the user owns:- 
*<pre>
    function(ws){return (ws[1]!=='mine')}
* </pre>
* @param {function} filter A function which should return true for any workspace to be filtered out. The 
* function is supplied with the workspace array , see {@link EnteroWSTree}
*/
EnteroWSTree.prototype.hideNodes=function(filter){
	var treeData = $(this.elementID).jstree(true).get_json('#', {flat:true});
	for (var index in treeData){
		var node_id =  treeData[index].id;
		var ws = this.parent.workspaces[node_id];
		if (!ws){
			continue;
		}
		if (filter(ws)){
			$(this.elementID).jstree("hide_node",node_id);
		}
	
	}

}

/** Show all nodes and remove any filters
*/
EnteroWSTree.prototype.showAllNodes=function(){
	this.filter_function=null;
	$(this.elementID).jstree("show_all");
}

/** Changes the node's text (does not actually rename the workspace/folder)
* @param {string} id The id of the node
* @param {string} text The text to be displayed in the node
*/
EnteroWSTree.prototype.changeNodeText = function(node,text){	
	$(this.elementID).jstree('set_text', node , text);
}


/** Gets the folder structure of the tree in a flat format
* @returns {object} A dictionary containing the folder structure see {@link EnteroWSTree}
*/
EnteroWSTree.prototype.getTreeData = function(){
	var data = {};
	var treeData = $(this.elementID).jstree(true).get_json('#', {flat:true});
			for (var index in treeData){
				var node = treeData[index];
				if (node.type==='folder' || node.type ==='default'){
					var node_info = {id:node.id,workspaces:[],children:[],text:node.text};
					data[node.id]=node_info;
				}
				if (node.parent !=="#"){
					var parent = data[node.parent];
					if (node.type ==='folder' || node.type ==='default'){
						parent.children.push(node.id);
					}
					else{
						//don't  save temp
						var info = this.parent.workspaces[node.id];
						if (info &&  info[1]==='temp'){
							continue;
						
						}
						//don't  save the new tag
						if(info.length===6){
							info.pop();
						}
						parent.workspaces.push(info[3]);
					}
				}
			}
	return data;
};

/** Gets all the folders that have been selected by a user
* @param {boolen} empty If true only empty folders will be returned
* @returns {list} A list of all selected folder ids
*/
EnteroWSTree.prototype.getSelectedFolders=function(empty){
	var a =  $(this.elementID).jstree('get_selected',true);
	var names=[];
	for (var index in a){
		 var node = a[index]
		  if (node.children.length !==0 && empty){
				continue;
		  }
		  names.push(node.id)
		 }
	return names;
}


/** Returns  the first node selected by a user
*/
EnteroWSTree.prototype.getSelectedNode=function(){
	var a =  $(this.elementID).jstree('get_selected');
	return a[0];
}


/** Gets the first folder selected by the user. If no folder is selected, ''RN' will be returned.
* If an item is selected rather than a folder, then the id of the parent folder will be returned.
* @returns The id of the selected folder
*/
EnteroWSTree.prototype.getSelectedFolder=function(){
	var a =  $(this.elementID).jstree('get_selected',true);
	var node = a[0];
	if (!node){
		return "RN";
	}
	if (node['type'] !== 'folder' ){
		return node['parent']
	}
	else{
		return node['id']
	}
}

/** Gets all selected workspaces.
* @param {boolean} onlyMine If true only workspaces the user owns will be returned
* @returns {list} A list of workspaces (see {@link EnteroWorkspaceDialog} for the format)
*/
EnteroWSTree.prototype.getSelectedWorkspaces=function(onlyMine){
	/* this is the old code
	var a =  $(this.elementID).jstree('get_selected');
	var ws_names=[];
	//var ws_names_folderId=[];//file names and folder_name_
	for (var index in a){
		var info = this.parent.workspaces[a[index]];
		if (info ){
			if (info[1]!=='mine' && onlyMine){
				continue;
			}
			ws_names.push(info);
		}
	}
	*/
	
	
	/**start below is the new code for folder sharing, 2018-09-27, yulei **/
	//get workspace_folders
	var workspace_folders=wholeData["workspace_folders"];
	//var idAll=[];	
	var a =  $(this.elementID).jstree('get_selected');
	var thisParentWorkspaces=this.parent.workspaces;
	var ws_names=[];
	//var ws_names_folderId=[];//file names and folder_name_
	for (var index in a){
		var info = thisParentWorkspaces[a[index]];
		
		if (info ){//means it is file id
			if (info[1]!=='mine' && onlyMine){
				continue;
			}
			ws_names.push(info);
		}else{//means it is folder id
			//get info about folder id a, such as j3_3
			
			/** start, added at 2018-10-04, yulei, for push the folder id only **/
			var a_info=workspace_folders[a[index]];	
			ws_names.push([a_info]);//set it as arrany type, i.e. like, [j3_3]
			/** end, added at 2018-10-04, yulei, for push the folder id only **/
			

		}
	}
	
	
	/**end below is the new code for folder sharing, 2018-09-27, yulei **/
	
	
	return ws_names;	
}


/** Saves the tree to the database 
* @param {string} species The name of the database
*/
EnteroWSTree.prototype.saveTree = function(species){
	if (!this.structure_altered){
		return;
	}
	var info = this.getTreeData();
	//check info is not corrupted
	if (info && info['RN']){
		to_send={
			database:species,
			type:"workspace_folders",
			name:"default",
			data:JSON.stringify(info)
		};
		if  (this.type=== 'public'){
			to_send.type= 'public_workspace_folders';
			to_send.admin=true;
		}
		console.log("saving "+ this.type)
		Enterobase.call_restful_json("/save_user_preferences","POST",to_send,self.showErrorMessage);
		this.structure_altered=false;
	}
}


/** Checks to see if the name exists in the tree
* @param {string} name The workspace name to check
* @returns {boolean} true if the name exists
*/
EnteroWSTree.prototype.nameExists=function(name){
	for (var id in this.parent.workspaces){
		if (name === this.parent.workspaces[id][0]){
			return true;
		}
	}
	return false;
}

/**
 * Creates a new workspace in the tree (not in the database), in the last selected folder
 * Should be called after the workspace has been saved and an id given
 * @param {list} ws The workspace to be saved see {@link EnteroWorkspaceDiallog} for the format
 */
EnteroWSTree.prototype.createNewWorkspace=function(ws,parent){
	//work out where to put it based on last selected node
	var a = $(this.elementID).jstree('get_selected',true);
	var node=a[0];
	//default 
	if (!parent){
		var parent ='RN';
		if (node){
			parent= node.id
			if (node.type!=='folder' && node.type!=='default' ){
				parent=node.parent;
			}
			if (parent==='#'){
				parent="RN";
			}	
		}
	}
	var new_node={
		text:ws[0],
		type:ws[4],
		id:ws[3]
	};
	this.parent.workspaces[ws[3]]=ws;
	$(this.elementID).jstree('create_node',parent,new_node,'first');
	$(this.elementID).jstree("open_node", parent);
}


/** Creates a new folder  
* @param {string} parent The id of the parent folder. If not supplied, the folder will be created in root
* @param {string} name The name of the folder
*/
EnteroWSTree.prototype.createNewFolder= function(parent,name){
	if (!parent || parent === '#'){
		parent='RN';
	}
	var node = {
		text:name,
		type:"folder"
	}
	var id =$(this.elementID).jstree('create_node',parent,node,'first');
	$(this.elementID).jstree("open_node", parent);
	this.structure_altered=true;
	return id;
}



EnteroWorkspaceDialog.prototype = Object.create(null);
EnteroWorkspaceDialog.prototype.constructor= EnteroWorkspaceDialog;
/**
* Creates a dialog from which the user can load/save or manipulate workspaces.The workspaces are in the format of a list in the following format
* [name, user_id or 'mine',user_name or 'mine' or 'public',id,type]
<pre>
  ["strains 101","mine","mine",456,"main_workspace"]  //user owns
  ["tree 101",67,"jack",456,"ms_tree"]  //shared
  ["snp 101",0."public",2323,"snp_project"]  //public
</pre>
* @constructor
* @param {string} species The name of the database
* @param {function} callback A function to be called once all the data is laoded into the tree(s). The function
* Should accept a pointer to the EnteroWorkspaceDailog e.g.
<pre>
 function(dialog){
	wspaces = dialog.workspaces;
	//do something with the workspaces;
}
</pre>
* @param {boolean} show_ws_details If true then subtrees of workspaces , will be displayed if
* the parent workspace contains subtrees
*/
function EnteroWorkspaceDialog(species,callback,show_ws_details, folder_type, folder_name_){
	this.species= species;
	this.folder_name_=folder_name_;
	//adding folder type (mine, public, shared) and folder name be to programtically selected
	// some other modifications to work with the selection process has been done through the code using these tow attibutes 
	//K.M. 22/10/2019
	if (folder_type=='mine'){
	  this.folder_type='ws-folder-tree';}  
	else if (folder_type=='public'){
	  this.folder_type='ws-public-folder-tree';}
	else if (folder_type=='shared'){
	  this.folder_type='ws-buddy-folder-tree';}

	this.ok_function = null;
	this.ws_tree=null;
	this.workspaces={};
	this.current_tree=null;
	this.tree_data={
		"mine":{},
		"public":{},
		"shared":{}
	}
	this._createDialog();
	this._addData(callback, this.folder_type, this.folder_name_);
	this._setupFunctions();
	this.current_ws_description=null;
	this.show_ws_details=show_ws_details;
}

EnteroWorkspaceDialog.prototype.showErrorMessage=function(msg){
	Enterobase.modalAlert(msg);
}

/**
* Shows the dialog and returns all the selected workspaces when the user presses the OK button 
* @param {function} callback A function, which accepts two arguments, the first being a list of all Ids selected
* and the second a list of all workspaces e.g.
<pre>
	function(ids,wps){
		alert ("You selected " + wps[0][0]) //the name of the first selected workspace
	}
</pre>
* @param {string} button_name - The text to display on the OK button e.g. Load
* @param {object} filter Limits the items from which to choose.can be a function see {@link EnteroWSTree.hideNodes} or
* an object  (see below)
<pre>
  {owner_only:true} //only show user workpaces
  {main_search_page:true} //only show items that can be laoded into a window
  {types:["ms_tree"]} //only show ms trees
</pre>
*/
EnteroWorkspaceDialog.prototype.getSelectedIDs= function(callback,button_name,filter){
	if(shareLoadButton==""){
		shareLoadButton=button_name;
	}
	$("#ws-folder-tree").jstree("open_node", "RN");
	var self = this;
	$(".ws-hide-show").hide();
	if (!button_name){
		button_name='Load'
	}
	$('#ws-ok-button').button('option', 'label', button_name);
	this.ok_function = function(){
		
		var info = self.current_tree.getSelectedWorkspaces(false);
		
		if(info && info[0][0]==undefined){
			return;
		}		
		
		if(info && info[0][0]["type"]=="folder" && (shareLoadButton =="Load" || shareLoadButton =="load") ) {
		    console.log("dblclick on folder");
		    return;		
		}
		
		//added by yulei, 20190513, find custom_view_id
		if(info && (info[0][0]=="cust_view" || info[0][4]=="custom_view") ){
			current_customViewId=info[0][3];
			rightGridType="customView";
		}		
		
		var ids=[];
		var wspaces=[];
		for (var n=0;n<info.length;n++){
			if( typeof(info[n][3]) == "undefined" ){
				//means a folder
				ids.push(info[n][0]["id"]);//get folder id, e.g, j3_3
			}else{
				ids.push(info[n][3]);
			}
			wspaces.push(info[n]);
		}
		
		callback(ids,wspaces);
		self.div.dialog("close");
	};
	this.div.dialog("open");
	if (filter){
		var func=null
		if (filter['main_search_page']){
			func=function(ws){
				if (self.workspace_types[ws[4]]['create_from_search']){
					return false;
				}
				return true;
			};
		}
		if (filter['owner_only']){
			func = function(ws){return (ws[1]!=='mine')};
		}
		if (filter['types']){
			func = function(ws){
			
				var index = filter['types'].indexOf(ws[4]);
				if (index === -1){
					return true;
				}
				return false;
			};
		}
		this.filter_function=func;
		for (var type in this.tree_data){
			var tree = this.tree_data[type]['tree'];
			if (tree){
				tree.hideNodes(func);
			}
		}
	}
}


/**
* Copies a public or shared workspace to the user's folders
* @param {list} ws The workspace to be added, see {@link EnteroWorkspaceDialog} for the format.
*/
EnteroWorkspaceDialog.prototype.copyToMainTree=function(ws){
	var text=this.ws_tree.getWorkspaceText(ws);
	var new_node={
		text:text,
		type:ws[4],
		id:ws[3]
	};
	$("#ws-folder-tree").jstree('create_node',"RN",new_node,'first');
}

/**
* Shows the dialog , users will be able to manipulate workspaces (delete,arrange folders etc.)but pressing the OK dbutton
* simply closes the dialog.
*/
EnteroWorkspaceDialog.prototype.showManageDialog= function(){
	var self = this;
	this.ok_function = function(){
		self.div.dialog("close");
	};
	this.div.dialog("open");
}



/**
* Adds a new workspace to the tree in the currently selected folder
* @param {list} ws The workspace to be added, see {@link EnteroWorkspaceDialog} for the format.
*/
EnteroWorkspaceDialog.prototype.addNewWorkspace=function(ws,parent){
	this.ws_tree.createNewWorkspace(ws,parent);
	this.workspaces[ws[3]]=ws;
	//this.ws_tree.saveTree(this.species);
	//this.structure_altered_true;
	this.ws_tree.structure_altered=true;
	this.ws_tree.saveTree(this.species);
}

/**
* Allows the user to select a folder and enter the new name of the workspace.
* @param {function} callback This function should accept a string, which is the name the user entered for the workspace.
* The workspace is not actually added to the to the tree. This should be done via the {@link EnteroWorkspaceDialog#addNewWorkspace} method, once the id of the workspace is known e.g
<pre>
		ws = ["","mine","mine","","ms_tree"]
		dialog.showSaveDialog(function(name){
			//save the ws to the database and get id
			ws[0]=name;
			ws[3]=id;
			dialog.createNewWorkspace(ws)
			},
		"MS Tree");
</pre>
*	@param (string) - type_name The title added to the dialog e.g Workspace (optional)
*/
EnteroWorkspaceDialog.prototype.showSaveDialog = function (callback,type_name){
	var self = this;
	if (type_name){
		$("#ws-save-label").html(type_name+" Name:");
		this.div.dialog("option","title","Save "+type_name)
	
	}
	else{
		$("#ws-save-label").html("Workspace Name:");
		this.div.dialog("option","title","Workspaces");
	}
	$(".ws-hide-show").show();
	$('#ws-ok-button').button('option', 'label', 'Save');
	$("#ws-folder-tree").jstree("open_node", "RN");
	this.div.dialog("open");
	this.ok_function = function(){
		var name= $("#ws-save-text").val();
		if (! name){
			Enterobase.modalAlert("Please enter a name for the workspace");
			return;
		}
		if (self.ws_tree.nameExists(name)){
			Enterobase.modalAlert("The name "+name +" already exists - please choose another");
			return;
		}
		self.div.dialog("close");
		callback(name);
	};
}

EnteroWorkspaceDialog.prototype._setupFunctions = function(){
	var self =this;
	
	$(".ws-tree-holder").bind("dblclick.jstree",function(){
		self.ok_function();
	});
	
	$("#ws-search-button").click(function(e){
		var name = $('#ws-search-text').val();
		if (!name){
			return;
		}
		
		self.current_tree.searchTree(name);	
	});

	$("#ws-search-text").keyup(function(e){
		var name = $('#ws-search-text').val();
		self.current_tree.searchTree(name);	
	});
	
	
	$("#ws-delete-button").click(function(e){
		
		var info = self.ws_tree.getSelectedWorkspaces(true);
		if (info.length===0){
			Enterobase.modalAlert("There are no workspaces selected that you own");
			return;
		}
		var ids = [];
		for (var n=0;n<info.length;n++){
			ids.push(info[n][3]);		
		}
		self.deleteWorkspaces(ids);
		
		//*******need to add code for deleting folder here******
		
	});
	
	$("#ws-deletefolder-button").click(function(e){
		var info = self.ws_tree.setPage(true);
		if (info.length===0){
			Enterobase.modalAlert("There are no empty folders selected");
			return;
		}
		self.ws_tree.deleteNodes(info);	
	});
	

};
/** Makes the workspace private, it will be removed frome public worspaces
* @param {integer} id The id of the workspace
*/
EnteroWorkspaceDialog.prototype.makePrivate= function(id){
	var self=this;
	Enterobase.call_restful_json("/make_workspace_private","POST",{"ws_id":id},self.showErrorMessage).done(function(data){
		if (data['status']==='OK'){
			self.ws_public_tree.deleteNodes([id]);
			Enterobase.modalAlert("The Analysis has been removed from public view and an email sent informing the original owner")
		}
		else{
			Enterobase.modalAlert(data['msg'])
		}
	});
};


/** Makes the workspace public, it will be removed from the user's folders and placed in the public folders
* @param {integer} id The id of the workspace
*/
EnteroWorkspaceDialog.prototype.makePublic= function(id){
	var self=this;
	Enterobase.call_restful_json("/make_workspace_public","POST",{"ws_id":id},self.showErrorMessage).done(function(data){
		if (data==='OK'){
			Enterobase.modalAlert("The item is now public","Information");
			//delete from owner folder
			self.ws_tree.deleteNodes([id]);
			var tree = $(self.ws_public_tree.elementID).jstree(true);
			//create user folder if not present
			var user_folder = tree.get_node(self.user_details[0]);
			if (!user_folder){
				tree.create_node("RN",{id:self.user_details[0],text:self.user_details[1],type:"folder"});
			}
			//add to public user folder
			self.ws_public_tree.createNewWorkspace(self.workspaces[id],self.user_details[0]);
		}
		else{
			Enterobase.modalAlert("There was a problem making the item public")
		}
	});

};


/** Permanantly deletes all workspaces from the database and removes them from the tree.
* @param {list} ids All the ids to be deleted
*/
EnteroWorkspaceDialog.prototype.deleteWorkspaces=function(ids){
	var self =this;
	var to_send={
			database:self.species,
			names: ids.join(",")
		}
	Enterobase.call_restful_json("/delete_workspaces","POST",to_send,self.showErrorMessage).done(function(data){
		var msg="";
		var ids_to_remove=[];
		for (var id in data){
			if (data[id][0]==='deleted'){
				ids_to_remove.push(id);
				msg+=data[id][1]+" was successfully deleted<br>"
			}
			else{
				msg+=data[id][1]+" could not be deleted<br>"
			}
		}
		
		Enterobase.modalAlert(msg);
		self.current_tree.deleteNodes(ids_to_remove)
	});
	
		
}

/** Gets the workspace
* @param {integer} id The id of the workspace
* @returns {list} A list describing the workspace see {@link EnteroWorkspaceDialog} for the format
*/
EnteroWorkspaceDialog.prototype.getWorkspace=function(id){
	return this.workspaces[id];
}

EnteroWorkspaceDialog.prototype._addData = function(callback, folder_type, folder_name_){
	
   var self = this;
 
	var to_send = {
		database :this.species,
		view_name:"default"
	};
	
	

	var preferences  = Enterobase.call_restful_json("/species/get_user_workspaces","GET",to_send,this.showErrorMessage);
	preferences.done(function(data){
	
		/**start, yulei added at 2018-09-26 **/
		wholeData=data;//set the data to wholeData
		var workspaceFoldersData=wholeData["workspace_folders"];
		var rootName="";
		
		if(workspaceFoldersData != null && workspaceFoldersData["RN"] != undefined){
			getRootInfo(workspaceFoldersData["RN"],rootName);//rootInfo
		}
		
		/**end, yulei added at 2018-09-26 **/
		
		
		
		
		for (var index in data['saved_workspaces']){
			var ws = data['saved_workspaces'][index];
			self.workspaces[ws[3]]=ws;
		}
		self.workspace_types=data['workspace_types'];
		self.is_administrator=data['is_administrator'];
		self.logged_in = data['logged_in'];
		self.user_details=data['user_details']
		self.ws_public_tree=new EnteroWSTree('ws-public-folder-tree',data['public_folders'],self,"public", folder_type, folder_name_);
		if (self.logged_in){
			self.ws_buddy_tree=new EnteroWSTree('ws-buddy-folder-tree',data['buddy_folders'],self,"shared", folder_type, folder_name_);
			self.ws_tree=new EnteroWSTree('ws-folder-tree',data['workspace_folders'],self,"main", folder_type, folder_name_);
			self.current_tree=self.ws_tree;
		}
		self.tree_data['mine']['tree']=self.ws_tree;
		self.tree_data['public']['tree']=self.ws_public_tree;
		self.tree_data['shared']['tree']=self.ws_buddy_tree;
		if (!self.logged_in){
			$("#public-tree-span").trigger("click");
			self.current_tree=self.ws_public_tree;
			$("#mine-tree-span").hide();
			$("#shared-tree-span").hide();
		}
        //check if the folder type if provided
	//if so, it will activate the its spane 
	//K.M. 22/10/2019
	if (folder_type=='ws-public-folder-tree')
		{
		    $("#public-tree-span").trigger("click");
		    self.current_tree=self.ws_public_tree;
		}
	else if (folder_type=='ws-buddy-folder-tree')
		{
		   $("#shared-tree-span").trigger("click");
		   self.current_tree=self.ws_buddy_tree;
		}
	else if (folder_type=='ws-folder-tree')
		{
		        $("#mine-tree-span").trigger("click");
			self.current_tree=self.ws_tree;			
		}
		
		if (self.logged_in  === 'false'){
			$('#ws-control-table').hide();		    
		}
		$(".ws-tree-holder").on("select_node.jstree", function (e, data) {
			self.showWSDetails(data);
			self.showWSDescription(data)
		});
		if (callback){
			callback(self);
		}
		
		/**start, yulei added at 2018-09-26 **/
		function getRootInfo(inputFolderData,inputRootName){
			
		    //
		    var text=inputFolderData["text"];
		    var id=inputFolderData["id"];
		    var children=inputFolderData["children"];
		    var workspaces=inputFolderData["workspaces"];
		
	            if(inputRootName == "" && text == "Root"){
			inputRootName=text;
		    }else{
			inputRootName=inputRootName+"/"+text;
		    }
		    
		    
		    allRoot[id]={rootPath:inputRootName,type:"folder"};
		    //find all workspaces
		    if(workspaces && workspaces.length>0){
			for(var indexOfWS in workspaces){
			    allRoot[workspaces[indexOfWS]]={rootPath:inputRootName,type:"file"};
			}
		    }
		
		    //check all children
		    if(children && children.length>0){
			for(var indexOfCHI in children){
			    allRoot[children[indexOfCHI]]={rootPath:inputRootName,type:"folder"};
			    getRootInfo(workspaceFoldersData[children[indexOfCHI]],inputRootName);
			}
		    }
		    
		
		}
		/**end, yulei added at 2018-09-26 **/		
		
		
		
	});
}

EnteroWorkspaceDialog.prototype.showWSDetails=function(data){
	var self = this;
	if (this.show_ws_details){
		var ws =this.workspaces[ data.node.id];
		if (ws[4]=='main_workspace'){
			Enterobase.call_restful_json("/get_workspace_children?id="+ws[3],"GET","",this.showErrorMessage).done(function(data){
				for (var i in data){
					var child = data[i];
					self.ws_tree.createNewWorkspace(child,ws[3]);
					self.workspaces[child[3]]=child;
				}
			});
		}
	}
};

/** Displays details of the workspace in the right hand panel of the dialog
* @param {data} id  The data returned from jstree e.g.
<pre>
$(".ws-tree-holder").on("select_node.jstree", function (e, data) {
	self.showWSDescription(data);
});
</pre>
*/
EnteroWorkspaceDialog.prototype.showWSDescription=function(data){
	var self = this;
	var ws = this.workspaces[data.node.id];
	var user_id = this.user_details[0]+"";
	if (ws){
		if (ws[2]==='mine' || this.is_administrator || data.node.parents.indexOf(user_id)!==-1){
			$("#ws-description-edit-icon").show();
		}
		else{
			$("#ws-description-edit-icon").hide();
		}
		Enterobase.call_restful_json("/get_workspace_summary?id="+ws[3],"GET","",this.showErrorMessage).done(function(data){
			$("#ws-description-paragraph").html(data.description);
			var html="<label>Created:</label>"+data.date_created+"<br>";
			html+="<label>Modified:</label>"+data.date_modified+"<br>";
			for (var label in data.parameters){
				html+="<b>"+label+":</b>"+data.parameters[label]+"<br>";
			}
			if (data.url){
				html+="<b>URL:</b>"+data.url;
			}
			$("#ws-dialog-description-div").html(html);
			self.current_ws_description = ws;
		});	
	}
}

EnteroWorkspaceDialog.prototype._updateDescription=function(){
	var text = $("#ws-description-text").hide().val();
	var to_send = {
		description:text,
		ws_id:this.current_ws_description[3]
	}
	Enterobase.call_restful_json("/save_workspace_summary","POST",to_send,this.showErrorMessage).done(function (data){
		if (data['status']==='OK'){
			$("#ws-description-paragraph").text(text).show();
		}
		else{
			Enterobase.modalAlert(data['msg']);
			$("#ws-description-paragraph").show();
		}
	});
}

EnteroWorkspaceDialog.prototype.getTypeIcons=function(){
	ws_icons={};
	for (var type in this.workspace_types){
		ws_icons[type]=this.workspace_types[type]['icon'];
	
	}
	return ws_icons;
}

EnteroWorkspaceDialog.prototype.showErrorMessage=function(msg){
	Enterobase.modalAlert(msg);
}

EnteroWorkspaceDialog.prototype._createDialog= function (){

	var self = this;
	var table = $("<table id ='entero-ws-dialog'></table>");
	this.div  = $("<div id ='individual_edit_dailog'></div>").append(table);
	$("body").append(this.div);
	
	this.div.dialog({     
		title: "Workspaces", 
		buttons:[{
			id: 'ws-ok-button',
			label:"OK",
			click: function() {
				self.ok_function()
			}
		}],
		close:function(){
			if (self.logged_in){
				self.ws_tree.saveTree(self.species);
				self.ws_public_tree.saveTree(self.species);
			}
			$(".ws-hide-show").hide();
			//unhide any filtered nodes
			for (var type in self.tree_data){
				var tree = self.tree_data[type]['tree'];
				if (tree){
					tree.showAllNodes();
				}
			}
			
		},
		open:function(){
			if (!self.logged_in){
				$("#ws-public-folder-tree").jstree("open_node", "RN");			
			}
		},
		autoOpen:false,
		width: '750px',
		height:'auto'   
	});
	this.tree_data['mine']['div']="#ws-folder-tree";
	this.tree_data['public']['div']= "#ws-public-folder-tree";
	this.tree_data['shared']['div']="#ws-buddy-folder-tree";
	var tree_div=$("<div style = 'float:left;'></div>");
	var mine_span=$("<span>").attr({"class":"tree-title-span","id":"mine-tree-span"}).text("Mine").data("tree",this.tree_data["mine"]).css("font-weight","bold");;
	var shared_span=$("<span>").attr({"class":"tree-title-span","id":"shared-tree-span"}).text("Shared").data("tree",this.tree_data["shared"]);
	var public_span=$("<span>").attr({"class":"tree-title-span","id":"public-tree-span"}).text("Public").data("tree",this.tree_data['public']);	
	var top_div=$("<div>");
	top_div.append(mine_span).append(shared_span).append(public_span);
	tree_div.append(top_div);	
	
	var buddy_tree = $("<div class='ws-tree-holder' id = 'ws-buddy-folder-tree' style = 'display:none'></div>")
	var folder_tree = $("<div  class='ws-tree-holder' id = 'ws-folder-tree' ></div>");
	var public_tree = $("<div  class='ws-tree-holder' id = 'ws-public-folder-tree' style = 'display:none'></div>");
	var bottom_div = $("<div>").height(400).width(350).css({"overflow":"scroll","border":"solid 1px"});
	bottom_div.append(buddy_tree).append(folder_tree).append(public_tree);
	tree_div.append(bottom_div).appendTo(this.div);
	
	var control_div= $("<div id = 'ws-control-div' style ='float:left;margin-left:10px'></div>");
	this.div.append(control_div);
	control_table= $("<table id ='ws-control-table'>");
	control_div.append(control_table);
	var row = $("<tr>").appendTo(control_table);	
	row.append($("<td style='padding:2px;border:4px'> <button id='ws-search-button'>Search</button></td>"));
	row = $("<tr>").appendTo(control_table);	
	row.append($("<td style='padding:2px;border:4px'><input  class ='table-input' id = 'ws-search-text' type='text'></td>"));
	control_table.append($("<tr><td>&nbsp;</td></tr>"));
	//row = $("<tr>").appendTo(control_table);	
	//row.append($("<td id ='ws-delete-row' ><button   id = 'ws-delete-button'>Delete Item</Button><button id = 'ws-deletefolder-button'>Delete Folder</Button></td>	"))
	//control_table.append($("<tr><td>&nbsp;</td></tr>"));
	//row = $("<tr>").appendTo(control_table);	
	//row.append($("<td><button  id = 'ws-public-button'>Make Public</button></td>"));
	//control_table.append($("<tr><td>&nbsp;</td></tr>"));
	row = $("<tr>").appendTo(control_table);	
	row.append($("<td><label  class = 'ws-hide-show' id ='ws-save-label'>Workspace Name:</label></td>"));
	row = $("<tr>").appendTo(control_table);	
	row.append($("<td><input  class='ws-hide-show'  class ='table-input'  id = 'ws-save-text' type='text'></td>"));
	var t_div=$("<div>").css("overflow","auto");
	t_div.append($("<label>Description</label>"));
	var edit_icon =$("<span id='ws-description-edit-icon' class='glyphicon glyphicon-edit'></span>")
			.css({"cursor":"pointer","display":"none"})
			.click(function(e){
				var text = $("#ws-description-paragraph").hide().text()
				$('#ws-description-text' ).val(text).show();
	
			});
	t_div.append(edit_icon).append("<br>").appendTo(control_div);
	var desc_para=$("<p id ='ws-description-paragraph'</p>").width(250);
	var desc_text= $("<textarea id = 'ws-description-text' rows='5' cols='30'></textarea>").css("display","none")
					.keypress(function(e){
						if (e.which===13){
							self._updateDescription();
						}
					});
	t_div.append(desc_para).append(desc_text);
	var description_div = $("<div>").attr("id","ws-dialog-description-div").width(200);
	control_div.append(description_div);
	
	$(".tree-title-span").css({"cursor":"pointer","font-size":"16px","padding":"3px","border-left":"solid 1px","border-top":"solid 1px","border-right":"solid 1px","margin-right":"5px"}).click(function(e){
		$(".tree-title-span").css("font-weight","normal");
		$(".ws-tree-holder").hide();
		$(this).css("font-weight","bold")
		var data =$(this).data("tree");
		$(data['div']).jstree("open_node", "RN");
		self.current_tree=data['tree']
		$(data['div']).show();
	})
	
	$(".ws-hide-show").hide();
};



EnteroWorkspaceSummaryDialog.prototype = Object.create(null);
EnteroWorkspaceSummaryDialog.prototype.constructor= EnteroWorkspaceSummaryDialog;


/**
	* Creates a dialog from which shows the workspace summary and allows users to alter description/links
	* @constructor
	* @param {integer} ws_id- The id of the workspace
	*/
function EnteroWorkspaceSummaryDialog(ws_id){
	this.ws_id=ws_id;
	this.data = null;
	this.trees_to_delete=[];
	this.ediit_mode=false;
	
	
}


EnteroWorkspaceSummaryDialog.prototype.showErrorMessage=function(msg){
	Enterobase.modalAlert(msg);
}

EnteroWorkspaceSummaryDialog.prototype._updateSummaryInformation=function(){
	var to_send= {
		description:$("#ws-summary-desc").val(),
		links:JSON.stringify(this.data['links']),
		ws_id:this.ws_id,
		trees_to_delete:JSON.stringify(this.trees_to_delete)
			
	};
	
	
	var info = Enterobase.call_restful_json("/save_workspace_summary","POST",to_send,this.showErrorMessage);
	info.done(function(data){
		
	});
	
}

EnteroWorkspaceSummaryDialog.prototype.treeClicked=function(id,type){};


/**
	* Opens the dialog (will load the data if not already loaded)
	*/
EnteroWorkspaceSummaryDialog.prototype.open=function(){
	var self =this;
	if (!this.div){
		var info = Enterobase.call_restful_json("/get_workspace_summary?id="+this.ws_id,"GET","",this.showErrorMessage);
		info.done(function(data){
			self.data=data;
			self._init();
		});
	
	
	}
	else{
		this.div.dialog("open");
	}

};

/**
Deletes the dialog
*/
EnteroWorkspaceSummaryDialog.prototype.delete=function(){
	if (this.div){
		this.div.dialog("destroy");
		this.div.remove();
	}

};

EnteroWorkspaceSummaryDialog.prototype._addLink=function(text,href){
	var self =this;
	var a = $("<a>").html(text).attr({"href":href,"target":"_blank"}).css("margin-right","3px");
	var li = $("<li>").append(a);
	if (this.data['edit_permissions']=='true'){
		var edit_icon = $("<span>").attr("class","glyphicon glyphicon-edit ws-sum-hide-show").css("cursor","pointer");
		edit_icon.click(function(e){
			var target = $(this).parent().find("a"); 
			$("#ws_summary_link_text_edit").val(target.html());
			$("#ws_summary_link_href_edit").val(target.attr("href"));
			$("#ws_summary_link_edit_div").show().data({"add":false,target:target});
			$("#ws_summary_add_but").hide();
		});
		li.append(edit_icon);
		var delete_icon = $("<span>").attr("class","glyphicon glyphicon-remove ws-sum-hide-show").css("cursor","pointer");
		delete_icon.click(function(e){
			var text = $(this).parent().find("a").html();
			delete self.data['links'][text];
			$(this).parent().remove();
		
		});
		li.append(delete_icon);
	}
	$("#ws_summary_link_list").append(li);
}


EnteroWorkspaceSummaryDialog.prototype.changeButtonText=function(id,text){
	var but =$("#"+id);
	var el = but.find(".ui-button-text");
	if (el.length===0){
		el=but;
	}
	el.text(text);

}

EnteroWorkspaceSummaryDialog.prototype._init=function(){
	var self = this;
	var editable = false;
	if (this.data['edit_permissions']=="true"){
		editable = true;
	}
	this.div  = $("<div id ='workspace_summary_dialog'></div>");
	$("body").append(this.div);
	var but_array= [{
		text:"Close",
		id:"ws-summary-ok-button",
		click: function() {
			self.div.dialog("close");
		}
	}];
	
	if (editable){
		but_array.push({
			text:"Edit",
			id:"ws-summary-edit-button",
			click:function(e){
				if (!self.edit_mode){
					$(".ws-sum-hide-show").show();
					self.changeButtonText("ws-summary-edit-button","Update");
					
					$("#ws-summary-desc").removeAttr("disabled");
					self.edit_mode=true;
					
				}
				else{
					$(".ws-sum-hide-show").hide();
					self.edit_mode=false;
					$("#ws-summary-desc").attr("disabled","disabled");
					self._updateSummaryInformation();	
					self.changeButtonText("ws-summary-edit-button","Edit");
				}		
			}
			
		});
	
	}
		
	
	
	
	
	this.div.dialog({     
		title: this.data['name'], 
		buttons:but_array,
		close:function(){
			
		},
		autoOpen:false,
		width: 'auto',
		height:'auto'   
	});
	
	var information_div = $("<div>");
	this.div.append(information_div);
	information_div.append("<label>Description</label><br>");
	var desc_text= $("<textarea>").attr({rows:"4",cols:"50",id:"ws-summary-desc","disabled":"disabled"})
							    .text(this.data['description']);
	information_div.append(desc_text);
	information_div.append($("<br>"));
	if (this.data['date_created']){
		information_div.append("<label>Created:</label>").append("<span>"+this.data['date_created']+"</span>");
		information_div.append("<br><label>Modified:</label").append("<span>"+this.data['date_modified']+"</span><br>");
	}
	for (var param in this.data['parameters']){
		information_div.append("<label>"+param+":</label><span>"+this.data['parameters'][param]+"</span><br>")
	}
	information_div.append($("<label>Links</label><br>"));
	var link_list = $("<ul>").attr("id","ws_summary_link_list");
	information_div.append(link_list);
	for (var text in this.data["links"]){
		this._addLink(text,this.data['links'][text]);
		
	}
	var link_edit_div = $("<div>").attr("id","ws_summary_link_edit_div");
	var text_input = $("<input>").attr({length:"70",id:"ws_summary_link_text_edit"});
	var href_input = $("<input>").attr({length:"70",id:"ws_summary_link_href_edit"})
	link_edit_div.append($("<label>Text</label><br>")).append(text_input).append($("<br>"));
	link_edit_div.append($("<label>URL</label><br>")).append(href_input).append($("<br><br>"));
	
	var edit_but = $("<button>").html("OK").click(function(e){
		var edit_div = $("#ws_summary_link_edit_div");
		var text= $("#ws_summary_link_text_edit").val();
		var href = $("#ws_summary_link_href_edit").val();
		$("#ws_summary_link_text_edit").val("");
		$("#ws_summary_link_href_edit").val("");
		self.data['links'][text]=href;
		if (edit_div.data("add")){
			self._addLink(text,href);
		}
		else{
			var target= edit_div.data("target");
			target.html(text).attr("href",href);
		
		}
		
		edit_div.hide();
		$("#ws_summary_add_but").show();
		
	});
	link_edit_div.append(edit_but);
	information_div.append(link_edit_div);
	link_edit_div.hide();
	if (editable){
		var but = $("<button>").html("Add").attr({"id":"ws_summary_add_but","class":"ws-sum-hide-show"});
		but.click(function(e){
			$("#ws_summary_link_edit_div").show();
			$("#ws_summary_link_edit_div").data("add",true);
			$(this).hide();
		
		
		});
		information_div.append(but);
	
	}
	
	
	
	
	
	var tree_div= $("<div>");
	this.div.append(tree_div);
	
	var tree_types = {"ms_trees": ["MS Trees","/static/img/icons/ms_tree.png"],"snp_projects":["SNP Projects","/static/img/icons/snp_tree.png"]};
	for (var tree_type in tree_types ){
		var label = tree_types[tree_type][0];
		var icon = tree_types[tree_type][1];
		var tree_list = $("<ul>").css("list-style-image","url('"+icon+"')");
		tree_div.append($("<label>"+label+"</label><br>"));
		tree_div.append(tree_list);
		for (var index in this.data[tree_type]){
			var info = this.data[tree_type][index].split(":")
			var list_item = $("<li>").attr("id","tree_list_item_"+info[1]).data({"tree_id":info[1],"tree_type":tree_type,"tree_name":info[0]});
			var tree_text = $("<span>").css({"cursor":"pointer"}).html(info[0]).click(function(e){
				var parent = $(this).parent();
				self.treeClicked(parent.data("tree_id"),parent.data("tree_type"),parent.data("tree_name"));
			
			});
			list_item.append(tree_text)
			
			
			if (editable){
				var delete_icon=$("<span>").attr("class","glyphicon glyphicon-remove ws-sum-hide-show")
										.css({"cursor":"pointer","margin-left":"4px"})
										
										
				delete_icon.click(function(e){
					var parent = $(this).parent();
					var tid = parent.data("tree_name")+":"+parent.data("tree_id");
					var type = parent.data("tree_type")
					self.trees_to_delete.push([tid,type]);
					parent.hide();
				});
				list_item.append(delete_icon);
			
			}
			tree_list.append(list_item)
			
		}
	}
	if (this.data.type !=="main_workspace"){
		tree_div.hide();
	}
	$('#ws-summary-ok-button').button('option', 'label', "OK");
	$('#ws-summary-edit-button').button('option', 'label', "Edit");
	$(".ws-sum-hide-show").hide();
	this.div.dialog("open");
	
}