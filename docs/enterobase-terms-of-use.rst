Enterobase Terms and Conditions
===============================

**PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY BEFORE USING THIS SERVICE.
READ APPENDIX A**

**PLEASE READ OUR GDPR UPDATE** :doc:`GDPR`.

WHAT'S IN THESE TERMS?
----------------------
These terms tell you the rules for using our web service EnteroBase https://enterobase.warwick.ac.uk/ 

WHO WE ARE AND HOW TO CONTACT US
--------------------------------
https://enterobase.warwick.ac.uk/ is a service operated by the Achtman Lab in 
Warwick Medical School, which is a department within the University of Warwick ("We"). 

To contact us, please email enterobase@warwick.ac.uk

EnteroBase aims to establish a world-class, one-stop, user-friendly, 
backwards-compatible but forward-looking genome database, EnteroBase -- 
together with a set of web-based tools, to enable bacteriologists to identify, 
analyse, quantify and visualize genomic variation.

BY USING OUR SERVICE YOU ACCEPT THESE TERMS
-------------------------------------------
By using our service, you confirm that you accept these terms of use and that you
agree to comply with them. If you do not agree to these terms, you must not use 
our service.

THERE ARE OTHER TERMS THAT MAY APPLY TO YOU
These terms of use refer to the following additional terms, which also apply to 
your use of our service:

**See** `APPENDIX A`_

WE MAY MAKE CHANGES TO THESE TERMS
----------------------------------
We amend these terms from time to time. Every time you wish to use our service, 
please check these terms to ensure you understand the terms that apply at that 
time.

WE MAY CHANGE, SUSPEND OR WITHDRAW OUR SERVICE
----------------------------------------------
We may update and change our service from time to time.

We do not guarantee that our service, or any content on it, will always be 
available or be uninterrupted. We may suspend or withdraw or restrict the 
availability of all or any part of our service for business and operational 
reasons. We will try to give you reasonable notice of any suspension or withdrawal.

You are also responsible for ensuring that all persons who access our service 
through your internet connection are aware of these terms of use and other 
applicable terms and conditions, and that they comply with them.

YOU MUST KEEP YOUR ACCOUNT DETAILS SAFE
---------------------------------------
If you choose, or you are provided with, a user identification code, password or
any other piece of information as part of our security procedures, you must treat
such information as confidential. You must not disclose it to any third party.

We have the right to disable any user identification code or password, whether 
chosen by you or allocated by us, at any time, if in our reasonable opinion you 
have failed to comply with any of the provisions of these terms of use.
If you know or suspect that anyone other than you knows your user identification 
code or password, or if you become aware of any apparent breaches of security 
such as loss, theft or unauthorised disclosure, you must promptly notify us at 
enterobase@warwick.ac.uk

HOW YOU MAY USE MATERIAL ON OUR SERVICE
---------------------------------------
**See APPENDIX A**

You will use this service in a manner consistent with all applicable laws, 
legislation, rules and regulations.
We are the owner or the licensee of all intellectual property rights in our 
service, and in the material published on it. Those works are protected by 
copyright laws and treaties around the world. All such rights are reserved.
Our status (and that of any identified contributors) as the authors of content 
on our service must always be acknowledged.

You must not use any part of the content on our service for commercial purposes 
without obtaining a licence to do so from us and/or our licensors.

DO NOT RELY ON INFORMATION ON THIS SERVICE
------------------------------------------
The content on our service is provided for general information only. It is not 
intended to amount to advice on which you should rely. You must obtain 
professional or specialist advice before taking, or refraining from, any action 
on the basis of the content on our service.

Although we make reasonable efforts to update the information on our service, 
we make no representations, warranties or guarantees, whether express or implied,
that the content on our service is accurate, complete or up to date. We are not 
responsible for the results of reliance on any such information.

WE ARE NOT RESPONSIBLE FOR WEB SERVICES WE LINK TO
--------------------------------------------------
The terms and conditions of any other web services that you link to through this
service will also apply to the exclusion of all other terms and conditions 
including the present ones.

OUR RESPONSIBILITY FOR LOSS OR DAMAGE SUFFERED BY YOU
-----------------------------------------------------
Whether you are a consumer or a business user:

* We do not exclude or limit in any way our liability to you where it would be 
  unlawful to do so. This includes liability for death or personal injury caused
  by our negligence or the negligence of our employees, agents or subcontractors
  and for fraud or fraudulent misrepresentation.
* To the extent permitted by law, we do not warrant that use of this service will
  not infringe the rights of any other person or organisation, and we accept no
  responsibility or liability for any material communicated by you or any third
  parties via our service.
* Without limitation to any other of our terms, we will in no case be liable for
  damage to any device or digital content belonging to you that you could have 
  avoided by following our advice to apply an update offered to you free of charge 
  or for damage that was caused by you failing to correctly follow installation 
  instructions or to have in place the minimum system requirements advised by us.

If you are a business user:

* We exclude all implied conditions, warranties, representations or other terms 
  that may apply to our service or any content on it.
* We will not be liable to you for any loss or damage, whether in contract, tort
  (including negligence), breach of statutory duty, or otherwise, even if 
  foreseeable, arising under or in connection with:
    * use of, or inability to use, our service; or
    * use of or reliance on any content displayed on our service.
* In particular, we will not be liable for:
    * loss of profits, sales, business, or revenue;
    * business interruption;
    * loss of anticipated savings;
    * loss of business opportunity, goodwill or reputation; or
    * any indirect or consequential loss or damage.

Attention all users:

* This service is provided for academic use only. You agree not to use our 
  service for any commercial or business purposes whatsoever, and we have no 
  liability to you for any loss of profit, loss of business, business 
  interruption, or loss of business opportunity.

POSTING MATERIAL ON OUR SERVICE
-------------------------------
We do not intend to regularly monitor the use of our service or data transmitted 
to it. However, if we become aware that you are misusing our service, we reserve 
the right to take appropriate action, which may include excluding you and your 
affiliates from accessing our service and removing all data and materials that 
you may have transmitted.

WE ARE NOT RESPONSIBLE FOR VIRUSES AND YOU MUST NOT INTRODUCE THEM
------------------------------------------------------------------
We do not guarantee that our service will be secure or free from bugs or viruses.
You are responsible for configuring your information technology, computer 
programmes and platform to access our service. You should use your own virus 
protection software.

You must not misuse our service by knowingly introducing viruses, trojans, worms,
logic bombs or other material that is malicious or technologically harmful. 
You must not attempt to gain unauthorised access to our service, the server 
on which our service is stored or any server, computer or database connected to 
our service. You must not attack our service via a denial-of-service attack or 
a distributed denial-of service attack. By breaching this provision, you would 
commit a criminal offence under the Computer Misuse Act 1990. We will report any 
such breach to the relevant law enforcement authorities and we will co-operate 
with those authorities by disclosing your identity to them. In the event of such 
a breach, your right to use our service will cease immediately.

INDEMNITY 
---------

If you breach any of these terms, you agree to indemnify us for any losses, 
costs, expenses, fines or damages, including reasonable legal fees, incurred by 
or imposed upon us in relation to, or arising out of, such a breach.

VALIDITY OF THESE TERMS
-----------------------
If any part of these terms is declared invalid for any reason by a court of 
competent jurisdiction, this will not affect the validity of any remaining part 
of the terms. Any such remaining part will remain in full force and effect as if 
the invalid part of the terms had been eliminated.

OTHER IMPORTANT TERMS 
---------------------
We may transfer this agreement to someone else. We may transfer our rights and 
obligations under these terms to another organisation. We will always tell you 
by email if this happens and we will ensure that the transfer will not affect 
your rights under the contract.

If a court finds part of this contract illegal, the rest will continue in force. 
Each of the paragraphs of these terms operates separately. If any court or 
relevant authority decides that any of them are unlawful, the remaining paragraphs 
will remain in full force and effect.

These terms of use, their subject matter and their formation (and any 
non-contractual disputes or claims) are governed by English law. We both agree 
to the exclusive jurisdiction of the courts of England and Wales.

PRIVACY POLICY
--------------
We are the Achtman Lab of Warwick Medical School, which is a department within 
the University of Warwick whose administrative offices are at University House, 
Kirby Corner Road, Coventry, CV4 8UW.

We want to protect the privacy of all visitors to our webs services and of all 
of our customers, and this policy will help you to understand how we use your 
personal data.

This policy and (any other documents referred to on it) sets out the basis on 
which any personal data we collect from you, or that you provide to us, will be 
processed by us. Please read the following carefully to understand our views and 
practices regarding your personal data and how we will treat it.

By visiting https://enterobase.warwick.ac.uk// and/or by giving us any personal 
data, you are accepting and consenting to the practices described in this policy, 
including our use of cookies as explained below.
For the purpose of the Data Protection Act 1998, the data controller is The 
Administrative Officer (Compliance), Deputy Registrar's Office, University of 
Warwick, whose administrative offices are at University House, Kirby Corner Road, 
CV4 8UW, United Kingdom.

INFORMATION WE COLLECT
----------------------
Information you give us. You may provide us with personal information such as 
your name and contact details when you fill in a form on our service or when 
you correspond with us by phone, e-mail or otherwise. This is done to enable us 
to provide our products and services to you, for example in order for you to 
create an account, purchase credits, place an order, make an enquiry, or report 
a problem.

Information automatically collected when visiting the web service. We may also 
collect information about your computer, including where available your IP 
address, operating system and browser type, and information about your visit 
(such as clickstream to and from our service and pages viewed) for system 
administration and to report aggregate information to our webmasters. This 
is statistical data about our users' browsing actions and patterns which does 
not identify any individual and allows us to ensure that content from our service 
is presented in the most effective manner for you and for your computer.

COOKIES
-------
Our web service uses cookies to distinguish you from other users of our web 
service. In particular, we use Google Analytics cookies for the purposes of 
managing the performance and design of the service. This helps us to provide 
you with a good experience when you browse our web service and also allows us to
improve our service. By continuing to browse our service, you are agreeing to 
our use of cookies.

A cookie is a small piece of data or message that is sent from a web server
to your browser and stored on the hard drive of your computer. Cookies may
make it easier for you to log on to and use this web service in future. They
allow us to monitor web service traffic and to personalise the content of
this service for our visitors.

We use the following cookies:

* Strictly necessary cookies. These are cookies that are essential in order to 
  enable you to move around the web service and use its features, such as 
  accessing secure areas of the web service.
* Analytical/performance cookies. They allow us to recognise and count the number 
  of visitors and to see how visitors move around our web service when they are 
  using it. All information these cookies collect is aggregated and therefore 
  anonymous. This helps us to improve the way our web service works, for 
  example, by ensuring that users are finding what they are looking for easily.
* Functionality cookies. These are used to recognise you when you return to our 
  web service and to remember choices you make and provide enhanced, more 
  personal features.

Most web browsers allow some control of most cookies through the browser
settings. To find out more about cookies, including how to see what cookies
have been set and how to manage and delete them, please visit
www.allaboutcookies.org. To opt out of being tracked by Google Analytics
across all web services visit http://tools.google.com/dlpage/gaoptout.
However, if you disable or decline cookies, some features will not be
available to you or function properly and you may not be able to access some
parts of the web service.

USES MADE OF THE INFORMATION
----------------------------
We use and analyse information held about you in the following ways:

* to provide you with access to relevant parts of our web service;
* to supply you with the information, products and services that you request from us;
* as part of our efforts to keep our service safe and secure;
* to administer, support, improve and develop our service, products and services; in particular, we may use your personal data to contact you for your views on our products and services and to notify you occasionally about changes or developments to our web service, products or services;

We may contact you by e-mail or by post to ask for feedback on how you use
our service, but you may stop receiving these types of communications from us
at any time by contacting us at enterobase@warwick.ac.uk.

WHEN WE SHARE YOUR INFORMATION
------------------------------
We do not sell your personal information to third parties.

We share your information within the University of Warwick, and to a limited extent with 
other users (Appendix A). 

We may also share 
your information with our affiliates, our business partners, our agents, our 
distributors, our suppliers, and our sub-contractors that assist us in making 
our web service and/or the products and services on our web service available to
you. This includes, without limitation, analytics and search engine providers 
that assist us in the improvement and optimisation of our service.

If we believe that your use of our web service is unlawful or damaging to others,
we reserve the right to disclose the information we have obtained through our 
web service about you to third parties to the extent that it is reasonably 
necessary in our opinion to prevent, remedy or take action in relation to such conduct.

We may also disclose or share your personal data in order to comply with any 
legal obligation; in order to enforce or apply any agreements or licences with 
you; or to protect the rights, property, or safety of the University of Warwick, 
our employees, affiliates, agents, distributors, licensors, suppliers, business 
partners, sub-contractors, and/or our customers. This includes exchanging 
information with other companies and organisations for the purposes of fraud 
protection and credit risk reduction.

If any part of our business (including those of our affiliates) is sold or 
integrated with another business, your details may be disclosed to our advisers 
and any prospective purchasers and their advisers.

WHERE WE STORE YOUR PERSONAL DATA
---------------------------------
Information which you provide to us will ordinarily be stored on our secure 
servers. However, we do work with third party contractors, some of whom host and 
operate certain features of the web service. Accordingly, information that we 
collect from you may be collected in or transferred to a destination outside 
the European Economic Area ("EEA"). 

That information may be processed by staff operating outside the EEA who work 
for us or for one of our contractors. Bear in mind that countries outside the 
EEA do not always have strong data protection laws. By submitting your personal 
data, you agree to this transfer, storing and processing. We will take all steps 
reasonably necessary to ensure that your data is treated securely and in accordance 
with the Data Protection Act 1998 and this policy.

We may disclose your personal information to third parties if we are under a 
duty to disclose or share such information in order to comply with any legal 
obligation or to protect the rights, property or safety of the University of 
Warwick, its members or others.

Unfortunately, no data transmission over the internet is completely secure.
We therefore cannot guarantee the security of your data transmitted to our
service, and any transmission is at your own risk. However, once we have
received your information, we will use strict procedures and security
features to try to prevent unauthorised access, unlawful processing,
accidental loss, destruction and damage.

Where we have given you (or where you have chosen) a password which enables
you to access certain parts of our service, you are responsible for keeping
this password confidential. We ask you not to share a password with anyone.

YOUR RIGHTS
-----------
You have the right to ask us not to process your personal data for marketing
purposes. We will usually inform you (before collecting your data) if we
intend to use your data for such purposes or if we intend to disclose your
information to any third party for such purposes. You can exercise your right
to prevent such processing at any time by contacting us at 
enterobase@warwick.ac.uk.

Our service may, from time to time, contain links to and from other web
services, for instance our third party data providers. If you follow a link
to any of these web services, please note that these web services have their
own privacy policies and that we do not accept any responsibility or
liability for their policies. Please check their policies before you submit
any personal data to those web services.

ACCESS TO INFORMATION
---------------------
Individuals have certain rights of access to their personal data, including
the rights to see the personal data held about them and to ask that necessary
changes are made to ensure that those data are accurate and up to date. If
you wish to do this, please contact: enterobase@warwick.ac.uk.

CHANGES TO OUR PRIVACY POLICY
-----------------------------
We may change this privacy policy at any time without notice, so please check
this policy each time you visit this web service.

CONTACT
-------
Questions, comments and requests regarding this privacy policy are welcomed
and should be addressed to us by telephone at ``+44 2476151921`` or by
writing to us at enterobase@warwick.ac.uk

APPENDIX A
----------
**Enterobase Users can do the following with no restrictions (public domain):**

* Use, download and redistribute  Achtman - 7 gene MLST schemes for Salmonella, Escherichia/Shigella, the Yersiniae and Moraxella.

**Enterobase Users can do the following for academic use only:**

* Search and download public strain records and analysis results including strain metadata, genotyping and serotyping results, genome annotation and other analyses provided by EnteroBase
* Perform analyses on subsets of the database within the website and export the public results.  
* View and download rMLST genotyping results for strains within EnteroBase
* Upload sequencing data to be analysed by Enterobase's suite of tools. 
* Use cgMLST schemes developed by the EnteroBase developers, Users can also export these data for offline analysis and presentation. 

Usage outside of academic use will require explicit licensing from University
of Warwick. Contact ventures@warwick.ac.uk

  
**Enterobase Users agree to the following if/when they upload their data to us:**

* EnteroBase will inspect user uploaded sequencing data, process it, and call a number of analyses - all results will be available to the data submitter.
* EnteroBase reserves the right to automatically run and present results of new analyses implemented on the website (subsequent to data submitters submission), without notifying the user. 
* derivative analyses results will be made available to other users as soon as they are completed - this includes, serotyping and genotyping results. 
* the genome assembly will be available to data submitter only  for a period of up to 12 months, after which it will be made publicly available. 
* the raw sequencing data will not be available through EnteroBase  to any other user, but will be stored for running new analyses or for verification of past ones.
* show their Name, Email Address, Country, Institution and Department to any registered user who clicks on Show Ownership for the uploaded entry in order to facilitate communication.
 

**Enterobase Users CANNOT:**

* Download, use or redistribute rMLST data, either allelic profiles or allele sequences. These are copyrighted by University of Oxford.
  Please contact them for licencing. 
* Modify and redistribute data or analyses from EnteroBase without clearly attributing the original source as EnteroBase, and stating the modifications they have made.  
* Reverse engineering and Reproducing EnteroBase-like database copies without explicit licencing
* download and/or publish any metadata, genomes, genotypes or trees containing uploadeed entries that have not yet been released.

**Caveats:**

* EnteroBase Users may be surveyed on their use of EnteroBase to promote and improve the service
* If you use data generated by 3rd party tools in EnteroBase, please cite both EnteroBase and the paper describing the specific tool:
    * **rMLST** is Copyright 2010-2016, University of Oxford. rMLST is described in: `Jolley et al. 2012 Microbiology 158:1005-15. <http://www.ncbi.nlm.nih.gov/pubmed/22282518>`_
    * **Serovar predictions (SISTR)** have been calculated using the pipeline developed
      by the SISTR team and is described in `Yoshida et al. 2016 PLoS ONE 11(1): e0147101. <http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0147101>`_
    * **ClermonTyping** is described in `Beghain et al. 2018 M Gen 4(7) doi:10.1099/mgen.0.000192`_
    * **EzClermont** is described in: `Waters et al. 2018 BioRxiv 317610`_
    * **FimTyper** is described in: `Roer et al. 2017 J.Clin.Microbiol. 2017; 55 (8):2538-2543`_
* EnteroBase retrieves data from public databases and is reviewed by EnteroBase 
  users who act as independent curators. We offer no guarantee of accuracy or 
  consistency with the metadata presented in EnteroBase.
* EnteroBase aims to serve genomic data to improve access to the scientific community. We hope that this will lead to collaborations between data submitters and 3rd parties. However data remains property of the data submitter (not with EnteroBase), and use of data beyond what EnteroBase displays should be checked with the original institution who submitted the data. This includes reproduction and derivative works, such as analysis, publication in scientific journals or online in web blogs. 

NCBI data policy, from where we fetch most of the data: 

https://www.ncbi.nlm.nih.gov/home/about/policies.shtml