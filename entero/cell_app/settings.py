# CELERY SETUP
import platform
BROKER_URL = 'amqp://guest:guest@localhost:5672//'
CELERY_IMPORTS = ('entero.cell_app.tasks', )
CELERY_RESULT_BACKEND = 'amqp'
CELERY_ACCEPT_CONTENT = ['pickle']
CELERY_ROUTES= {}
#CELERYD_FORCE_EXECV = True

#specify which queue to send each task to in windows all sent to the same
if platform.system() <> 'Windows':
    CELERY_ROUTES = {'entero.cell_app.tasks.database_task': {'queue': 'database'},
                     'entero.cell_app.tasks.file_task': {'queue': 'file'}
                    }

from datetime import timedelta
    
CELERYBEAT_SCHEDULE = {
    'check_queued_assemblies': {
        'task': 'entero.cell_app.tasks.check_queued',
        'schedule': timedelta(minutes=30),
        'args': (),
    },
}

CELERY_TIMEZONE = 'UTC'
