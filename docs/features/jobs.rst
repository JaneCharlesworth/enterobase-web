Monitoring Jobs
===============
The progress of your jobs can be views by Tasks>Show My Jobs 
 
By default, the last 200 jobs are shown in all states. To show more jobs, increase the value in the Limit input (Figure 14 red box) and press one of the buttons in the blue box:

* **All** - Shows all jobs
* **Running** - Shows jobs in wait resource, queue or running state
* **Failed** – Shows jobs which are failed or have been killed
* **Stale** - Shows jobs which have been running longer than a day

By clicking on the eye, information about the job is displayed in the right hand pane (although this is not very readable). When the page first loads information about the top job in the table will be displayed in this pane.

Usually all jobs are run automatically, once your assembly job has finished, nomenclature and any other jobs that act upon the assembly will be initiated. In addition every hour all databases are automatically checked and any outstanding or failed jobs are resent. However, in certain circumstance jobs can be manipulated by right clicking on the grid which brings up the context menu.

Context menu options:

* **Refresh Selected Jobs** – Jobs go through four states Wait Resource > Queued > Running >Complete/Failed. The server is informed only when a job has completed or fails. Therefore refreshing a job will update its intermediate status in the grid
* **Resend Selected Jobs** – This will resend any selected jobs which have failed
* **Force Reload on Selected** - If there has been major network issues or the server was temporarily down, the server may be unaware that a job has. In this case the job status in the grid will be Running/Queued but the information on the right hand pane will show the job has been completed. Under these circumstances, forcing a reload will ensure that the job is processed


  .. image:: https://bitbucket.org/repo/Xyayxn/images/182563582-jobs.png