Dear {{ user.firstname }} {{user.lastname}},

Your {{type}} named {{analysis.name}} (ID:{{analysis.id}}) has been removed from public view
because it was deemed to be unsuitable.

Sincerely,

The Enterobase Team